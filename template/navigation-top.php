        
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $base_url ?>">
                        <!--<img class="logo" src="<?php echo $base_url ?>/assets/imgs/rateme-banner.png" width="170px" style="margin-top: -9px" />-->
                        <font color="#ffffff"><b><span>RATE</span><span class="color-gray">ME</span></b></font>
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <!--<li>
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Results <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">Announce Results</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="">Over-all Results</a></li>
                                <li><a href="">Invidual Results</a></li>
                                <li><a href="">Personal Ratings</a></li>
                            </ul>
                        </li>-->
                        <li>
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php print($my_name) ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href=""><i class="fa fa-user-o"></i> My Profile</a></li>
                                <li><a href="<?php echo $base_url ?>/application/change-password"><i class="fa fa-lock"></i> Change Password</a></li>
                                <li><a href="<?php echo $base_url ?>/application/logout/logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <!--<div style="margin-bottom: 70px;"></div>-->