<?php 
if (!isset($page_title)) {   
    $page_title = 'RateME';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo $base_url ?>/assets/imgs/small-icons/favicon.png">
        <title><?php echo $page_title ?></title>
        <link href="<?php echo $base_url ?>/assets/css/adminlte.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url ?>/assets/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo $base_url ?>/assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $base_url ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $base_url ?>/assets/js/show-password.js" type="text/javascript"></script>
        <link href="<?php echo $base_url ?>/assets/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo $base_url ?>/assets/datatables/datatables.min.js" type="text/javascript"></script>
        <link href="<?php echo $base_url ?>/assets/css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="loading" style="display: none;">            
            <div class="spinner">
                <h3>Loading</h3>
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>