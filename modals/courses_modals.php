<!-- Add Course Modal [Start] -->
<div id="addCourseModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add Course</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <form id="addCourseForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" class="form-control" required>
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="course_name">Course Name:</label>
                                <input type="text" name="course_name" class="form-control" placeholder="Course Name" />
                            </div>
                            <div class="form-group">
                                <label for="course_acronym">Course Acronym:</label>
                                <input type="text" name="course_acronym" class="form-control" placeholder="e.g. BSIT" />
                            </div>
                            <div class="form-group">
                                <label for="course_description">Course Description:</label>
                                <textarea name="course_description" class="form-control" placeholder="Course Description" required></textarea>
                            </div>
                            <input type="hidden" name="action" value="addCourse" />
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Course Modal [End] -->

<!-- Update Course Modal [Start] -->
<div id="updateCourseModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Update Course</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <form id="updateCourseForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select id="u_branch" name="branch" class="form-control" required>
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="course_name">Course Name:</label>
                                <input type="text" id="u_course_name" name="course_name" class="form-control" placeholder="Course Name" />
                            </div>
                            <div class="form-group">
                                <label for="course_acronym">Course Acronym:</label>
                                <input type="text" id="u_course_acronym" name="course_acronym" class="form-control" placeholder="e.g. BSIT" />
                            </div>
                            <div class="form-group">
                                <label for="course_description">Course Description:</label>
                                <textarea id="u_course_description" name="course_description" class="form-control" placeholder="Course Description" required></textarea>
                            </div>
                            <input type="hidden" id="u_id" name="u_id" />
                            <input type="hidden" name="action" value="updateCourse" />
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Update Course Modal [End] -->

<!-- Delete Course Modal [Start] -->
<div id="deleteCourseModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delete Course</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success yes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Delete Course Modal [End] -->


<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->