<div id="facultyEvaluationsModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="margin-top: 100px !important;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="text-center">
                    <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/><br/>
                    <h3><b><span id="ProfessorName"></span></b></h3>
                </div>
                <form method="get" action="evaluated.php">
                    <input type="hidden" name="evaluation_type" value="student" />
                    <input type="hidden" id="f_id" name="f_id" />
                    <button type="submit" class="btn btn-lg btn-block btn-default">
                        <span class="pull-left">
                            <img src="<?php echo $base_url ?>/assets/imgs/icons/students-icon.png" width="25px"/>
                        </span>
                        <span class="pull-right"><b><span id="stdsEvaluated"></span>/<span id="totalStds"></span></b></span>
                        Students
                    </button>
                </form>
                <form method="get" action="evaluated.php">
                    <input type="hidden" name="evaluation_type" value="peer" />
                    <input type="hidden" id="peer_f_id" name="f_id" />
                    <button type="submit" class="btn btn-lg btn-block btn-default">
                        <span class="pull-left">
                            <img src="<?php echo $base_url ?>/assets/imgs/icons/peer-icon.png" width="25px"/>
                        </span>
                        <span class="pull-right"><b><span id="peersEvaluated"></span>/5</b></span>
                        Peers
                    </button>
                </form>
                <form method="get" action="evaluated.php">
                    <input type="hidden" name="evaluation_type" value="supervisor" />
                    <input type="hidden" id="supervisor_f_id" name="f_id" />
                    <button type="submit" class="btn btn-lg btn-block btn-default">
                        <span class="pull-left">
                            <img src="<?php echo $base_url ?>/assets/imgs/icons/peer-icon.png" width="25px"/>
                        </span>
                        <span class="pull-right"><b><span id="supervisorEvaluated"></span>/1</b></span>
                        Supervisor
                    </button>
                </form>
                <form method="get" action="evaluated.php">
                    <input type="hidden" name="evaluation_type" value="self" />
                    <input type="hidden" id="self_f_id" name="f_id" />
                    <button type="submit" class="btn btn-lg btn-block btn-default">
                        <span class="pull-left">
                            <img src="<?php echo $base_url ?>/assets/imgs/icons/peer-icon.png" width="25px"/>
                        </span>
                        <span class="pull-right"><b><span id="selfEvaluated"></span>/1</b></span>
                        Self
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>