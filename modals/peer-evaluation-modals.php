<div id="addUpdatePeerScheduleModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-calendar"></i> Add Peer Evaluation Schedule</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="AddUpdatePeerScheduleForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" id="branch" class="form-control" required>
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">Bongabong Campus</option>
                                    <option value="MMC">Main Campus</option>
                                    <option value="MCC">Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="professor">Professor:</label>
                                <select name="f_id" class="form-control" id="f_id"></select>
                            </div>
                            <div class="form-group">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <button type="button" onclick="pickEvaluators()" class="btn btn-success"><i class="fa fa-random"></i> Random Pick Evaluators</button>
                                        </div>
                                        <div id="evaluators"></div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="start_date">Start Date:</label>
                                <input type="date" name="start_date" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date:</label>
                                <input type="date" name="end_date" class="form-control" />
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="semester">Semester:</label>
                                <input type="text" name="semester" value="<?php print($today_sem) ?>" class="form-control" readonly />
                            </div>
                            <div class="form-group">
                                <label for="semestral_term">Semestral Term:</label>
                                <input type="text" name="semestral_term" value="<?php print($today_sem_term) ?>" class="form-control" readonly />
                            </div>
                            <div class="form-group">
                                <label for="school_year">School Year:</label>
                                <input type="text" name="school_year" value="<?php print($today_sy) ?>" class="form-control" readonly />
                            </div>
                            <input type="hidden" name="action" value="AddUpdatePeerSchedule" />
                            <button type="submit" class="btn btn-success">Set Schedule</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="UpdatePeerScheduleModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-calendar"></i> Update Peer Evaluation Schedule</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="UpdatePeerScheduleForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <input type="hidden" id="a_branch" name="u_branch" />
                                <select name="branch" id="u_branch" class="form-control" disabled="true">
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">Bongabong Campus</option>
                                    <option value="MMC">Main Campus</option>
                                    <option value="MCC">Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="professor">Professor:</label>
                                <input type="hidden" id="a_u_f_id" name="u_f_id" />
                                <select id="u_f_id" name="f_id" class="form-control" disabled="true"></select>
                            </div>
                            <div class="form-group">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <h4>Evaluators</h4>
                                        </div>
                                        <div style="margin: 15px !important;"></div>
                                        <div id="u_evaluators"></div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="start_date">Start Date:</label>
                                <input type="date" id="u_start_date" name="start_date" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date:</label>
                                <input type="date" id="u_end_date" name="end_date" class="form-control" />
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="semester">Semester:</label>
                                <input type="text" id="u_semester" name="semester" value="<?php print($today_sem) ?>" class="form-control" readonly />
                            </div>
                            <div class="form-group">
                                <label for="semestral_term">Semestral Term:</label>
                                <input type="text" id="u_semestral_term" name="semestral_term" value="<?php print($today_sem_term) ?>" class="form-control" readonly />
                            </div>
                            <div class="form-group">
                                <label for="school_year">School Year:</label>
                                <input type="text" id="u_school_year" name="school_year" value="<?php print($today_sy) ?>" class="form-control" readonly />
                            </div>
                            <input type="hidden" name="action" value="UpdatePeerSchedule" />
                            <button type="submit" class="btn btn-success">Update Schedule</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Student Modal [Start] -->
<div id="deleteScheduleModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delete Schedule</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this schedule?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger yes">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>                        
            </div>
        </div>
    </div>
</div>
<!-- Delete Student Modal [End] -->

<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->