<div id="addDesignationModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add Designation</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="addDesignationForm">
                            <div class="form-group">
                                <label for="designation">Designation:</label>
                                <input type="text" name="designation" class="form-control" required />
                            </div>
                            <input type="hidden" name="action" value="addDesignation" />
                            <button type="submit" class="btn btn-success">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="updateDesignationModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Update Designation</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="updateDesignationForm">
                            <div class="form-group">
                                <label for="designation">Designation:</label>
                                <input type="text" id="u_designation" name="designation" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="peer">Peer:</label>
                                <select id="u_peer" name="peer" class="form-control" required>
                                    <option value="">-- choose peer --</option>
                                    <?php
                                        $peers = $db_con->prepare("SELECT * FROM designations");
                                        $peers->execute();
                                        while($peer_data = $peers->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                    <option value="<?php echo $peer_data['id'] ?>"><?php echo $peer_data['designation'] ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="supervisor">Supervisor:</label>
                                <select id="u_supervisor" name="supervisor" class="form-control" required>
                                    <option value="">-- choose peer --</option>
                                    <?php
                                        $supervisor = $db_con->prepare("SELECT * FROM designations");
                                        $supervisor->execute();
                                        while($supervisor_data = $supervisor->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                    <option value="<?php echo $supervisor_data['id'] ?>"><?php echo $supervisor_data['designation'] ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <input type="hidden" id="u_id" name="u_id" />
                            <input type="hidden" name="action" value="updateDesignation" />
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="successModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center color-green"><i class="fa fa-check"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->