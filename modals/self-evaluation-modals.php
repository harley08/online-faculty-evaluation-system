<div id="AddSelfScheduleModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Add Self Schedule</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="addSelfScheduleForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" id="branch" class="form-control">
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">Bongabong Campus</option>
                                    <option value="MMC">Main Campus</option>
                                    <option value="MCC">Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="f_id">Professor</label>
                                <select name="f_id" id="professors" class="form-control">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="start_date">Start Date:</label>
                                <input type="date" name="start_date" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date:</label>
                                <input type="date" name="end_date" class="form-control" required />
                            </div>
                            <input type="hidden" name="action" value="addUpdateSelfSchedule" />
                            <button type="submit" class="btn btn-success">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="updateSelfScheduleModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Update Self Schedule</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="updateSelfScheduleForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <input type="hidden" name="a_branch" id="a_u_branch" />
                                <select name="branch" id="u_branch" class="form-control" disabled="true">
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">Bongabong Campus</option>
                                    <option value="MMC">Main Campus</option>
                                    <option value="MCC">Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="f_id">Professor</label>
                                <select name="f_id" id="u_professors" class="form-control" disabled="true">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="start_date">Start Date:</label>
                                <input type="date" id="u_start_date" name="start_date" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date:</label>
                                <input type="date" id="u_end_date" name="end_date" class="form-control" required />
                            </div>
                            <input type="hidden" name="action" value="UpdateSelfSchedule" />
                            <input type="hidden" id="u_id" name="u_id" />
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="SetSelfEvaluationByCampusModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-calendar"></i> Set Self Schedule By Campus</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="setSelfEvaluationByCampusForm">
                            <div class="form-group">
                                <label for="branch">Branch</label>
                                <select name="branch" class="form-control" required>
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">Bongabong Campus</option>
                                    <option value="MMC">Main Campus</option>
                                    <option value="MCC">Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="start_date">Start Date:</label>
                                <input type="date" name="start_date" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date:</label>
                                <input type="date" name="end_date" class="form-control" required />
                            </div>
                            <input type="hidden" name="action" value="UpdateSelfEvaluationByCampus" />
                            <button type="submit" class="btn btn-success">Update Schedule</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Student Modal [Start] -->
<div id="deleteSelfSchedModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delete Schedule</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this schedule?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger yes">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>                        
            </div>
        </div>
    </div>
</div>
<!-- Delete Student Modal [End] -->

<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->