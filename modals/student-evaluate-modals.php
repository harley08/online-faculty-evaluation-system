<div id="continueModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="margin-top: 100px !important;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="text-center">
                    <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/><br/>
                    <h3>Evaluating Prof. <b><span id="ProfessorName"></span></b></h3>
                    <div class="alert alert-danger">
                        <i class="fa fa-info-circle"></i> <b>Note:</b> Please read the instruction first. Read the questions carefully and rate appropriately base on your professor's performance! Thank you!
                    </div>
                    <form method="get" action="<?php echo $base_url ?>/application/evaluating">
                        <input type="hidden" id="f_id" name="professor_id" />
                        <input type="hidden" id="evaluation_type" name="evaluation_type" value="student" />
                        <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-pencil"></i> Evaluate Now!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myCertificateModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="printArea">
                <div class="panel panel-default" style="border-bottom-left-radius: 5px !important; border-bottom-right-radius: 5px !important; margin-bottom: 0px !important;">
                    <img src="<?php echo $base_url ?>/assets/imgs/minscat-header.png" width="100%" />
                    <div style="border-top: 1px solid #049900; border-bottom: 1px solid #049900; padding: 10px;">
                        <div class="text-center">
                            <h4 class="modal-title">CERTIFICATE OF COMPLETION</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        This is to certify that <b><?php print($my_name) ?></b> (<?php print($my_school_id) ?>) has successfully completed the evaluation for all of his/her subjects this School Year of <b><?php print($today_sy) ?></b>, <b><?php print($today_sem) ?></b> - <b><?php print($today_sem_term) ?></b>.
                    </div>
                </div>
                </div>
                <div style="margin: 15px"></div>
                <div class="text-center">
                    <button type="button" onclick="printDiv('printArea')" class="btn btn-success"><i class="fa fa-print"></i> Print Certificate</button>
                </div>
            </div>
        </div>
    </div>
</div>