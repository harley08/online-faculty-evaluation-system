<div id="successEvaluateModal" class="modal fade" data-backdrop="static"data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="<?php echo $base_url ?>" class="close">&times;</a>
                <h4 class="modal-title"><i class="fa fa-thumbs-o-up"></i> Evaluation Submitted</h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h4><i class="fa fa-check"></i> Thank you for cooperation. Your evaluation was successfully submitted!</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="instructionModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> Instruction</h4>
            </div>
            <div class="modal-body">
                <p>Please evaluate the faculty using the scale below.</p>
                    <table class="table table-bordered" style="margin-bottom: 0px !important;">
                        <thead>
                            <tr>
                                <th class="text-center">Scale</th>
                                <th class="text-center">Descriptive Rating</th>
                                <th class="text-center">Qualitative Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"><b>5</b></td>
                                <td class="text-center">Outstanding</td>
                                <td class="text-center">The performance almost always exceeds job requirements. The faculty is an exceptional role model.</td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>4</b></td>
                                <td class="text-center">Very Satisfactory</td>
                                <td class="text-center">The performance meets and often exceeds the job requirements.</td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>3</b></td>
                                <td class="text-center">Satisfactory</td>
                                <td class="text-center">The performance meets the job requirements.</td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>2</b></td>
                                <td class="text-center">Fair</td>
                                <td class="text-center">The performance needs some development to meet job requirements.</td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>1</b></td>
                                <td class="text-center">Poor</td>
                                <td class="text-center">The faculty fails to meet the job requirements.</td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>