<!-- Add Category Modal [Start] -->
<div id="addCategoryModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="addCategoryForm">
                            <div class="form-group">
                                <label for="categoryName">Category Name:</label>
                                <input type="text" class="form-control" name="catagory_name" placeholder="Category Name" required />
                            </div>
                            <div class="form-group">
                                <label for="percentage">Percentage:</label>
                                <input type="text" name="percentage" class="form-control" placeholder="e.g. 0.25" required /> 
                            </div>
                            <div class="form-group">
                                <label for="status">Status:</label>
                                <select name="status" class="form-control" required>
                                    <option value="">-- choose status --</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                </select>
                            </div>
                            <input type="hidden" name="action" value="addCategory" />
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Category Modal [End] -->

<!-- Update Category Modal [Start] -->
<div id="updateCategoryModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Update Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="updateCategoryForm">
                            <div class="form-group">
                                <label for="categoryName">Category Name:</label>
                                <input type="text" class="form-control" id="u_category_name" name="category_name" placeholder="Category Name" required />
                            </div>
                            <div class="form-group">
                                <label for="percentage">Percentage:</label>
                                <input type="text" id="u_percentage" name="percentage" class="form-control" placeholder="e.g. 0.25" required /> 
                            </div>
                            <div class="form-group">
                                <label for="status">Status:</label>
                                <select id="u_status" name="status" class="form-control" required>
                                    <option value="">-- choose status --</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                </select>
                            </div>
                            <input type="hidden" name="action" value="updateCategory" />
                            <input type="hidden" id="u_id" name="u_id" />
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Update Category Modal [End] -->

<!-- Delete Category [Start] -->
<div id="deleteCategoryModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delete Category</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this category?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger yes">Yes</button>
                <button type="button" class="btn btn-primary">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Delete Category [End] -->

<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->