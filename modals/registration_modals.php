

<div id="verifiedStudentModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title"><i class="fa fa-check"></i> Now! I know who you are!</h4>
            </div>
            <div class="modal-body">
                <form id="createStudentAccountForm">
                    <h5 class="text-center">Create Your Account Now!</h5>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="username">This will be your username:</label>
                                <input type="text" name="username" id="username" readonly class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" name="password" class="form-control" data-toggle="password" />
                            </div>
                            <div class="form-group">
                                <label for="password_confirm">Re-type Password:</label>
                                <input type="password" name="password_confirm" class="form-control" data-toggle="password" />
                            </div>
                            <input type="hidden" name="id" id="id" />
                            <input type="hidden" name="action" value="createStudentAccount" />
                            <button type="submit" class="btn btn-primary btn-block">Create Account</button>
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="verifiedFacultyModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title"><i class="fa fa-check"></i> Now! I know who you are!</h4>
            </div>
            <div class="modal-body">
                <form id="createFacultyAccountForm">
                    <h5 class="text-center">Create Your Account Now!</h5>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="username">This will be your username:</label>
                                <input type="text" name="username" id="fac_username" readonly class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" name="password" class="form-control" data-toggle="password" />
                            </div>
                            <div class="form-group">
                                <label for="password_confirm">Re-type Password:</label>
                                <input type="password" name="password_confirm" class="form-control" data-toggle="password" />
                            </div>
                            <input type="hidden" id="fac_id" name="id" />
                            <input type="hidden" name="action" value="createFacultyAccount" />
                            <button type="submit" class="btn btn-primary btn-block">Create Account</button>
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="accountSuccessModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="text-center"><i class="fa fa-check"></i> Account successfully created! Activate your account now!</h4>
                <div class="text-center">
                    <a href="<?php echo $base_url ?>/application/verify-account" class="btn btn-default">Activate Now!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Success Modal [Start] -->
<div id="successModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center color-green"><span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->