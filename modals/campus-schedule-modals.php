<div id="updateCampusSchedModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-calendar-o"></i> Update Campus Schedule</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="updateCampusSchedForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <input type="text" id="u_branch" name="branch" class="form-control" readonly />
                            </div>
                            <div class="form-group">
                                <label for="start_date">Start Date:</label>
                                <input type="date" id="u_start_date" name="start_date" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for=end_date">End Date:</label>
                                <input type="date" id="u_end_date" name="end_date" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="staus">Status:</label>
                                <select id="u_status" name="status" class="form-control">
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                </select>
                            </div>
                            <input type="hidden" id="u_id" name="u_id" />
                            <input type="hidden" name="action" value="UpdateCampusSched" />
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->