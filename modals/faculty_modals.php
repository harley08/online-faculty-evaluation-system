<!-- Add Faculty Modal [Start] -->
<div id="addFacultyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addFacultyModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user-plus"></i> Add Faculty</h4>
            </div>
            <div class="modal-body">
                <form id="addFacultyForm">
                    <div class="row">
                        <div class="col-md-5 pc">
                            <div class="text-center">
                                <img id="selected_avatar" class="avatar" src="../../assets/imgs/avatar/no-avatar.jpg" width="60%" />
                                <div id="loadImgSelected" style="display: none;">
                                    Loading <img src="<?php echo $base_url ?>/assets/imgs/gif/loader-small.gif" alt=""/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="avatar">Choose Avatar:</label>
                                <input type="file" name="avatar" id="select_avatar" />
                            </div>
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" required class="form-control">
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus (Labasan, Bongabong)</option>
                                    <option value="MMC">MinSCAT Main Campus (Alcate, Victoria)</option>
                                    <option value="MCC">MinSCAT Calapan City Campus(Calapan City)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" style="margin-bottom: 5px;" />
                                <input type="text" class="form-control" id="middlename" name="middlename" placeholder="Middle Name" style="margin-bottom: 5px;" />
                                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" />
                            </div>
                            <div class="form-group">
                                <label for="employment_status">Employment Status:</label>
                                <select class="form-control" id="employment_status" required name="employment_status">
                                    <option value="">-- choose status --</option>
                                    <option value="Full Time">Full Time</option>
                                    <option value="Part-Time">Part-Time</option>
                                    <option value="Temporary">Temporary</option>
                                    <option value="Permanent">Permanent</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="contact_details">Contact Details:</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" style="margin-bottom: 5px;" />
                                <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" />
                            </div>
                            <div class="form-group">
                                <label for="designation">Designation:</label>
                                <div class="input-group my-group">
                                    <select name="designation" class="form-control" style="width: 200px !important">
                                        <option value="">-- select designation --</option>
                                        <?php
                                            $designation = $db_con->prepare("SELECT * FROM designations");
                                            $designation->execute();
                                            if ($designation->rowCount() != null) {
                                                while($designation_data = $designation->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                        <option value="<?php echo $designation_data['id'] ?>"><?php echo $designation_data['designation'] ?></option>
                                                    <?php
                                                }                                                
                                            } else {
                                                ?>
                                        <option value="">none</option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                    <select name="desig_level" class="form-control" style="width: 78px">
                                        <option value="0">none</option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                        <option value="5">V</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role">Role:</label>
                                <select name="role" id="role" class="form-control" required>
                                    <option value="">-- choose role --</option>
                                    <option value="Professor">Professor</option>
                                    <option value="Advisor">Supervisor</option>
                                </select>
                            </div>
                            <input type="hidden" name="action" value="AddFaculty" />
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                        </div>
                    </div>
                </form>
            </div>            
        </div>
    </div>
</div>
<!-- Add Faculty Modal [End] -->
        
<!-- Edit Faculty Modals [Start] -->
<div id="editFacultyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addFacultyModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user"></i> Update Faculty</h4>
            </div>
            <div class="modal-body">
                <form id="updateFacultyForm">
                    <div class="row">
                        <div class="col-md-5 pc">
                            <div class="text-center">
                                <img style="margin-top: 70px; margin-bottom: 20px; border: 5px solid #c0c0c0; border-radius: 50%; background-color: #c0c0c0;" src="../../assets/imgs/avatar/no-avatar-male.png" width="60%" />
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" required class="form-control" id="u_branch">
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus (Labasan, Bongabong)</option>
                                    <option value="MMC">MinSCAT Main Campus (Alcate, Victoria)</option>
                                    <option value="MCC">MinSCAT Calapan City Campus(Calapan City)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="u_firstname" placeholder="First Name" style="margin-bottom: 5px;" name="firstname" />
                                <input type="text" class="form-control" id="u_middlename" placeholder="Middle Name" style="margin-bottom: 5px;" name="middlename" />
                                <input type="text" class="form-control" id="u_lastname" placeholder="Last Name" name="lastname" />
                            </div>
                            <div class="form-group">
                                <label for="employment_status">Employment Status:</label>
                                <select class="form-control" id="u_employment_status" required name="employment_status">
                                    <option value="">-- choose status --</option>
                                    <option value="Full Time">Full Time</option>
                                    <option value="Part-Time">Part-Time</option>
                                    <option value="Temporary">Temporary</option>
                                    <option value="Permanent">Permanent</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="contact_details">Contact Details:</label>
                                <input type="email" class="form-control" id="u_email" placeholder="Email Address" style="margin-bottom: 5px;" name="email" />
                                <input type="text" class="form-control" id="u_mobile" placeholder="Mobile Number" name="mobile" />
                            </div>
                            <div class="form-group">
                                <label for="role">Role:</label>
                                <select id="u_role" class="form-control" required name="role">
                                    <option value="">-- choose role --</option>
                                    <option value="Professor">Professor</option>
                                    <option value="Advisor">Supervisor</option>
                                </select>
                            </div>
                            <input type="hidden" name="u_id" id="u_id" >
                            <input type="hidden" name="action" value="updateFaculty">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                        </div>
                    </div>
                </form>
            </div>            
        </div>
    </div>
</div>
<!-- Edit Faculty Modals [End] -->

<!-- Delete Faculty Modal [Start] -->
<div id="deleteFacultyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addFacultyModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delele Faculty</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this faculty?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger yes">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>                        
            </div>
        </div>
    </div>
</div>
<!-- Delete Faculty Modal [End] -->

<!-- Show Faculty Info [Start] -->
<div id="showFacInfoModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span id="show_fullname"></span></h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="100px"/>
                <div style="margin: 20px;"></div>
                <ul class="list-group">
                    <li class="list-group-item">Designation: <b><span id="show_designation"></span></b></li>
                    <li class="list-group-item">Highest Educational Attainment: <b><span id="show_education"></span></b></li>
                    <li class="list-group-item">Mobile: <b><span id="show_mobile"></span></b></li>
                    <li class="list-group-item">Email: <b><span id="show_email"></span></b></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Show Faculty Info [End] -->

<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->