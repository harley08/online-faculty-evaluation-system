<div id="addEvaluatorModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add Evaluation Type</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="addEvaluatorForm">
                            <div class="form-group">
                                <label for="evaluator">Evaluator:</label>
                                <input type="text" name="evaluator" class="form-control" placeholder="e.g. student" />
                            </div>
                            <div class="form-group">
                                <label for="evaluator">Percentage:</label>
                                <input type="text" name="percentage" class="form-control" placeholder="e.g. .20" />
                            </div>
                            <input type="hidden" name="action" value="addEvaluatorType" />
                            <button type="submit" class="btn btn-success">Add</button>
                            <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="updateEvaluatorModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Update Evaluation Type</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-5">
                        <form id="updateEvaluatorForm">
                            <div class="form-group">
                                <label for="evaluator">Evaluator:</label>
                                <input type="text" id="u_evaluator" name="evaluator" class="form-control" placeholder="e.g. student" />
                            </div>
                            <div class="form-group">
                                <label for="evaluator">Percentage:</label>
                                <input type="text" id="u_percentage" name="percentage" class="form-control" placeholder="e.g. .20" />
                            </div>
                            <div class="form-group">
                                <label class="total_evaluators">Total Evaluators:</label>
                                <input type="number" id="u_total_evaluators" name="total_evaluators" class="form-control" />
                            </div>
                            <input type="hidden" id="u_id" name="u_id" />
                            <input type="hidden" name="action" value="updateEvaluatorType" />
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->