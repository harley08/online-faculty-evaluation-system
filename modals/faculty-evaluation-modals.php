<div id="continueModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="margin-top: 100px !important;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="text-center">
                    <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/><br/>
                    <h3>Evaluating Prof. <b><span id="ProfessorName"></span></b></h3>
                    <div class="alert alert-danger">
                        <i class="fa fa-info-circle"></i> <b>Note:</b> Please read the instruction first. Read the questions carefully and rate appropriately base on your peer's performance! Thank you!
                    </div>
                    <form method="get" action="<?php echo $base_url ?>/application/evaluating">
                        <input type="hidden" id="f_id" name="professor_id" />
                        <input type="hidden" id="evaluation_type" name="evaluation_type" value="peer" />
                        <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-pencil"></i> Evaluate Now!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="continueSelfEvaluateModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="margin-top: 100px !important;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="text-center">
                    <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/><br/>
                    <h3>Evaluating Prof. <b><span id="self_ProfessorName"></span></b> (Self)</h3>
                    <div class="alert alert-danger">
                        <i class="fa fa-info-circle"></i> <b>Note:</b> Please read the instruction first. Read the questions carefully and rate appropriately base on your peer's performance! Thank you!
                    </div>
                    <form method="get" action="<?php echo $base_url ?>/application/evaluating">
                        <input type="hidden" id="self_f_id" name="professor_id" />
                        <input type="hidden" id="evaluation_type" name="evaluation_type" value="self" />
                        <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-pencil"></i> Evaluate Now!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>