<!-- Success Modal [Start] -->
<div id="successModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center color-green"><i class="fa fa-check"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->

<!-- Add Subject Modal [Start] -->
<div id="addSubjectModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add Subject</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <form id="addSubjectForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" id="branch" class="form-control" required>
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="course">Course:</label>
                                <select name="course" id="coursesPerBranch" class="form-control" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="curriculum_year">Curriculum Year:</label>
                                <select name="curriculum_year" class="form-control" required>
                                    <option value="">-- choose year --</option>
                                    <option value="1">1st Year</option>
                                    <option value="2">2nd Year</option>
                                    <option value="3">3rd Year</option>
                                    <option value="4">4th Year</option>
                                    <option value="5">5th Year</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="subject_code">Subject Code:</label>
                                <input type="text" name="subject_code" class="form-control" required placeholder="Subject Code" />
                            </div>
                            <div class="form-group">
                                <label for="subject_description">Subject Description:</label>
                                <textarea name="subject_description" class="form-control" required placeholder="Subject Description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="subject_description">Professor:</label>
                                <select id="professors" name="f_id" class="form-control" required>
                                </select>
                            </div>
                            <input type="hidden" name="action" value="addSubject" />
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Subject Modal [End] -->

<!-- Update Subject Modal [Start] -->
<div id="updateSubjectModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i> Update Subject</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <form id="updateSubjectForm">
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select id="u_branch" name="branch" class="form-control" disabled="true" required>
                                    <option value="">-- select branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="course">Course:</label>
                                <select id="u_courses" name="course" class="form-control" disabled="true" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="curriculum_year">Curriculum Year:</label>
                                <select id="u_curriculum_year" name="curriculum_year" class="form-control" disabled="true" required>
                                    <option value="">-- choose year --</option>
                                    <option value="1">1st Year</option>
                                    <option value="2">2nd Year</option>
                                    <option value="3">3rd Year</option>
                                    <option value="4">4th Year</option>
                                    <option value="5">5th Year</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="subject_code">Subject Code:</label>
                                <input type="text" id="u_subject_code" name="subject_code" class="form-control" required placeholder="Subject Code" />
                            </div>
                            <div class="form-group">
                                <label for="subject_description">Subject Description:</label>
                                <textarea id="u_subject_description" name="subject_description" class="form-control" required placeholder="Subject Description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="subject_description">Professor:</label>
                                <select id="u_professors" name="f_id" class="form-control" required>
                                </select>
                            </div>
                            <input type="hidden" name="action" value="updateSubject" />
                            <input type="hidden" id="u_id" name="u_id" />
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Update Subject Modal [End] -->

<!-- Delete Subject Modal [Start] -->
<div id="deleteSubjectModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addFacultyModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delele Subject</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this subject?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger yes">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>                        
            </div>
        </div>
    </div>
</div>
<!-- Delete Subject Modal [End] -->