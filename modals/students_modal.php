<!-- Add Student Modal [Start] -->
<div id="addStudentModal" class="modal fade" tabindex="-1" role="dialog">    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user-plus"></i> Add Student</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5 pc">
                        <div class="text-center">
                            <div class="text-center">
                                <div class="avatar_cover">
                                    <img id="selected_avatar" class="header-avatar" src="../../assets/imgs/avatar/no-avatar-student.png" width="105%" />
                                </div>
                            </div>
                            <input type="file" id="select_avatar" onchange="onImgSelected(event)" style="display: none;" />
                            <div style="margin: 20px;"></div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form id="addStudentForm">
                            <div class="form-group">
                                <label for="branch">Branch</label>
                                <select name="branch" id="selectedBranch_loadCourse" class="form-control" required>
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="firstname" class="form-control" placeholder="First Name" required style="margin-bottom: 5px;" />
                                <input type="text" name="middlename" class="form-control" placeholder="Middle Name" required style="margin-bottom: 5px;" />
                                <input type="text" name="lastname" class="form-control" placeholder="Last Name" required />
                            </div>
                            <div class="form-group">
                                <label for="school_id">School ID:</label>
                                <input type="text" class="form-control" name="school_id" placeholder="0000-0000" required />
                            </div>
                            <div class="form-group">
                                <label for="course">Couse:</label>
                                <select name="course" class="form-control" id="loadCourseByBranch" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="curriculum_year">Curriculum Year:</label>
                                <select name="curriculum_year" class="form-control" required>
                                    <option value="">-- choose year --</option>
                                    <option value="1">1st Year</option>
                                    <option value="2">2nd Year</option>
                                    <option value="3">3rd Year</option>
                                    <option value="4">4th Year</option>
                                    <option value="5">5th Year</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact:</label>
                                <input type="email" class="form-control" name="email" required style="margin-bottom: 5px" placeholder="Email Address" />
                                <input type="text" name="mobile_no" placeholder="Mobile Number" class="form-control" required />
                            </div>                                    
                            <input type="hidden" name="action" value="AddStudent" />
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>
<!-- Add Student Modal [End] -->
        
<!-- Update Student Modal [Start] -->
<div id="updateStudentModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-user"></i> Update Student</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5 pc">
                        <div class="text-center">
                            <img style="margin-top: 70px; margin-bottom: 20px; border: 5px solid #c0c0c0; border-radius: 50%; background-color: #c0c0c0;" src="../../assets/imgs/avatar/no-avatar-student.png" width="60%" />
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form id="updateStudentForm">
                            <div class="form-group">
                                <label for="branch">Branch</label>
                                <select id="u_branch" name="branch" class="form-control" required>
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="firstname" id="u_firstname" class="form-control" placeholder="First Name" required style="margin-bottom: 5px;" />
                                <input type="text" name="middlename" id="u_middlename" class="form-control" placeholder="Middle Name" required style="margin-bottom: 5px;" />
                                <input type="text" name="lastname" id="u_lastname" class="form-control" placeholder="Last Name" required />
                            </div>
                            <div class="form-group">
                                <label for="school_id">School ID:</label>
                                <input type="text" class="form-control" name="school_id" id="u_school_id" placeholder="0000-0000" required />
                            </div>
                            <div class="form-group">
                                <label for="course">Couse:</label>
                                <select name="course" id="u_course" class="form-control" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="curriculum_year">Curriculum Year:</label>
                                <select name="curriculum_year" id="u_curriculum_year" class="form-control" required>
                                    <option value="">-- choose year --</option>
                                    <option value="1">1st Year</option>
                                    <option value="2">2nd Year</option>
                                    <option value="3">3rd Year</option>
                                    <option value="4">4th Year</option>
                                    <option value="5">5th Year</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact:</label>
                                <input type="email" class="form-control" name="email" id="u_email" required style="margin-bottom: 5px" placeholder="Email Address" />
                                <input type="text" name="mobile_no" id="u_mobile_no" placeholder="Mobile Number" class="form-control" required />
                            </div>
                            <input type="hidden" id="u_id" name="u_id" >
                            <input type="hidden" name="action" value="updateStudent">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Update Student Modal [End] -->
        
<!-- Delete Student Modal [Start] -->
<div id="deleteStudentModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-remove"></i> Delete Student</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this student?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger yes">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>                        
            </div>
        </div>
    </div>
</div>
<!-- Delete Student Modal [End] -->
     
<!-- Success Modal [Start] -->
<div id="successModal" class="modal modal-center fade" tabindex="-1">
    <div class="modal-dialog modal-center-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
            </div>
        </div>
    </div>
</div>
<!-- Success Modal [End] -->