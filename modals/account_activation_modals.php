<div id="successActivateModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="text-center">
                    <i class="fa fa-check"></i> Account successfully activated! <a href="<?php echo $base_url ?>">Login now</a>.
                </h4>
            </div>
        </div>
    </div>
</div>