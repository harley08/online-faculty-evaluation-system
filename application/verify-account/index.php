<?php
    include '../../config/config.php';
    $page_title = 'Verify Account';
    include '../../template/header.php';
    
    if ($logged_in) {
        header("Location: $base_url/application/dashboard");
    }
?>

        <style>
            .footer {
                display: none;
            }
        </style>
        <div class="container">
            <div class="form-signin">
                <div class="text-center">
                    <a href="<?php echo $base_url ?>">
                        <img src="<?php echo $base_url ?>/assets/imgs/rateme-banner.png" width="60%" />
                    </a>
                </div>
                <div style="height: 60px;"></div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                            <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/>
                        </div>
                        <hr/>
                        <form id="activateAccountForm">
                            <div class="alert alert-danger">
                                You must activate your account first!
                            </div>
                            <div class="form-group">
                                <label for="user_code">Enter your School ID/Faculty/User Code:</label>
                                <input type="text" name="user_code" class="form-control" required placeholder="0000-0000" />
                            </div>
                            <div class="form-group">
                                <label for="activation_code">Enter your account activation code:</label>
                                <input type="text" name="activation_code" class="form-control" required placeholder="e.g. xHyhbgFr" />
                            </div>
                            <input type="hidden" name="action" value="activateAccount" />
                            <button type="submit" class="btn btn-success btn-block">Activate Account</button>
                        </form>
                    </div>
                </div>
                <center>
                    <p>2017-2018. All Rights Reserved. RateME</p>
                </center>
            </div>
        </div>

        <?php include '../../modals/account_activation_modals.php'; ?>
        
        <script src="../../ajax/account_activation_ajax.js" type="text/javascript"></script>

<?php
    include '../../template/footer.php';
?>