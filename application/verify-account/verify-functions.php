<?php
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        
        $action = $_POST['action'];
        switch($action) {
            case 'activateAccount':
                activateAccount();
                break;
            default:
                #codes here
                break;
        }
        
    }
    
    function activateAccount() {
        
        include '../../config/config.php';
        
        $user_code = $_POST['user_code'];
        $activation_code = $_POST['activation_code'];
        
        $data->activateAccount($user_code, $activation_code);
    }
?>