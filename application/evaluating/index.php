<?php
    $page_title = 'Evaluating';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
    
    if (isset($_GET['professor_id']) && isset($_GET['evaluation_type'])) {
        
        $evaluation_type = $_GET['evaluation_type'];
        $f_id = $_GET['professor_id'];
        
        $get_professor = $db_con->prepare("SELECT * FROM faculties WHERE f_id = :id");
        $get_professor->bindparam(':id', $f_id);
        $get_professor->execute();
        
        $profData = $get_professor->fetch(PDO::FETCH_ASSOC);
        
    }
?>

        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo $base_url ?>">Dashboard</a></li>
                <li>Evaluate Professor</li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Evaluation Form</h4>
                </div>
                <div class="panel-body" style="padding: 5px !important;">
                    <table width="100%" style="margin: 10px;">
                        <tr>
                            <td align="right" width="10%" style="padding-right: 10px;" valign="top">
                                <img src="../../assets/imgs/avatar/no-avatar-male.png" width="50px" style="background-color: #e6e6e6;" />
                            </td>
                            <td align="left" width="90%" valign="top">
                                <b><?php print($profData['firstname'] . " " . $profData['lastname']) ?></b><br/>
                                <b>Subjects: </b> 
                                <?php
                                    $get_subjects = $db_con->prepare("SELECT * FROM subjects WHERE f_id = :id AND branch = :branch AND course = :course AND curriculum_year = :cy");
                                    $get_subjects->bindparam(':id', $f_id);
                                    $get_subjects->bindparam(":branch", $my_branch);
                                    $get_subjects->bindparam(":course", $my_course);
                                    $get_subjects->bindparam(':cy', $my_curriculum_year);
                                    $get_subjects->execute();
                                    
                                    while ($subjects = $get_subjects->fetch(PDO::FETCH_ASSOC)) {
                                        
                                        echo $subjects['subject_description'] . ", ";
                                        
                                    }                                   
                                    
                                ?>
                            </td>
                        </tr>
                    </table>
                    <div id="accordion">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#instruction">
                            <div class="alert alert-success" style="padding: 5px !important; margin-bottom: 5px !important;"><i class="fa fa-info-circle"></i> Instruction <span class="pull-right"><i class="fa fa-arrow-down"></i></span></div>
                        </a>
                        <div class="collapse" id="instruction">    
                            <p>Please evaluate the faculty using the scale below.</p>
                    <table class="table table-bordered" style="margin-bottom: 0px !important;">
                        <thead>
                            <tr>
                                <th class="text-center">Scale</th>
                                <th class="text-center">Descriptive Rating</th>
                                <th class="text-center">Qualitative Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">5</td>
                                <td class="text-center">Outstanding</td>
                                <td class="text-center">The performance almost always exceeds job requirements. The faculty is an exceptional role model.</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center">Very Satisfactory</td>
                                <td class="text-center">The performance meets and often exceeds the job requirements.</td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td class="text-center">Satisfactory</td>
                                <td class="text-center">The performance meets the job requirements.</td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td class="text-center">Fair</td>
                                <td class="text-center">The performance needs some development to meet job requirements.</td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Poor</td>
                                <td class="text-center">The faculty fails to meet the job requirements.</td>
                            </tr>
                        </tbody>
                    </table>
                        <div style="margin: 3px;"></div>
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#instruction" style="background-color: 009100; border: 1px solid #009100; padding: 5px; color: #ffffff; display: block;" class="text-center"><i class="fa fa-arrow-up"></i> Hide</a>
                        <div style="margin-bottom: 10px !important;"></div>
                        </div>
                    </div>
                    <form id="evaluateForm">
                        <table class="table table-bordered" style="margin-bottom: 0px !important;">
                            <thead>
                                <tr>
                                    <th class="text-center" style="background-color: #fafafa;">QUESTIONS</th>
                                    <th class="text-center" style="background-color: #fafafa;">RATING</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $categories = $db_con->prepare("SELECT * FROM categories WHERE status = 'Active'");
                                $categories->execute();
                                
                                if ($categories->rowCount() != null) {
                                    
                                    while ($categ = $categories->fetch(PDO::FETCH_ASSOC)) {
                                        
                                        ?>
                                <tr>
                                    <td class="green-bg" colspan="2">
                                        
                                        <b><?php print($categ['category']) ?>.</b> <?php print($categ['category_name']) ?>
                                    </td>
                                </tr>
                                        <?php
                                        
                                        /* Get the Active Questions per Category */
                                        
                                        $questions = $db_con->prepare("SELECT * FROM questions WHERE category = :category AND status = 'Active'");
                                        $questions->bindparam(":category", $categ['category']);
                                        $questions->execute();
                                        
                                        if ($questions->rowCount() != null) {
                                           
                                            
                                            for ($i = 1; $i <= $questions->rowCount(); $i++) {
                                                
                                                $quest = $questions->fetch(PDO::FETCH_ASSOC);
                                                
                                                ?>
                                <tr>
                                    <td>
                                        <b><?php print($i) ?>.</b> <?php print($quest['question']) ?>
                                    </td>
                                    <td>
                                       <input type="hidden" value="<?=$quest['id']; ?>" name="qid[]"/>
                                       <input type="hidden" value="<?=$quest['category']; ?>" name="cat[]"/>
                                       <select name="rating[]" required>
                                            <option value="">-- rate --</option>
                                            <option value="5">5</option>
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                    </td>
                                </tr>
                                                <?php
                                                
                                            }
                                            
                                        } else {
                                            
                                            ?>
                                <tr>
                                    <td colspan="2" class="text-center"><b>no active questions in this category</b></td>
                                </tr>
                                            <?php
                                            
                                        }
                                        
                                    }
                                    
                                } else {
                                    
                                    ?>
                                <tr>
                                    <td colspan="2" class="text-center">no active categories</td>
                                </tr>
                                    <?php
                                    
                                }
                            ?>
                                <tr>
                                    <td colspan="2">
                                        <b>Comment/s</b> <i>(Optional)</i>
                                        <textarea class="form-control" name="comment" placeholder="Please type your comment here..."></textarea>
                                        <div style="margin: 10px;"></div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success">Submit Evaluation</button>
                                        </div>
                                        <div style="margin: 10px;"></div>
                                        <div class="alert alert-danger">
                                            <p>
                                                Your privacy and the confidentiality of this session will be treated with utmost importance and under no circumstances will we reveal/publish any of your personal information regarding this evaluation session. Thank you.
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="evaluation_type" value="<?php print($evaluation_type) ?>" />
                        <input type="hidden" name="branch" value="<?php print($my_branch) ?>" />
                        <input type="hidden" name="user_code" value="<?php print($my_userCode) ?>" />
                        <input type="hidden" name="f_id" value="<?php print($f_id) ?>" />
                        <input type="hidden" name="semester" value="<?php print($today_sem) ?>" />
                        <input type="hidden" name="semestral_term" value="<?php print($today_sem_term) ?>" />
                        <input type="hidden" name="school_year" value="<?php print($today_sy) ?>" />
                        <input type="hidden" name="action" value="evaluateFaculty" />
                    </form>
                </div>
            </div>
        </div>

        <?php include '../../modals/evaluating-modals.php'; ?>
        
        <script src="../../ajax/evaluating-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>