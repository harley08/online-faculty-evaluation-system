<?php
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        
        $action = $_POST['action'];        
        switch($action) {            
            case 'evaluateFaculty':
                evaluateFaculty();
                break;
            case 'getTheFacultyName':
                getTheFacultyName();
                break;
            default :
                break;            
        }
        
    }
    
    function evaluateFaculty() {
        
        include '../../config/config.php';
        
        $n_rates = count($_POST['rating']);
        $rating = $_POST['rating'];
        $cat = $_POST['cat'];
        $qid = $_POST['qid'];
        
        $comment = $_POST['comment'];
        $evaluation_type = $_POST['evaluation_type'];
        $branch = $_POST['branch'];
        $user_code = $_POST['user_code'];
        $f_id = $_POST['f_id'];
        $semester = $_POST['semester'];
        $semestral_term = $_POST['semestral_term'];
        $school_year = $_POST['school_year'];
        
        $data->evaluateFaculty($n_rates, $rating, $cat, $qid, $evaluation_type, $branch, $user_code, $f_id, $semester, $semestral_term, $school_year, $comment);
        
    }
    
    function getTheFacultyName() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $data->getTheFacultyName($id);
        
    }
?>