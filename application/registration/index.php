<?php
    include '../../config/config.php';
    $page_title = 'Registration';
    include '../../template/header.php';
    
    if ($logged_in) {
        header("Location: $base_url/application/dashboard");
    }
?>

        <style>
            .footer {
                display: none;
            }
        </style>
        <div class="container">
            <div class="form-signup">
                <div class="text-center">
                    <a href="<?php echo $base_url ?>">
                        <img src="<?php echo $base_url ?>/assets/imgs/rateme-banner.png" width="60%" />
                    </a>
                </div>
                <div style="height: 60px;"></div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                            <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/>
                        </div>
                        <hr/>
                        <form id="verifyForm">
                            <div class="alert alert-warning">Let me know you first!</div>
                            <div class="form-group">
                                <label for="branch">Branch:</label>
                                <select name="branch" id="selectBranch" class="form-control" required>
                                    <option value="">-- choose branch --</option>
                                    <option value="MBC">MinSCAT Bongabong Campus</option>
                                    <option value="MMC">MinSCAT Main Campus</option>
                                    <option value="MCC">MinSCAT Calapan City Campus</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" name="firstname" required placeholder="First Name" style="margin-bottom: 10px;" />
                                <input type="text" class="form-control" name="middlename" placeholder="Middle Name" style="margin-bottom: 10px;" />
                                <input type="text" class="form-control" name="lastname" required placeholder="Last Name" />
                            </div>
                            <div class="form-group">
                                <label>Register as:</label>
                                <select id="user" name="user" class="form-control" required>
                                    <option value="">-- choose --</option>
                                    <option value="faculty">Faculty</option>
                                    <option value="student_selected">Student</option>
                                </select>
                            </div>
                            <div class="register_option" id="student_selected" style="display: none">
                            <div class="form-group">
                                <label for="school_id">School ID:</label>
                                <input type="text" class="form-control" name="school_id" placeholder="School ID" />
                            </div>
                            <div class="form-group">
                                <label for="course">Course:</label>
                                <select name="course" id="coursesData" class="form-control">
                                </select>
                            </div>
                            </div>
                            <input type="hidden" name="action" value="check_user" />
                            <button type="submit" class="btn btn-success btn-block">Submit</button>
                        </form>
                    </div>
                </div>
                <center>
                    <a href=""><i class="glyphicon glyphicon-question-sign"></i> Forgot password?</a><br><br>
                    <p>2017-2018. All Rights Reserved. RateME</p>
                </center>
            </div>
        </div>
        
        <?php
            include '../../modals/registration_modals.php';
        ?>
        
        
        <script src="../../ajax/registration_ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>