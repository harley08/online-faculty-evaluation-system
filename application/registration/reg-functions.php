<?php
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        
        $action = $_POST['action'];
        
        switch($action) {
            case 'check_user':
                checkUser();
                break;
            case 'createStudentAccount':
                createStudentAccount();
                break;
            case 'createFacultyAccount':
                createFacultyAccount();
                break;
            case 'loadCoursesPerBranch':
                loadCoursesPerBranch();
                break;
            default:
                #codes here
                break;
        }
        
    }
    
    function checkUser() {
        
        include '../../config/config.php';
        
        $user = $_POST['user'];
        
        if ($user == 'student_selected') {
            
            $branch = $_POST['branch'];
        
            $firstname = $_POST['firstname'];
            $middlename = $_POST['middlename'];
            $lastname = $_POST['lastname'];
        
            $school_id = $_POST['school_id'];
            $course = $_POST['course'];
            
            $data->checkStudent($branch, $firstname, $middlename, $lastname, $school_id, $course);
            
        } else if ($user == 'faculty') {
            
            $branch = $_POST['branch'];
        
            $firstname = $_POST['firstname'];
            $middlename = $_POST['middlename'];
            $lastname = $_POST['lastname'];
            
            $data->checkFaculty($branch, $firstname, $middlename, $lastname);
            
        }
        
    }
    
    function createStudentAccount() {
        
        include '../../config/config.php';
        
        $username = $_POST['username'];
        $password = $_POST['password'];
        $id = $_POST['id'];
        
        $data->createStudentAccount($id, $username, $password);
        
    }
    
    function createFacultyAccount() {
        
        include '../../config/config.php';
        
        $username = $_POST['username'];
        $password = $_POST['password'];
        $id = $_POST['id'];
        
        $data->createFacultyAccount($id, $username, $password);
        
    }
    
    function loadCoursesPerBranch() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->loadCoursesPerBranch($branch);
        
    }
?>