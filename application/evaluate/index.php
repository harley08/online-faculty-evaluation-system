<?php
    $page_title = 'Evaluate';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    } else if ($my_role == 'Faculty') {
        header("Location: $base_url/application/faculty");
    } else if ($my_role == 'Supervisor') {
        header("Location: $base_url/application/supervisor");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div style="height: 50px"></div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/>
                            </div>
                            <div class="text-center">
                                <h3><b><?php print($my_name) ?></b></h3>
                                <h4><?php print($my_role) ?></h4>
                            </div>
                        </div>
                        <ul class="nav-sidebar-menu">
                            <li>School ID: <span class="pull-right"><b><?php print($my_school_id) ?></b></span></li>
                            <li>Curriculum Year: <span class="pull-right"><b><?php print($my_curriculum_year) ?></b></span></li>
                            <li>Course: <span class="pull-right"><b><?php print($my_course) ?></b></span></li>
                        </ul>
                        <div class="panel-footer">
                            <h5 class="text-center"><b><i class="fa fa-calendar-o"></i> <?php echo date("F j, Y"); ?></b></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="breadcrumb">
                        <li>Dashboard</li>
                    </ul>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-book"></i> Enrolled Subjects</h4>
                        </div>
                        <div class="panel-body">
                            <?php
                            
                                /* Get the number of Professor */
                                $n_prof = $db_con->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year = :curriculum_year GROUP BY f_id");
                                $n_prof->bindparam(":branch", $my_branch);
                                $n_prof->bindparam(":course", $my_course);
                                $n_prof->bindparam(":curriculum_year", $my_curriculum_year);
                                $n_prof->execute();
                                
                                /* Get the number of Professors Evaluated */
                                $e_prof = $db_con->prepare("SELECT * FROM evaluation_results WHERE semester = :semester AND "
                                        . "semestral_term = :semestral_term AND "
                                        . "school_year = :school_year AND "
                                        . "branch = :branch AND "
                                        . "user_code = :user_code");
                                $e_prof->bindparam(":semester", $today_sem);
                                $e_prof->bindparam(":semestral_term", $today_sem_term);
                                $e_prof->bindparam(":school_year", $today_sy);
                                $e_prof->bindparam(":branch", $my_branch);
                                $e_prof->bindparam(":user_code", $my_userCode);
                                $e_prof->execute();
                                
                                if ($n_prof->rowCount() == $e_prof->rowCount()) {
                                    
                                    $checkCert = $db_con->prepare("SELECT * FROM certificates WHERE branch = :branch AND "
                                            . "user_code = :user_code AND "
                                            . "semester = :semester AND "
                                            . "semestral_term = :semestral_term AND "
                                            . "school_year = :school_year");
                                    $checkCert->bindparam(":branch", $my_branch);
                                    $checkCert->bindparam(":user_code", $my_userCode);
                                    $checkCert->bindparam(":semester", $today_sem);
                                    $checkCert->bindparam(":semestral_term", $today_sem_term);
                                    $checkCert->bindparam(":school_year", $today_sy);
                                    $checkCert->execute();
                                    
                                    if ($checkCert->rowCount() == null) {
                                        
                                        $generateCertficate = $db_con->prepare("INSERT INTO certificates (branch, user_code, semester, semestral_term, school_year) VALUES "
                                            . "(:branch, :user_code, :semester, :semestral_term, :school_year)");
                                        $generateCertficate->bindparam(":branch", $my_branch);
                                        $generateCertficate->bindparam(":user_code", $my_userCode);
                                        $generateCertficate->bindparam(":semester", $today_sem);
                                        $generateCertficate->bindparam(":semestral_term", $today_sem_term);
                                        $generateCertficate->bindparam(":school_year", $today_sy);
                                        $generateCertficate->execute();
                                        
                                        ?>
                                <div class="text-right">
                                    <a href="" class="btn btn-success"><i class="fa fa-certificate"></i> Your certificate is ready. Refresh.</a>
                                </div>
                                <div style="margin: 15px"></div>
                                        <?php
                                        
                                        
                                    } else { 
                                    
                                    ?>
                            
                                <div class="text-right">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#myCertificateModal"><i class="fa fa-certificate"></i> View Certificate</button>
                                </div>
                                <div style="margin: 15px"></div>
                                    <?php
                                    
                                    }
                                    
                                }
                                
                                ?>
                                <div class="scroll-x">
                                <table class="table table-hover table-bordered" style="margin: 0px auto;">
                                <thead>
                                    <tr>
                                        <th class="text-center">Subjects</th>
                                        <th class="text-center">Professor</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="showStudentProfessors">
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar-o"></i> Evaluation Schedule</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Campus</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $campus_schedules = $db_con->prepare("SELECT * FROM campus_schedules WHERE status = 'Active'");
                                            $campus_schedules->execute();
                                            
                                            if ($campus_schedules->rowCount() != null) {

                                                while ($campus_SchedData = $campus_schedules->fetch(PDO::FETCH_ASSOC)) {
                                                    
                                                    $branch = "";
                                                    
                                                    switch($campus_SchedData['branch']) {
                                                        case 'MBC':
                                                            $branch = "Bongabong Campus";
                                                            break;
                                                        case 'MMC':
                                                            $branch = "Main Campus";
                                                            break;;
                                                        case 'MCC':
                                                            $branch = "Calapan City Campus";
                                                            break;
                                                        default :
                                                            break;
                                                    }
                                                    
                                                    ?>
                                        <tr>
                                            <td><b><?php print($branch) ?></b></td>
                                            <td><?php print($campus_SchedData['start_date']) ?></td>
                                            <td><?php print($campus_SchedData['end_date']) ?></td>
                                        </tr>
                                                    <?php
                                                    
                                                }
                                                
                                            } else {
                                                
                                                ?>
                                        <tr>
                                            <td class="text-center" colspan="3">no active schedules yet</td>
                                        </tr>
                                                <?php                                                
                                                
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include '../../modals/student-evaluate-modals.php'; ?>
        <script src="../../ajax/evaluate-student-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>