<?php
    $page_title = 'Supervisor';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    } else if ($my_role != 'Administrator' && $my_role == 'Student') {
        header("Location: $base_url/application/evaluate");
    } else if ($my_role == 'Faculty') {
        header("Location: $base_url/application/faculty");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div style="height: 50px"></div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/>
                            </div>
                            <div class="text-center">
                                <h3><b><?php print($my_name) ?></b></h3>
                                <h4><?php print($my_role) ?></h4>
                            </div>
                        </div>
                        <ul class="nav-sidebar-menu">
                            <li>User Code: <span class="pull-right"><b><?php print($my_fac_userCode) ?></b></span></li>
                        </ul>
                        <div class="panel-footer">
                            <h5 class="text-center"><b><i class="fa fa-calendar-o"></i> <?php echo date("F j, Y"); ?> <span id="time"></span></b></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar-o"></i> Supervisor Evaluation</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                                <table id="supervisorEvaluationTable" class="table table-hover table-bordered" style="margin: 0px !important;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Professor</th>
                                            <th class="text-center">Evaluation Type</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                            $peer = $db_con->prepare("SELECT * FROM faculties WHERE branch = :branch AND role = 'Professor' AND f_id != :f_id");
                                            $peer->bindparam(":branch", $my_branch);
                                            $peer->bindparam(":f_id", $my_fac_id);
                                            $peer->execute();
                                            
                                            if ($peer->rowCount() != null) {
                                                
                                                while ($row = $peer->fetch(PDO::FETCH_ASSOC)) {
                                                    
                                                    ?>
                                        <tr>
                                            <td class="text-center">
                                                <b><?php print($row['firstname'] . " " . $row['lastname']) ?></b>
                                            </td>
                                            <td class="text-center">Supervisor</td>
                                            <td class="text-center">
                                                <?php
                                                $evaluation_type = 'supervisor';
        
                                                $checkIfEvaluated = $db_con->prepare("SELECT * FROM evaluation_results WHERE branch = :branch AND "
                                                . "user_code = :user_code AND "
                                                . "f_id = :f_id "
                                                . "AND semester = :semester AND "
                                                . "semestral_term = :semestral_term AND "
                                                . "school_year = :school_year AND "
                                                . "evaluation_type = :evaluation_type");
                                                $checkIfEvaluated->bindparam(":branch", $my_branch);
                                                $checkIfEvaluated->bindparam(":user_code", $my_userCode);
                                                $checkIfEvaluated->bindparam('f_id', $row['f_id']);
                                                $checkIfEvaluated->bindparam(":semester", $today_sem);
                                                $checkIfEvaluated->bindparam(":semestral_term", $today_sem_term);
                                                $checkIfEvaluated->bindparam(":school_year", $today_sy);
                                                $checkIfEvaluated->bindparam(":evaluation_type", $evaluation_type);
                                                $checkIfEvaluated->execute();
                               
                                                if ($checkIfEvaluated->rowCount() == null && $now_campus_schedule->rowCount() == 1) {
                                   
                                                ?>
                                                <button type="button" onclick="evaluateProfessor(<?php print($row['f_id']) ?>)" class="btn-evaluate"><i class="fa fa-pencil-square-o"></i> Evaluate</button>
                                                <?php
                                   
                                                } else if ($checkIfEvaluated->rowCount() != null && $now_campus_schedule->rowCount() == 1) {
                                    
                                                ?>
                                                <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-pencil-square-o"></i> Evaluated</button>
                                                <?php
                                    
                                                } else if ($now_campus_schedule->rowCount() == 0) {
                                                    
                                                    ?>
                                                <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-calendar-times-o"></i> no schedule</button>
                                                    <?php
                                                    
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                                    <?php
                                                    
                                                }
                                                
                                            } else {
                                                
                                                ?>
                                        <tr>
                                            <td class="text-center" colspan="3">no data</td>
                                        </tr>
                                                <?php
                                                
                                            }
                                            
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar-o"></i> Campus Evaluation Schedule</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Campus</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $campus_schedules = $db_con->prepare("SELECT * FROM campus_schedules WHERE status = 'Active'");
                                            $campus_schedules->execute();
                                            
                                            if ($campus_schedules->rowCount() != null) {

                                                while ($campus_SchedData = $campus_schedules->fetch(PDO::FETCH_ASSOC)) {
                                                    
                                                    $branch = "";
                                                    
                                                    switch($campus_SchedData['branch']) {
                                                        case 'MBC':
                                                            $branch = "Bongabong Campus";
                                                            break;
                                                        case 'MMC':
                                                            $branch = "Main Campus";
                                                            break;;
                                                        case 'MCC':
                                                            $branch = "Calapan City Campus";
                                                            break;
                                                        default :
                                                            break;
                                                    }
                                                    
                                                    ?>
                                        <tr>
                                            <td><b><?php print($branch) ?></b></td>
                                            <td><?php print($campus_SchedData['start_date']) ?></td>
                                            <td><?php print($campus_SchedData['end_date']) ?></td>
                                        </tr>
                                                    <?php
                                                    
                                                }
                                                
                                            } else {
                                                
                                                ?>
                                        <tr>
                                            <td class="text-center" colspan="3">no active schedules yet</td>
                                        </tr>
                                                <?php                                                
                                                
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include '../../modals/supervisor-modals.php'; ?>
        <script src="../../ajax/supervisor-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>