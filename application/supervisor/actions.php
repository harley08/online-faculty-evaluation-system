<?php
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        
        $action = $_POST['action'];
        switch ($action) {
            case 'showStudentProfessors':
                showStudentProfessors();
                break;
            case 'getTheFacultyName':
                getTheFacultyName();
                break;
            default:
                break;
        }
        
    }
    
    function showStudentProfessors() {
        
        include '../../config/config.php';
        
        $branch = $my_branch;
        $course = $my_course;
        $curriculum_year = $my_curr_year;
        
        $data->showStudentProfessors($branch, $course, $curriculum_year);
        
    }
    
    function getTheFacultyName() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $data->getTheFacultyName($id);
        
    }
?>