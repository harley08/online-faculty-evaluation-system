<?php
    $page_title = 'Courses';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button class="btn btn-default" data-toggle="modal" data-target="#addCourseModal" style="margin-bottom: 10px !important"><i class="fa fa-plus"></i> Add New Course</button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Courses</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="select_branch" id="select_branch" class="form-control">
                                        <option value="">-- select branch --</option>
                                        <option value="MBC">MinSCAT Bongabong Campus</option>
                                        <option value="MMC">MinSCAT Main Campus</option>
                                        <option value="MCC">MinSCAT Calapan City Campus</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="scroll-x">
                        <table id="coursesTable" class="table table-hover table-bordered" style="margin: 0px;">
                            <thead>
                                <tr>
                                    <th class="text-center">Course</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="coursesData"></tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>


        <?php include '../../modals/courses_modals.php' ?>

        <script src="../../ajax/courses_ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>