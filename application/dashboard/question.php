<?php
    $page_title = 'Question';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addQuestionModal" style="margin-bottom: 10px !important;"><small><i class="fa fa-plus"></i> Add Question</small></button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-question-circle-o"></i> Questions</h4>
                    </div>
                    <div class="panel-body">
                        <div class="scroll-x">
                            <div id="questionsData"></div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        
        <!-- Add Question Modal[Start] -->
        <div id="addQuestionModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus"></i> Add Question</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-7">
                                <form id="addQuestionForm">
                                    <div class="form-group">
                                        <label for="category">Category:</label>
                                        <select name="category" class="form-control" required>
                                            <option value="">-- choose category --</option>
                                            <?php
                                                $categories = $db_con->prepare("SELECT * FROM categories");
                                                $categories->execute();
                                                
                                                if ($categories->rowCount() != null) {
                                                    while ($row = $categories->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                            <option value="<?php print($row['category']) ?>"><?php print($row['category_name']) ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                            <option value="no data"></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="question">Question:</label>
                                        <textarea name="question" class="form-control" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status:</label>
                                        <select name="status" class="form-control" required>
                                            <option value="">-- activate/deactivate --</option>
                                            <option value="Active">Activate</option>
                                            <option value="Inactive">Deactivate</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="action" value="addQuestion" />
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Question Modal [End] -->
        
        <!-- Update Question Modal[Start] -->
        <div id="updateQuestionModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Question</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-7">
                                <form id="updateQuestionForm">
                                    <div class="form-group">
                                        <label for="category">Category:</label>
                                        <select id="u_category" name="category" class="form-control" required>
                                            <option value="">-- choose category --</option>
                                            <?php
                                                $categories = $db_con->prepare("SELECT * FROM categories");
                                                $categories->execute();
                                                
                                                if ($categories->rowCount() != null) {
                                                    while ($row = $categories->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                            <option value="<?php print($row['category']) ?>"><?php print($row['category_name']) ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                            <option value="no data"></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="question">Question:</label>
                                        <textarea name="question" id="u_question" class="form-control" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status:</label>
                                        <select name="status" class="form-control" id="u_status" required>
                                            <option value="">-- activate/deactivate --</option>
                                            <option value="Active">Activate</option>
                                            <option value="Inactive">Deactivate</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="action" value="updateQuestion" />
                                    <input type="hidden" name="u_id" id="u_id" />
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Update Question Modal [End] -->
        
        <!-- Delete Question Modal [Start] -->
        <div id="deleteQuestionModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Question</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this question?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger yes">Yes</button>
                        <button type="button" data-dismiss="modal" class="btn btn-primary">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete Question Modal [End] -->
        
        <!-- Success Modal [Start] -->
        <div id="successModal" class="modal modal-center fade" tabindex="-1">
            <div class="modal-dialog modal-center-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <font class="color-green"><b><i class="fa fa-check"></i></b> <b id="text_content"></b></font>
                    </div>
                </div>
            </div>
        </div>
        <!-- Success Modal [End] -->
        <script src="../../ajax/questions_ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>