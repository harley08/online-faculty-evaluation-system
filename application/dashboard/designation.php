<?php
    $page_title = 'Designation';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addDesignationModal"><i class="fa fa-plus"></i> Add Designation</button>
                <div style="margin: 15px;"></div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Designation</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Designation</th>
                                    <th class="text-center">Peer</th>
                                    <th class="text-center">Supervisor</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="designationsData"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>


        <?php include '../../modals/designations-modals.php' ?>

        <script src="../../ajax/designations-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>