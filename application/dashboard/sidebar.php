            
            <div class="col-md-3">
                    <div style="height: 50px;"></div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/>
                            </div>
                            <div class="text-center">
                                <h3><b><?php print($my_name) ?></b></h3>
                                <h4><?php print($my_role) ?></h4>
                            </div>
                        </div>
                        <ul class="nav-sidebar" id="accordion">
                            <li>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#users"><i class="fa fa-users"></i> Users <i class=" fa fa-caret-down pull-right"></i></a>
                                <ul class="nav-dropdown collapse fade" id="users">
                                    <li><a href="students.php">Students</a></li>
                                    <li><a href="faculty.php">Faculty</a></li>
                                </ul>
                            </li>
                            <li>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#questionaire"><i class="fa fa-question-circle-o"></i> Questionaire <i class=" fa fa-caret-down pull-right"></i></a>
                                <ul class="nav-dropdown collapse fade" id="questionaire">
                                    <li><a href="category.php">Category</a></li>
                                    <li><a href="question.php">Question</a></li>
                                </ul>
                            </li>
                            <li>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#schedule"><i class="fa fa-calendar-check-o"></i> Evaluation Schedule <i class=" fa fa-caret-down pull-right"></i></a>
                                <ul class="nav-dropdown collapse fade" id="schedule">
                                    <li><a href="campus-schedule.php">Set Campus Schedule</a></li>
                                    <li><a href="self-evaluation.php">Self Evaluation Schedule</a></li>
                                    <li><a href="peer-evaluation.php">Peer Evaluation Schedule</a></li>
                                </ul>
                            </li>
                            <li>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#reports"><i class="fa fa-newspaper-o"></i> Reports <i class=" fa fa-caret-down pull-right"></i></a>
                                <ul class="nav-dropdown collapse fade" id="reports">
                                    <li><a href="current-evaluations.php">Current Evaluations</a></li>
                                    <li><a href="evaluation-results.php">Evaluation Results</a></li>
                                    <li><a href="evaluation-history.php">Evaluation History</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="courses.php"><i class="fa fa-institution"></i> Courses</a>
                            </li>
                            <li><a href="faculty-loads.php"><i class="fa fa-tasks"></i> Review Faculty Loads</a></li>
                            <li>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#settings"><i class="fa fa-chain"></i> Settings <i class=" fa fa-caret-down pull-right"></i></a>
                                <ul class="nav-dropdown collapse fade" id="settings">
                                    <li><a href="semester_&_school_year.php">SEM & SY Activation</a></li>
                                    <li><a href="percentage-settings.php">Percentage Settings</a></li>
                                    <li><a href="designation.php">Designation</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
            </div>