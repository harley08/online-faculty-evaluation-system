            <?php

                $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $page_url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

            ?>
            <div class="col-md-9">
                <ol class="breadcrumb breadcrumb-arrow">
                    <?php
                        $page_link = $_SERVER['QUERY_STRING'];
                        if ($page_url == "$base_url/appplication/dashboard/" || $page_url == "$base_url/application/dashboard/index.php") {
                            ?>
                    <li><a href="index.php">Home</a></li>
                    <li class="active"><span>Dashboard</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/students.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Students</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/faculty.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Faculty & Staff</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/category.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Category</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/question.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Question</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/faculty-loads.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Faculty Loads</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/class-schedule.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Class Schedule</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/campus-schedule.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Campus Schedule</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/self-evaluation.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Self Evaluation</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/supervisor-evaluation.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Supervisor Evaluation</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/peer-evaluation.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Peer Evaluation</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/settings.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Settings</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/evaluation-history.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Evaluation History</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/courses.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Courses</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/semester_&_school_year.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Semester and School Year Activation</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/percentage-settings.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Percentage Settings</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/campus-schedule.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Campus Schedule</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/evaluation-results.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Evaluation Results</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/current-evaluations.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Current Evaluations</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/designation.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Designations</span></li>
                    <?php
                        } else if ($page_url == "$base_url/application/dashboard/evaluated.php") {
                            ?>
                    <li><a href="<?php echo $base_url ?>">Home</a></li>
                    <li><a href="<?php echo $base_url ?>/application/dashboard">Dashboard</a></li>
                    <li class="active"><span>Evaluations</span></li>
                    <?php
                        } else {
                            ?>
                    <li><a href="index.php">Home</a></li>
                    <li class="active"><span>Dashboard</span></li>
                    <?php
                        }
                    ?>
                </ol>
            </div>
