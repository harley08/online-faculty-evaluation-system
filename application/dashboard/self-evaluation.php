<?php
    $page_title = 'Self Evaluation';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <?php include 'link-directory.php';?>
                <div class="col-md-9">
                    <button type="button" data-toggle="modal" data-target="#SetSelfEvaluationByCampusModal" style="margin-bottom: 10px !important;" class="btn btn-default"><i class="fa fa-calendar-plus-o"></i> Set Campus Self Schedule</button>
                    <button type="button" data-toggle="modal" data-target="#AddSelfScheduleModal" style="margin-bottom: 10px !important;" class="btn btn-default"><i class="fa fa-calendar-plus-o"></i> Add Faculty Schedule</button>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Self Evaluation</h4>
                        </div>
                        <div class="panel-body">
                            <select id="select_branch" class="form-control" style="max-width: 190px; margin-bottom: 10px !important;">
                                <option value="">-- select branch --</option>
                                <option value="MBC">Bongabong Campus</option>
                                <option value="">Main Campus</option>
                                <option value="">Calapan City Campus</option>
                            </select>
                            <table id="selfEvaluationTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Professor</th>
                                        <th class="text-center">Start Date</th>
                                        <th class="text-center">End Date</th>
                                        <th class="text-center">Sem & SY</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="selfEvaluationData"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include '../../modals/self-evaluation-modals.php'; ?>

        <script src="../../ajax/self-evaluation-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>