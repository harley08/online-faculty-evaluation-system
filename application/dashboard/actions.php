<?php    
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        $action = $_POST['action'];
        switch($action) {
            case 'showFacultyData':
                showFacultyData();
                break;
            case 'showBranchData':
                showBranchData();
                break;
            case 'showQuestionsData':
                showQuestionsData();
                break;
            case 'showCategories':
                showCategories();
                break;
            case 'AddFaculty':
                addFaculty();
                break;
            case 'AddStudent':
                addStudent();
                break;
            case 'addCategory':
                addCategory();
                break;
            case 'addQuestion':
                addQuestion();
                break;
            case 'showUpdateFaculty';
                showUpdateFaculty();
                break;
            case 'showUpdateStudent':
                showUpdateStudent();
                break;
            case 'showUpdateCategory':
                showUpdateCategory();
                break;
            case 'showUpdateQuestion':
                showUpdateQuestion();
                break;
            case 'updateFaculty':
                updateFaculty();
                break;
            case 'updateStudent':
                updateStudent();
                break;
            case 'deleteFaculty':
                deleteFaculty();
                break;
            case 'deleteStudent':
                deleteStudent();
                break;
            case 'deleteCategory':
                deleteCategory();
                break;
            case 'deleteQuestion':
                deleteQuestion();
                break;
            case 'updateQuestion':
                updateQuestion();
                break;
            case 'updateCategory':
                updateCategory();
                break;
            case 'addCourse':
                addCourse();
                break;
            case 'loadCourse':
                loadCourse();
                break;
            case 'showUpdateCourse':
                showUpdateCourse();
                break;
            case 'updateCourse':
                updateCourse();
                break;
            case 'deleteCourse':
                deleteCourse();
                break;
            case 'addSubject':
                addSubject();
                break;
            case 'loadSubjects':
                loadSubjects();
                break;
            case 'loadSubjectsPerBranch':
                loadSubjectsPerBranch();
                break;
            case 'showStudents':
                showStudents();
                break;
            case 'loadCoursesPerBranch':
                loadCoursesPerBranch();
                break;
            case 'deleteSubject':
                deleteSubject();
                break;
            case 'showSemANDSY':
                showSemANDSY();
                break;
            case 'showUpdateSemAndSY':
                showUpdateSemAndSY();
                break;
            case 'updateSemANDSY':
                updateSemANDSY();
                break;
            case 'addEvaluatorType':
                addEvaluatorType();
                break;
            case 'showEvaluatorTypes':
                showEvaluatorTypes();
                break;
            case 'showUpdateEvaluatorType':
                showUpdateEvaluatorType();
                break;
            case 'updateEvaluatorType':
                updateEvaluatorType();
                break;
            case 'ShowCampusScheduleData':
                ShowCampusScheduleData();
                break;
            case 'showSelfEvaluationData':
                showSelfEvaluationData();
                break;
            case 'showUpdateCampusSchedule':
                showUpdateCampusSchedule();
                break;
            case 'UpdateCampusSched':
                UpdateCampusSched();
                break;
            case 'showProfessorsByBranch_inSelect':
                showProfessorsByBranch_inSelect();
                break;
            case 'addUpdateSelfSchedule':
                addUpdateSelfSchedule();
                break;
            case 'showFacultiesPerBranch_inSelect':
                showFacultiesPerBranch_inSelect();
                break;
            case 'ShowPeerEvaluationData':
                ShowPeerEvaluationData();
                break;
            case 'PickRandomEvaluators':
                PickRandomEvaluators();
                break;
            case 'AddUpdatePeerSchedule':
                AddUpdatePeerSchedule();
                break;
            case 'deletePeerSchedules':
                deletePeerSchedules();
                break;
            case 'showUpdatePeerSchedule':
                showUpdatePeerSchedule();
                break;
            case 'showUpdateEvaluatorsOfProfessor':
                showUpdateEvaluatorsOfProfessor();
                break;
            case 'UpdatePeerSchedule':
                UpdatePeerSchedule();
                break;
            case 'UpdateSelfEvaluationByCampus':
                UpdateSelfEvaluationByCampus();
                break;
            case 'deleteSelfSchedule':
                deleteSelfSchedule();
                break;
            case 'showUpdateSelfSchedule':
                showUpdateSelfSchedule();
                break;
            case 'UpdateSelfSchedule':
                UpdateSelfSchedule();
                break;
            case 'showEvaluationResults':
                showEvaluationResults();
                break;
            case 'showProfessorsPerBranch':
                showProfessorsPerBranch();
                break;
            case 'showUpdateSubjects':
                showUpdateSubjects();
                break;
            case 'updateSubject':
                updateSubject();
                break;
            case 'showFacultyEvaluations':
                showFacultyEvaluations();
                break;
            case 'getFacultyData':
                getFacultyData();
                break;
            case 'viewFacultyInfo':
                viewFacultyInfo();
                break;
            case 'showDesignationData':
                showDesignationData();
                break;
            case 'addDesignation':
                addDesignation();
                break;
            case 'showUpdateDesignation':
                showUpdateDesignation();
                break;
            case 'updateDesignation':
                updateDesignation();
                break;
            case 'showEvaluationsData':
                showEvaluationsData();
                break;
            default :
                #code
                break;
        }
    }
    
    function showFacultyData()
    {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showFacultyData($branch);
        
    }
    
    function showFacultyData_afterEvent($branch)
    {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->showFacultyData($branch);
        
    }
    
    function showBranchData()
    {
        include '../../config/config.php';
        $branch = $_POST['branch'];
        
        $data->showBranchData($branch);
    }
    
    function showCategories()
    {
        include '../../config/config.php';
        $data->showCategories();
    }
    
    function showCategory_afterEvent()
    {
        include '../../config/database.php';
        $data = new data($db_con);
        $data->showCategories();
    }
    
    function addCategory()
    {
        include '../../config/config.php';
        
        $category_name = $_POST['catagory_name'];
        $status = $_POST['status'];
        $percentage = $_POST['percentage'];
        
        $data->addCategory($category_name, $percentage, $status);
    }
    
    function addFaculty() {
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $firstname = $_POST['firstname'];
        $middlename = $_POST['middlename'];
        $lastname = $_POST['lastname'];
        $employmentStatus = $_POST['employment_status'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $role = $_POST['role'];
        
        $data->addFaculty($branch, $firstname, $middlename, $lastname, $employmentStatus, $email, $mobile, $role);
        
        showFacultyData_afterEvent($branch);
    }
    
    if (isset($_GET['searchStudentQuery']))
    {
        $keyword = htmlspecialchars($_GET['searchStudentQuery'], ENT_QUOTES, 'UTF-8');
        
        if (strlen($keyword) < 3)
        {
            
            ?>
                <tr>
                    <td colspan="7" class="text-center">Minimum of 3 characters required!</td>
                </tr>
            <?php
                
        } else {
            include '../../config/config.php';
            $data->search_student($keyword);
        }
    }
    
    if (isset($_GET['query'])) {
        $keyword = htmlspecialchars($_GET['query'], ENT_QUOTES, 'UTF-8');
        
        if (strlen($keyword) < 3) {
            
            ?>
                <tr>
                    <td colspan="8" class="text-center">Minimum of 3 characters required!</td>
                </tr>
            <?php
            
        } else {
            include '../../config/config.php';
            
            $stmt = $db_con->prepare("SELECT * FROM faculties WHERE (firstname LIKE :kw) or (lastname LIKE :kw) or (f_code LIKE :kw) or (employment_status LIKE :kw) or (email LIKE :kw) or (mobile LIKE :kw) or (role LIKE :kw)");
            $stmt->bindValue(":kw", '%'. $keyword . '%');
            $stmt->execute();
            
            if ($stmt->rowCount() != null) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    
                    ?>
                    <tr id="content">
                        <td class="text-center"><?php print($row['f_code']) ?></td>
                        <td class="text-center"><?php print($row['firstname']) ?></td>
                        <td class="text-center"><?php print($row['lastname']) ?></td>
                        <td class="text-center"><?php print($row['employment_status']) ?></td>
                        <td class="text-center"><?php print($row['email']) ?></td>
                        <td class="text-center"><?php print($row['mobile']) ?></td>
                        <td class="text-center"><?php print($row['role']) ?></td>
                        <td class="text-center"><a onclick="updateFaculty(<?php print($row['f_id']) ?>)"><i class="fa fa-edit"></i></a> <a onclick="deleteFaculty(<?php print($row['f_id']) ?>)"><i class="fa fa-remove"></i></a></td>
                    </tr>
                    <?php
                        
                }
            } else {
                
                ?>
                <tr>
                    <td colspan="8" class="text-center">no data found</td>
                </tr>                
                <?php
            }
        }
    }
    
    function showUpdateFaculty()
    {
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $stmt = $db_con->prepare("SELECT * FROM faculties WHERE f_id = :u_id");
        $stmt->bindparam(':u_id', $id);
        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $branch = $row['branch'];
        $firstname = $row['firstname'];
        $middlename = $row['middlename'];
        $lastname = $row['lastname'];
        $employment_status = $row['employment_status'];
        $email = $row['email'];
        $mobile = $row['mobile'];
        $role = $row['role'];
        
        echo json_encode(array("branch" => $branch, "firstname" => $firstname, "middlename" => $middlename, "lastname" => $lastname, "employment_status" => $employment_status, "email" => $email, "mobile" => $mobile, "role" => $role));
    }
    
    function showUpdateCategory()
    {
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateCategory($id);
    }


    function showUpdateStudent()
    {
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateStudent($id);
    }
    
    function updateFaculty()
    {
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $firstname = $_POST['firstname'];
        $middlename = $_POST['middlename'];
        $lastname = $_POST['lastname'];
        $employment_status = $_POST['employment_status'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $role = $_POST['role'];
        $id = $_POST['u_id'];
        
        $stmt = $db_con->prepare("UPDATE faculties SET branch = :branch, firstname = :firstname, middlename = :middlename, lastname = :lastname, employment_status = :employment_status, email = :email, mobile = :mobile, role = :role WHERE f_id = :f_id");
        $stmt->bindparam(":branch", $branch);
        $stmt->bindparam(":firstname", $firstname);
        $stmt->bindparam(":middlename", $middlename);
        $stmt->bindparam(":lastname", $lastname);
        $stmt->bindparam(":employment_status", $employment_status);
        $stmt->bindparam(":email", $email);
        $stmt->bindparam(":mobile", $mobile);
        $stmt->bindparam(":role", $role);
        $stmt->bindparam(':f_id', $id);
        
        $stmt->execute();
        
        showFacultyData_afterEvent($branch);
    }
    
    function deleteFaculty()
    {
        include '../../config/config.php';
        $id = $_POST['id'];
        $branch = $_POST['branch'];
        
        $stmt = $db_con->prepare("DELETE FROM faculties WHERE f_id = :f_id");
        $stmt->bindparam(':f_id', $id);
        $stmt->execute();
        
        showFacultyData_afterEvent($branch);
    }
    
    function addStudent()
    {
        include '../../config/config.php';
        
        $firstname = $_POST['firstname'];
        $middlename = $_POST['middlename'];
        $lastname = $_POST['lastname'];
        $school_id = $_POST['school_id'];
        $course = $_POST['course'];
        $email = $_POST['email'];
        $mobile_no = $_POST['mobile_no'];
        $branch = $_POST['branch'];
        $curriculum_year = $_POST['curriculum_year'];
        
        $data->addStudent($branch, $firstname, $middlename, $lastname, $school_id, $course, $curriculum_year, $email, $mobile_no);
        
        showStudents_afterEvent($course, $branch);
        
    }
    
    function deleteCategory()
    {
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->deleteCategory($id);
        showCategory_afterEvent();
    }
    
    function showQuestionsData()
    {
        include '../../config/config.php';
        
        $data->showQuestionsData();
    }
    
    function showQuestionsData_afterEvent()
    {
        include '../../config/database.php';
        $data = new data($db_con);
                
        $data->showQuestionsData();
    }
    
    function addQuestion()
    {
        include '../../config/config.php';
        $category = $_POST['category'];
        $question = $_POST['question'];
        $status = $_POST['status'];
        
        $data->addQuestion($category, $question, $status);
        
        showQuestionsData_afterEvent();
    }
    
    function deleteQuestion()
    {
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->deleteQuestion($id);
        
        showQuestionsData_afterEvent();
    }
    
    function showUpdateQuestion()
    {
        include '../../config/config.php';
        $id = $_POST['id'];
        
        $data->showUpdateQuestion($id);
    }
    
    function updateQuestion()
    {
        
        include '../../config/config.php';
        
        $category = $_POST['category'];
        $question = $_POST['question'];
        $status = $_POST['status'];
        $id = $_POST['u_id'];
        
        $data->updateQuestion($id, $category, $question, $status);
        
        showQuestionsData_afterEvent();
    }
    
    function updateCategory()
    {
        
        include '../../config/config.php';
        
        $category_name = $_POST['category_name'];
        $percentage = $_POST['percentage'];
        $status = $_POST['status'];
        $id = $_POST['u_id'];
        
        $data->updateCategory($id, $category_name, $percentage, $status);
        showCategory_afterEvent();
    }
    
    function addCourse()
    {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $course_name = $_POST['course_name'];
        $course_acronym = $_POST['course_acronym'];
        $course_description = $_POST['course_description'];
        
        $data->addCourse($branch, $course_name, $course_acronym, $course_description);
        
        loadCourse_afterEvent($branch);
        
    }
    
    function loadCourse()
    {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->loadCourse($branch);
        
    }
    
    function loadCourse_afterEvent($branch)
    {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->loadCourse($branch);
        
    }
    
    function showUpdateCourse() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $data->showUpdateCourse($id);
        
    }
    
    function updateCourse()
    {
        
        include '../../config/config.php';
        
        $id = $_POST['u_id'];
        $branch = $_POST['branch'];
        $course_name = $_POST['course_name'];
        $course_acronym = $_POST['course_acronym'];
        $course_description = $_POST['course_description'];
        
        $data->updateCourse($id, $branch, $course_name, $course_acronym, $course_description);
        
        loadCourse_afterEvent($branch);
        
    }
    
    function deleteCourse()
    {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $branch = $_POST['branch'];
        
        
        $data->deleteCourse($id);
        
        loadCourse_afterEvent($branch);
        
    }
    
    function addSubject()
    {
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $course = $_POST['course'];
        $curriculum_year = $_POST['curriculum_year'];
        $subject_code = $_POST['subject_code'];
        $subject_description = $_POST['subject_description'];
        $f_id = $_POST['f_id'];
        
        $data->addSubject($branch, $course, $curriculum_year, $subject_code, $subject_description, $f_id);
        
        loadSubjects_afterEvent($course, $branch);
    }
    
    function loadSubjects()
    {
        include '../../config/config.php';
        
        $course = $_POST['course'];
        $branch = $_POST['branch'];
        
        $data->loadSubjects($course, $branch);
    }
    
    function loadSubjects_afterEvent($course, $branch)
    {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->loadSubjects($course, $branch);
        
    }
    
    function loadSubjectsPerBranch()
    {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->loadSubjectsPerBranch($branch);
        
    }
    
    function loadSubjectsPerBranch_afterEvent($branch)
    {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->loadSubjectsPerBranch($branch);
        
    }
    
    function showStudents() {
        
        include '../../config/config.php';
        
        $course = $_POST['course'];
        $branch = $_POST['branch'];
        
        $data->showStudents($branch, $course);
        
    }
    
    function showStudents_afterEvent($course, $branch) {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->showStudents($branch, $course);
        
    }
    
    function loadCoursesPerBranch() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->loadCoursesPerBranch($branch);
        
    }
    
    function deleteStudent() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $course = $_POST['course'];
        $branch = $_POST['branch'];
        
        $data->deleteStudent($id);
        
        showStudents_afterEvent($course, $branch);
        
    }
    
    function deleteSubject() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $branch = $_POST['branch'];
        $course = $_POST['course'];
        
        $data->deleteSubject($id);
        
        loadSubjects_afterEvent($course, $branch);
        
    }
    
    function showSemANDSY() {
        
        include '../../config/config.php';
        $data->showSemANDSY();
        
    }
    
    function showSemANDSY_afterEvent() {
        
        include '../../config/database.php';
        $data = new data($db_con);
        $data->showSemANDSY();
        
    }
    
    function showUpdateSemAndSY()
    {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateSemAndSY($id);
        
    }
    
    function updateSemANDSY() {
        
        include '../../config/config.php';
        
        $semester = $_POST['semester'];
        $term = $_POST['sem_term'];
        $school_year = $_POST['school_year'];
        $status = $_POST['status'];
        
        $data->updateSemANDSY($semester, $term, $school_year, $status);
        
        showSemANDSY_afterEvent();
        
    }
    
    function addEvaluatorType() {
        
        include '../../config/config.php';
        
        $evaluator = $_POST['evaluator'];
        $percentage = $_POST['percentage'];
        
        $data->addEvaluatorType($evaluator, $percentage);
        showEvaluatorTypes_afterEvent();
        
    }
    
    function showEvaluatorTypes() {
        
        include '../../config/config.php';
        
        $data->showEvaluatorTypes();
        
    }
    
    function showEvaluatorTypes_afterEvent() {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->showEvaluatorTypes();
        
    }
    
    function showUpdateEvaluatorType() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateEvaluatorType($id);
        
    }
    
    function updateEvaluatorType() {
        
        include '../../config/config.php';
        
        $evaluator = $_POST['evaluator'];
        $percentage = $_POST['percentage'];
        $id = $_POST['u_id'];
        $total_evaluators = $_POST['total_evaluators'];
        
        $data->updateEvaluatorType($evaluator, $percentage, $id, $total_evaluators);
        showEvaluatorTypes_afterEvent();
        
    }
    
    function ShowCampusScheduleData() {
        
        include '../../config/config.php';
        
        $data->ShowCampusScheduleData();
        
    }
    
    function ShowCampusScheduleData_afterEvent() {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->ShowCampusScheduleData();
        
    }
    
    function showUpdateCampusSchedule() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateCampusSchedule($id);
        
    }
    
    function UpdateCampusSched() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $status = $_POST['status'];
        $id = $_POST['u_id'];
        
        $data->UpdateCampusSched($branch, $start_date, $end_date, $status, $id);
        ShowCampusScheduleData_afterEvent();
        
    }
    
    function showProfessorsByBranch_inSelect() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showProfessorsByBranch_inSelect($branch);
        
    }
    
    function addUpdateSelfSchedule() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $f_id = $_POST['f_id'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        
        $data->addUpdateSelfSchedule($branch, $f_id, $start_date, $end_date);
        
        showSelfEvaluationData_afterEvent($branch);
        
    }
    
    function showSelfEvaluationData() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showSelfEvaluationData($branch);
        
    }
    
    function showSelfEvaluationData_afterEvent($branch) {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->showSelfEvaluationData($branch);
        
    }
    
    function showFacultiesPerBranch_inSelect()
    {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showFacultiesPerBranch_inSelect($branch);
        
    }
    
    function ShowPeerEvaluationData()
    {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->ShowPeerEvaluationData($branch);
        
    }
    
    function ShowPeerEvaluationData_afterEvent($branch)
    {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->ShowPeerEvaluationData($branch);
        
    }
    
    function PickRandomEvaluators() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->PickRandomEvaluators($branch);
        
    }
    
    function AddUpdatePeerSchedule() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $f_id = $_POST['f_id'];
        $totalNo_user_codes = count($_POST['user_code']);
        $user_code = $_POST['user_code'];
        
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $semester = $_POST['semester'];
        $semestral_term = $_POST['semestral_term'];
        $school_year = $_POST['school_year'];
        
        $data->AddUpdatePeerSchedule($branch, $f_id, $totalNo_user_codes, $user_code, $start_date, $end_date, $semester, $semestral_term, $school_year);
        
        ShowPeerEvaluationData_afterEvent($branch);
        
    }
    
    function updateStudent() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $firstname = $_POST['firstname'];
        $middlename = $_POST['middlename'];
        $lastname = $_POST['lastname'];
        $school_id = $_POST['school_id'];
        $course = $_POST['course'];
        $curriculum_year = $_POST['curriculum_year'];
        $email = $_POST['email'];
        $mobile_no = $_POST['mobile_no'];
        $id = $_POST['u_id'];
        
        $data->updateStudent($branch, $firstname, $middlename, $lastname, $school_id, $curriculum_year, $course, $email, $mobile_no, $id);
        
        showStudents_afterEvent($course, $branch);
        
    }
    
    function deletePeerSchedules() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $id = $_POST['id'];
        
        $data->deletePeerSchedules($id);
        
        ShowPeerEvaluationData_afterEvent($branch);
        
    }
    
    function showUpdatePeerSchedule() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $id = $_POST['id'];
        
        $data->showUpdatePeerSchedule($branch, $id);
        
    }
    
    function showUpdateEvaluatorsOfProfessor() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateEvaluatorsOfProfessor($id);
        
    }
    
    function UpdatePeerSchedule() {
        
        include '../../config/config.php';
        
        $branch = $_POST['u_branch'];
        $f_id = $_POST['u_f_id'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        
        $data->UpdatePeerSchedule($f_id, $start_date, $end_date, $today_sem, $today_sem_term, $today_sy);
        
        ShowPeerEvaluationData_afterEvent($branch);
        
    }
    
    function UpdateSelfEvaluationByCampus() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        
        $data->UpdateSelfEvaluationByCampus($branch, $start_date, $end_date);
        
        showSelfEvaluationData_afterEvent($branch);
        
    }
    
    function deleteSelfSchedule() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        $branch = $_POST['branch'];
        
        $data->deleteSelfSchedule($id);
        
        showSelfEvaluationData_afterEvent($branch);
        
    }
    
    function showUpdateSelfSchedule() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateSelfSchedule($id);
        
    }
    
    function UpdateSelfSchedule() {
        
        include '../../config/config.php';
        
        $branch = $_POST['a_branch'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $id = $_POST['u_id'];
        
        $data->UpdateSelfSchedule($id, $start_date, $end_date);
        showSelfEvaluationData_afterEvent($branch);
        
    }
    
    function showEvaluationResults() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showEvaluationResults($branch);
        
    }
    
    function showProfessorsPerBranch() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showProfessorsPerBranch($branch);
        
    }
    
    function showUpdateSubjects() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateSubjects($id);
        
    }
    
    function updateSubject() {
        
        include '../../config/config.php';
        
        $subject_code = $_POST['subject_code'];
        $subject_description = $_POST['subject_description'];
        $f_id = $_POST['f_id'];
        $id = $_POST['u_id'];
        
        $data->updateSubject($subject_code, $subject_description, $f_id, $id);
        loadSubjects_afterEvent($course, $branch);
        
    }
    
    function showFacultyEvaluations() {
        
        include '../../config/config.php';
        
        $branch = $_POST['branch'];
        
        $data->showFacultyEvaluations($branch);
        
    }
    
    function getFacultyData() {
        
        include '../../config/config.php';
        
        $fid = $_POST['id'];
        
        $data->getFacultyData($fid, $today_sem, $today_sem_term, $today_sy);
        
    }
    
    function viewFacultyInfo() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->viewFacultyInfo($id);
        
    }
    
    function showDesignationData() {
        
        include '../../config/config.php';
        
        $data->showDesignationData();
        
    }
    
    function showDesignationData_afterEvent() {
        
        include '../../config/database.php';
        $data = new data($db_con);
        
        $data->showDesignationData();
        
    }
    
    function addDesignation() {
        
        include '../../config/config.php';
        
        $designation = $_POST['designation'];
        
        $data->addDesignation($designation);
        showDesignationData_afterEvent();
    }
    
    function showUpdateDesignation() {
        
        include '../../config/config.php';
        
        $id = $_POST['id'];
        
        $data->showUpdateDesignation($id);
        
    }
    
    function showEvaluationsData() {
        
        include '../../config/config.php';
        
        $evaluation_type = $_POST['evaluation_type'];
        $f_id = $_POST['f_id'];
        
        $data->showEvaluationsData($evaluation_type, $f_id);
        
    }

    function updateDesignation() {

        include '../../config/config.php';

        $designation = $_POST['designation'];
        $peer = $_POST['peer'];
        $supervisor = $_POST['supervisor'];
        $id = $_POST['u_id'];

        $data->updateDesignation($designation, $peer, $supervisor, $id);
        showDesignationData_afterEvent();

    }
?>