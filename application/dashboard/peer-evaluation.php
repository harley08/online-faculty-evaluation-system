<?php
    $page_title = 'Peer Evaluation';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <?php include 'link-directory.php';?>
                <div class="col-md-9">
                    <button type="button" data-toggle="modal" data-target="#addUpdatePeerScheduleModal" class="btn btn-default" style="margin-bottom: 10px !important;"><i class="fa fa-calendar-plus-o"></i> Add Peer Evaluation Sched.</button>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar"></i> Peer Evaluation Schedules</h4>
                        </div>
                        <div class="panel-body">
                            <select id="select_branch" class="form-control" style="width: 200px">
                                <option value="">-- select branch --</option>
                                <option value="MBC">Bongabong Campus</option>
                                <option value="MMC">Main Campus</option>
                                <option value="MCC">Calapan City Campus</option>
                            </select>
                            <div style="margin-bottom: 10px !important"></div>
                            <div class="scroll-x">
                                <table id="PeerEvaluationTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Professor</th>
                                            <th class="text-center">Evaluators</th>
                                            <th class="text-center">Start Date</th>
                                            <th class="text-center">End Date</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="PeerEvaluationData"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include '../../modals/peer-evaluation-modals.php'; ?>
        <script src="../../ajax/peer-evaluation-schedule-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>