<?php
    $page_title = 'Dashboard';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    } else if ($my_role != 'Administrator' && $my_role == 'Student') {
        header("Location: $base_url/application/evaluate");
    } else if ($my_role == 'Faculty') {
        header("Location: $base_url/application/faculty");
    } else if ($my_role == 'Supervisor') {
        header("Location: $base_url/application/supervisor");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
    
    /* Students Total */
    $student_total = $db_con->prepare("SELECT * FROM students");
    $student_total->execute();
    
    /* Faculty Total */
    $faculty_total = $db_con->prepare("SELECT * FROM faculties");
    $faculty_total->execute();   
    
?>

        <div class="container">            
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php'; ?>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php print($faculty_total->rowCount()); ?></h3>
                            <p>Faculty Members</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user"></i>
                        </div>
                        <a href="faculty.php" class="small-box-footer">View All <small><i class="fa fa-arrow-right"></i></small></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3><?php print($student_total->rowCount()); ?></h3>
                            <p>Students</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <a href="students.php" class="small-box-footer">View All <small><i class="fa fa-arrow-right"></i></small></a>
                    </div>                    
                </div>

                <div class="col-md-9">
                <?php

                /* This will show if the evaluation schedule was already ended! */

                
                /* [BONGABONG] */

                $MBC_CampusSchedule = $db_con->prepare("SELECT * FROM campus_schedules WHERE branch = 'MBC' AND status = 'Active' AND start_date <= NOW() AND end_date >= NOW()");
                $MBC_CampusSchedule->execute();

                if ($MBC_CampusSchedule->rowCount() == null) {
                    
                    ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <b>Bongabong Campus</b> was done on their campus evaluation Schedule! Start generating their results!
                    </div>
                    <?php

                }

                /* [MAIN] */

                $MMC_CampusSchedule = $db_con->prepare("SELECT * FROM campus_schedules WHERE branch = 'MMC' AND status = 'Active' AND start_date <= NOW() AND end_date >= NOW()");
                $MMC_CampusSchedule->execute();

                if ($MMC_CampusSchedule->rowCount() == null) {
                    
                    ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <b>Main Campus</b> was done on their campus evaluation Schedule! Start generating their results!
                    </div>
                    <?php

                }

                /* [Calapan City] */

                $MCC_CampusSchedule = $db_con->prepare("SELECT * FROM campus_schedules WHERE branch = 'MCC' AND status = 'Active' AND start_date <= NOW() AND end_date >= NOW()");
                $MCC_CampusSchedule->execute();

                if ($MCC_CampusSchedule->rowCount() == null) {
                    
                    ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <b>Calapan City Campus</b> was done on their campus evaluation Schedule! Start generating their results!
                    </div>
                    <?php

                }

                ?>
                </div>

            </div>
        </div>
<?php
    include '../../template/footer.php';
?>