<?php
    $page_title = 'Campus Schedule';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <?php include 'link-directory.php';?>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar-o"></i> Campus Schedule Activation</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Branch</th>
                                            <th class="text-center">Start Date</th>
                                            <th class="text-center">End Date</th>
                                            <th class="text-center">SEM & SY</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Update</th>
                                        </tr>
                                    </thead>
                                    <tbody id="campusSchedulesData"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
            
        <?php include '../../modals/campus-schedule-modals.php'; ?>

        <script src="../../ajax/campus-schedule-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>