<?php
    $page_title = 'Evaluation History';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <?php include 'link-directory.php';?>
                <div class="col-md-9">
                    <form id="searchEvaluationHistoryForm">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control">
                                <option value="">-- select branch --</option>
                                <option value="MBC">Bongabong Campus</option>
                                <option value="MMC">Main Campus</option>
                                <option value="MCC">Calapan City Campus</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control">
                                <option value="">-- select SEM/S.Y. --</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                    </form>
                    <div style="margin: 15px"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-clock-o"></i> Evaluation History</h4>
                        </div>
                        <div class="panel-body">
                            <div id="evaluationHistoryData"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include '../../modals/evaluation-history-modal.php'; ?>
        
        <script src="../../ajax/evaluation-history-ajax.js" type="text/javascript"></script>
        
<?php
    include '../../template/footer.php';
?>