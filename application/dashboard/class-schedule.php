<?php
    $page_title = 'Class Schedule';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            </div>
        </div>
        <script src="<?php echo $base_url; ?>/assets/js/collapse.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>