<?php
    $page_title = 'Category';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button type="button" data-toggle="modal" data-target="#addCategoryModal" class="btn btn-default" style="margin-bottom: 10px !important;">
                    <small><i class="fa fa-plus"></i> Add Category</small>
                </button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-list-alt"></i> Category</h4>
                    </div>
                    <div class="panel-body">
                        <div class="scroll-x">
                        <table id="categoryTable" class="table table-hover table-bordered" style="margin: 0px !important">
                            <thead>
                                <tr>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Category Name</th>
                                    <th class="text-center">Percentage</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="categoryData"></tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        
        
        <?php include '../../modals/category_modals.php'; ?>
        
        
        <script src="../../ajax/category_ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>