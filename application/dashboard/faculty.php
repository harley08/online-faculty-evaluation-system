<?php
    $page_title = 'Faculty';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>

        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button type="button" data-toggle="modal" data-target="#addFacultyModal" class="btn btn-default" style="margin-bottom: 10px !important;"><small><i class="fa fa-plus"></i> Add New Faculty</small></button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Faculty</h4>
                    </div>
                    <div class="panel-body">
                        <select id="selectBranch" class="form-control" style="width: 200px;">
                            <option value="">-- select branch --</option>
                            <option value="MBC">MinSCAT Bongabong Campus</option>
                            <option value="MMC">MinSCAT Main Campus</option>
                            <option value="MCC">MinSCAT Calapan City Campus</option>
                        </select>
                    <div style="margin: 10px"></div>
                        <div id="printArea">
                            <section>
                            <div class="scroll-x">
                            <table id="facultyTable" class="table table-hover table-bordered" style="margin: 0px !important;">
                                <thead>
                                    <tr>
                                        <th class="text-center">Faculty Code</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Employment Status</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Mobile</th>
                                        <th class="text-center">Access Code</th>
                                        <th class="text-center">Role</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="facultyData"></tbody>
                            </table>
                            </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        

        <?php include '../../modals/faculty_modals.php'; ?>

        <script src="../../ajax/faculty-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>