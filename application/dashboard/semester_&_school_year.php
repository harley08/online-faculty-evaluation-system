<?php
    $page_title = 'Semester & School Year Activation';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <?php include 'link-directory.php';?>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Semester & School Year Activation</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="margin: 0px !important">
                                <thead>
                                    <tr>
                                        <th class="text-center">Semester</th>
                                        <th class="text-center">Semestral Term</th>
                                        <th class="text-center">School Year</th>
                                        <th class="text-center">Update</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                </thead>
                                <tbody id="showSemAndSY">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="sem_and_syUpdateModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Semester & School Year</h4>
                    </div>
                    <div class="modal-body">
                        <form id="updateSemAndSYForm">
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="semester">Semester:</label>
                                        <select id="u_semester" name="semester" class="form-control">
                                            <option value="First Semester">First Semester</option>
                                            <option value="Second Semester">Second Semester</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="school_year">School Year:</label>
                                        <input type="text" id="u_school_year" name="school_year" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="sem_term">Semestral Term:</label>
                                        <select id="u_sem_term" name="sem_term" class="form-control">
                                            <option value="Midterm">Midterm</option>
                                            <option value="Finals">Finals</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status:</label>
                                        <select id="u_status" name="status" class="form-control">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="u_id" id="u_id" />
                                    <input type="hidden" name="action" value="updateSemANDSY" />
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Success Modal [Start] -->
        <div id="successModal" class="modal modal-center fade" tabindex="-1">
            <div class="modal-dialog modal-center-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4 style="margin: 0px; color: #049d00;" class="text-center"><i class="fa fa-check-circle"></i> <span id="text_content"></span></h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- Success Modal [End] -->

        <script type="text/javascript">
            
            $(document).ready(function(){
                
                $('#loading').show();
                
                $.ajax({
                    
                    url: 'actions.php',
                    type: 'POST',
                    data: {action: 'showSemANDSY'},
                    dataType: 'html',
                    success: function(result)
                    {
                        $('#showSemAndSY').html(result);
                        $('#loading').hide();
                    },
                    error: function()
                    {
                        
                        alert('Failed show load SEM and SY!');
                        $('#loading').hide();
                        
                    }
                    
                });
                
            });
            
            function updateSemAndSY(id) {
                
                $('#sem_and_syUpdateModal').modal('show');
                
                $('#loading').show();
                
                $.ajax({
                    
                    url: 'actions.php',
                    type: 'POST',
                    data: {action: 'showUpdateSemAndSY', id: id},
                    dataType: 'json',
                    success: function(result)
                    {
                        $('#u_semester').val(result.semester);
                        $('#u_sem_term').val(result.term);
                        $('#u_school_year').val(result.school_year);
                        $('#u_status').val(result.status);
                        $('#loading').hide();
                    },
                    error: function()
                    {
                        
                        laert('Failed to load SEMD and SY data!');
                        $('#loading').hide();
                        
                    }
                    
                });
                
                $('#updateSemAndSYForm').submit(function(e){
                    
                    e.preventDefault();
                    
                    $('#loading').show();
                    
                    $.ajax({
                        
                        url: 'actions.php',
                        type: 'POST',
                        data: $(this).serialize(),
                        dataType: 'html',
                        success: function(result)
                        {
                            $('#showSemAndSY').html(result);
                            $('#sem_and_syUpdateModal').modal('hide');
                            $('#text_content').text('Semester and School Year Successfully Updated!');
                            $('#successModal').modal('show');
                            $('#loading').hide();
                        },
                        error: function()
                        {
                            alert('Failed to update SEM and SY!');
                            $('#loading').hide();
                        }
                        
                    });
                    
                });
            }
        </script>
<?php
    include '../../template/footer.php';
?>