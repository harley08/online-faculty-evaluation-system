<?php
    $page_title = 'Percentage Settings';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <?php include 'link-directory.php';?>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-percent"></i> Percentage Settings</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                            <table class="table table-bordered" style="margin: 0px !important">
                                <thead>
                                    <tr>
                                        <th class="text-center">Evaluator</th>
                                        <th class="text-center">Percentage</th>
                                        <th class="text-center">Total Evaluators</th>
                                        <th class="text-center">Update</th>
                                    </tr>
                                </thead>
                                <tbody id="evaluatorsTypeData"></tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include '../../modals/percentage-settings-modals.php'; ?>
        <script src="../../ajax/percentage-settings-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>