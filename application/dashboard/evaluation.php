<?php
    $page_title = 'Evaluation';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
?>
    
        <style type="text/css">            
            .evaluation-data {
                margin: auto !important;
            }
            .evaluation {
                background-color: #ffffff;
                max-width: 8.5in;
                max-height: 11in;
                padding: 20px;
                margin: 10px auto;
                -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
                box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
            }
        </style>
        
        <div class="evaluation-data">
            <div class="evaluation shadow">
                <div class="text-center">The QCE of NBC No. 461</div>
                <div class="text-center">Instrument for Instruction/Teaching Effectiveness</div>
                <div class="text-center" style="margin-top: 10px">
                    <b>Rating Period:</b> ________________________ <b>to</b> ________________________
                </div>
                <div class="row" style="margin-top: 10px">
                        <div class="col-md-5">Name of Faculty:</div>
                    <div class="text-center" style="margin-top: 10px">
                        <div class="col-md-5">Academic Rank:</div>
                    </div>
                </div>
                <div style="margin-top: 10px">
                    Evaluators:
                </div>
                <div class="text-center" style="margin-top: 10px">
                    <table width="80%" style="margin: auto;">
                        <tr>
                            <td width="40%">
                                <i class="fa fa-check-square"></i> Self<br/>
                                <i class="fa fa-check-square"></i> Student
                            </td>
                            <td width="40%">
                                <i class="fa fa-check-square"></i> Peer<br/>
                                <i class="fa fa-check-square"></i> Supervisor
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="margin: 10px; margin-left: 30px;">
                    Instruction: Please evaluate the faculty using the scale below.
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Scale</th>
                            <th class="text-center">Descriptive Rating</th>
                            <th class="text-center">Qualitative Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">5</td>
                            <td class="text-center">Outstanding</td>
                            <td>The performance almost always exceeds the job requirements. The faculty is an exceptional role model.</td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td class="text-center">Very Satisfactory</td>
                            <td>The performance meets and often exceeds the job requirements.</td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td class="text-center">Satisfactory</td>
                            <td>The performance meets job requirements.</td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td class="text-center">Fair</td>
                            <td>The performance needs some development to meet the job requirements.</td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-center">Poor</td>
                            <td>The faculty fails to meet job requirements.</td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin: 10px;"></div>
                <table class="table table-bordered">
                    <tbody>
                        <?php
                            $categories;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
<?php
    include '../../template/footer.php';
?>