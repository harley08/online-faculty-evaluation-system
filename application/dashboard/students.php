<?php
    $page_title = 'Students';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addStudentModal" style="margin-bottom: 10px !important;"><small><i class="fa fa-plus"></i> Add Student</button></small>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Students</h4>
                    </div>
                    <div class="panel-body">
                        <select id="select_branch" class="form-control" style="width: 200px; text-align: center !important;">
                            <option value="">-- select branch --</option>
                            <option value="MBC_selected">MinSCAT Bongabong Campus</option>
                            <option value="MMC_selected">MinSCAT Main Campus</option>
                            <option value="MCC_selected">MinSCAT Calapan City Campus</option>
                        </select>
                        <div style="margin: 10px;"></div>
                        <div class="branch_option" id="MBC_selected" style="display: none">
                            
                            <?php
                            
                                $mbcians = $db_con->prepare("SELECT * FROM students WHERE branch = 'MBC'");
                                $mbcians->execute();
                                        
                            ?>
                            
                            <!-- MBC Students -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="mbcCourses">
                                                <option value="">-- select course --</option>
                                                <?php
                                                    
                                                    $mbc_courses = $db_con->prepare("SELECT * FROM courses WHERE branch = 'MBC'");
                                                    $mbc_courses->execute();
                                                    
                                                    if ($mbc_courses->rowCount() != null) {
                                                        
                                                        while ($mbcCourseData = $mbc_courses->fetch(PDO::FETCH_ASSOC)) {
                                                            
                                                            ?>
                                                <option value="<?php print($mbcCourseData['course_acronym']) ?>"><?php print($mbcCourseData['course_name']) ?></option>
                                                            <?php
                                                            
                                                        }
                                                        
                                                    } else {
                                                        
                                                        ?>
                                                <option value="">no data</option>
                                                        <?php
                                                        
                                                    }
                                                    
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="margin: 10px"></div>
                                    <div class="scroll-x">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">School ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Curr. Year</th>
                                                <th class="text-center">Email</th>
                                                <th class="text-center">Mobile</th>
                                                <th class="text-center">Access Code</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="mbcstudentsData"></tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="branch_option" id="MMC_selected" style="display: none">
                            
                            <?php
                            
                                $mmcians = $db_con->prepare("SELECT * FROM students WHERE branch = 'MMC'");
                                $mmcians->execute();
                            
                            ?>
                            
                            <!-- MMC Students -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="mmcCourses">
                                                <option value="">-- select course --</option>
                                                <?php
                                                    
                                                    $mmc_courses = $db_con->prepare("SELECT * FROM courses WHERE branch = 'MMC'");
                                                    $mmc_courses->execute();
                                                    
                                                    if ($mmc_courses->rowCount() != null) {
                                                        
                                                        while ($mmcCourseData = $mmc_courses->fetch(PDO::FETCH_ASSOC)) {
                                                            
                                                            ?>
                                                <option value="<?php print($mmcCourseData['course_acronym']) ?>"><?php print($mmcCourseData['course_name']) ?></option>
                                                            <?php
                                                            
                                                        }
                                                        
                                                    } else {
                                                        
                                                        ?>
                                                <option value="">no data</option>
                                                        <?php
                                                        
                                                    }
                                                    
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="margin: 10px"></div>
                                    <div class="scroll-x">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">School ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Curr. Year</th>
                                                <th class="text-center">Email</th>
                                                <th class="text-center">Mobile</th>
                                                <th class="text-center">Access Code</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="mmcstudentsData"></tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="branch_option" id="MCC_selected" style="display: none">
                            
                            <?php
                            
                                $mccians = $db_con->prepare("SELECT * FROM students WHERE branch = 'MCC'");
                                $mccians->execute();
                            
                            ?>
                            
                            <!-- MCC Students -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control" id="mccCourses">
                                                <option value="">-- select course --</option>
                                                <?php
                                                    
                                                    $mcc_courses = $db_con->prepare("SELECT * FROM courses WHERE branch = 'MCC'");
                                                    $mcc_courses->execute();
                                                    
                                                    if ($mcc_courses->rowCount() != null) {
                                                        
                                                        while ($mccCourseData = $mcc_courses->fetch(PDO::FETCH_ASSOC)) {
                                                            
                                                            ?>
                                                <option value="<?php print($mccCourseData['course_acronym']) ?>"><?php print($mccCourseData['course_name']) ?></option>
                                                            <?php
                                                            
                                                        }
                                                        
                                                    } else {
                                                        
                                                        ?>
                                                <option value="">no data</option>
                                                        <?php
                                                        
                                                    }
                                                    
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="margin: 10px"></div>
                                    <div class="scroll-x">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">School ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Curr. Year</th>
                                                <th class="text-center">Email</th>
                                                <th class="text-center">Mobile</th>
                                                <th class="text-center">Access Code</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="mccstudentsData"></tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <?php include '../../modals/students_modal.php'; ?>
        
        <script src="../../ajax/students.ajax.js" type="text/javascript"></script>
        
<?php
    include '../../template/footer.php';
?>