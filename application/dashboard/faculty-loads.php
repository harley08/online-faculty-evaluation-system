<?php
    $page_title = 'Faculty Loads';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
            <?php include 'sidebar.php'; ?>
            <?php include 'link-directory.php';?>
            <div class="col-md-9">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addSubjectModal" style="margin-bottom: 10px !important;"><small><i class="fa fa-plus"></i> Add Subject</small></button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Faculty Loads</h4>
                    </div>
                    <div class="panel-body">  
                        <ul id="branches" class="nav nav-tabs">
                            <li><a href="#mbc" data-toggle="tab">MBC</a></li>
                            <li><a href="#mmc" data-toggle="tab">MMC</a></li>
                            <li><a href="#mcc" data-toggle="tab">MCC</a></li>
                        </ul>
                        
                        <div id="branchesContent" class="tab-content">
                            <div class="tab-pane fade" id="mbc">
                                <div style="margin: 20px;"></div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="mbc_courses" id="mbc_courses" class="form-control">
                                                <option value="">-- sort by course --</option>
                                                <?php
                                                    $mbc_courses = $db_con->prepare("SELECT * FROM courses WHERE branch = 'MBC'");
                                                    $mbc_courses->execute();
                                                    
                                                    if ($mbc_courses->rowCount() != null) {
                                                        while ($mbc_course_data = $mbc_courses->fetch(PDO::FETCH_ASSOC)) {
                                                            
                                                            ?>
                                                <option value="<?php print($mbc_course_data['course_acronym']) ?>"><?php print($mbc_course_data['course_name']) ?></option>
                                                            <?php
                                                            
                                                        }
                                                    } else {
                                                        
                                                        ?>
                                                <option value="">-- no data yet --</option>
                                                        <?php
                                                
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin: 20px;"></div>
                                <div class="scroll-x">
                                    <div id="mbcSubjectsData"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="mmc">
                                <div style="margin: 20px;"></div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="mmc_courses" id="mmc_courses" class="form-control">
                                                <option value="">-- sort by course --</option>
                                                <?php
                                                    $mmc_courses = $db_con->prepare("SELECT * FROM courses WHERE branch = 'MMC'");
                                                    $mmc_courses->execute();
                                                    
                                                    if ($mmc_courses->rowCount() != null) {
                                                        while ($mmc_course_data = $mmc_courses->fetch(PDO::FETCH_ASSOC)) {
                                                            
                                                            ?>
                                                <option value="<?php print($mmc_course_data['course_acronym']) ?>"><?php print($mmc_course_data['course_name']) ?></option>
                                                            <?php
                                                            
                                                        }
                                                    } else {
                                                        
                                                        ?>
                                                <option value="">-- no data yet --</option>
                                                        <?php
                                                
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin: 20px;"></div>
                                <div class="scroll-x">
                                    <div id="mmcSubjectsData"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="mcc">
                                <div style="margin: 20px;"></div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="mcc_courses" id="mcc_courses" class="form-control">
                                                <option value="">-- sort by course --</option>
                                                <?php
                                                    $mcc_courses = $db_con->prepare("SELECT * FROM courses WHERE branch = 'MCC'");
                                                    $mcc_courses->execute();
                                                    
                                                    if ($mmc_courses->rowCount() != null) {
                                                        while ($mcc_course_data = $mcc_courses->fetch(PDO::FETCH_ASSOC)) {
                                                            
                                                            ?>
                                                <option value="<?php print($mcc_course_data['course_acronym']) ?>"><?php print($mcc_course_data['course_name']) ?></option>
                                                            <?php
                                                            
                                                        }
                                                    } else {
                                                        
                                                        ?>
                                                <option value="">-- no data yet --</option>
                                                        <?php
                                                
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin: 20px;"></div>
                                <div class="scroll-x">
                                    <div id="mccSubjectsData"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            </div>
        </div>

        
        <?php include '../../modals/faculty-loads_modals.php'; ?>

        <script src="../../ajax/faculty-loads_ajax.js" type="text/javascript"></script>
        <script src="../../ajax/mbc_subjects_ajax.js" type="text/javascript"></script>
        <script src="../../ajax/mmc_subjects_ajax.js" type="text/javascript"></script>
        <script src="../../ajax/mcc_subjects_ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>