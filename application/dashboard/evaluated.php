<?php    
    $page_title = 'Evaluations';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    }
    
    if ($my_role != 'Administrator') {
            header("Location: $base_url/application/evaluate");
    }
    
    if (isset($_GET['evaluation_type']) && !empty($_GET['evaluation_type']) && isset($_GET['f_id']) && !empty($_GET['f_id'])) {
        $evaluation_type = $_GET['evaluation_type'];
        $f_id = $_GET['f_id'];
        if ($evaluation_type == 'student') {
            $title = 'Student Evaluations';
        } else if ($evaluation_type == 'peer') {
            $title = "Peer Evaluations";
        } else if ($evaluation_type == 'supervisor') {
            $title = "Supervisor Evaluations";
        } else if ($evaluation_type == 'self') {
            $title = "Self Evaluation";
        }
        
        $get_faculy_info = $db_con->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
        $get_faculy_info->bindparam(':f_id', $f_id);
        $get_faculy_info->execute();
        
        $faculty_info = $get_faculy_info->fetch(PDO::FETCH_ASSOC);
        
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
?>
        <div class="container">
            <div class="row">
                <?php include 'sidebar.php'; ?>
                <div class="col-md-9">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo $base_url ?>">Home</a></li>
                        <li><a href="<?php echo $base_url ?>">Dashboard</a></li>
                        <li><a href="current-evaluations.php">Current Evaluations</a></li>
                        <li>Evaluations</li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><b><i class="fa fa-user"></i> <?php echo $faculty_info['firstname']." ".$faculty_info['lastname'] ?></b> | <?php echo $title ?></h4>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <div id="fetch">
                                    <h3>
                                        <p>Fetching evaluations in <b><span id="time"></span></b></p>
                                    </h3>
                                    <div style="margin: 15px;"></div>
                                </div>
                                <div id="show" style="display: none;">
                                    <form id="showEvaluationsForm">
                                        <input type="hidden" name="evaluation_type" value="<?php echo $_GET['evaluation_type'] ?>" />
                                        <input type="hidden" name="f_id" value="<?php echo $_GET['f_id'] ?>" />
                                        <input type="hidden" name="action" value="showEvaluationsData" />
                                        <button type="submit" class="btn btn-default btn-lg btn-block"><i class="fa fa-search"></i> Show Evaluation Results</button>
                                    </form>
                                </div>
                            </div>
                            <div id="evaluationsData"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script src="../../ajax/evaluated.js" type="text/javascript"></script>
        
<?php
    include '../../template/footer.php';
?>