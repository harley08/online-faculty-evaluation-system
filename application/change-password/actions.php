<?php
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        
        $action = $_POST['action'];
        
        switch($action) {
            case 'chk_curr_pw':
                chk_curr_pw();
                break;
            case 'chk_new_pw':
                chk_new_pw();
                break;
            default :
                break;
        }
        
    }
    
    function chk_curr_pw() {
        
        include '../../config/config.php';
        
        $pw_txt = $_POST['pw_txt'];
        
        $data->chk_curr_pw($pw_txt, $my_userCode);
        
    }
    
    function chk_new_pw() {
        
        include '../../config/config.php';
        
        $pw_txt = $_POST['pw_txt'];
        
        $data->chk_new_pw($pw_txt);
        
    }
?>