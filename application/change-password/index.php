<?php
    $page_title = 'Change Password';
    include '../../config/config.php';
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
    
?>

        <div class="container">   
            <div class="panel panel-default" style="margin: auto; max-width: 500px;">
                <div class="panel-heading">
                    <h4 class="panel-title text-center"><i class="fa fa-lock"></i> Change my Account Password</h4>
                </div>
                <div class="panel-body">
                    <form id="updatePasswordForm">
                        <div class="form-group">
                            <label for="curr_pw">Current Password:</label> <span id="chk_curr_pw"></span>
                            <input type="password" name="curr_pw" id="curr_pw" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="new_pw">New Password:</label> <span id="chk_new_pw"></span>
                            <input type="password" name="new_pw" id="new_pw" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="rt_new_pw">Re-type New Password:</label> <span id="chk_rt_new_pw"></span>
                            <input type="password" name="rt_new_pw" id="rt_new_pw" class="form-control" required />
                        </div>
                        <input type="hidden" name="action" value="changePassword" />
                        <button type="submit" class="btn btn-success">Change Password</button>
                    </form>
                </div>
            </div>
        </div>

        <?php include '../../modals/change-password-modals.php'; ?>
        <script src="../../ajax/change-password-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>