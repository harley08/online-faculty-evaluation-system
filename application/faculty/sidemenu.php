					<?php

					$get_stds = $db_con->prepare("SELECT * FROM students WHERE branch = :branch");
				    $get_stds->bindparam(":branch", $my_branch);
				    $get_stds->execute();
				    
				    while ($stds = $get_stds->fetch(PDO::FETCH_ASSOC)) {
				        
				        $std_subjs = $db_con->prepare("SELECT * FROM subjects WHERE "
				                . "branch = :branch AND "
				                . "course = :course AND "
				                . "curriculum_year = :curriculum_year");
				        $std_subjs->bindparam(":branch", $stds['branch']);
				        $std_subjs->bindparam(":course", $stds['course']);
				        $std_subjs->bindparam(":curriculum_year", $stds['curriculum_year']);
				        $std_subjs->execute();
				        
				    }

				    /* Get the total faculty evaluators per type of evaluator */

				    /* [Peer] Total Evaluators (Based on System Settings) */
				    $get_t_peer_evaluators = $db_con->prepare("SELECT * FROM evaluator_types WHERE evaluator = 'peer'");
				    $get_t_peer_evaluators->execute();

				    $t_peer_evaluators = $get_t_peer_evaluators->fetch(PDO::FETCH_ASSOC);


				    /* [Supervisor] Total Evaluators (Based on System Settings) */
				    $get_t_supervisor_evaluators = $db_con->prepare("SELECT * FROM evaluator_types WHERE evaluator = 'supervisor'");
				    $get_t_supervisor_evaluators->execute();

				    $t_supervisor_evaluators = $get_t_supervisor_evaluators->fetch(PDO::FETCH_ASSOC);

				    
				    /* Get the faculty Total Peers Evaluated */
				    $get_prof_peers_evaluated = $db_con->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
				            . "evaluation_type = 'peer' AND "
				            . "semester = :semester AND "
				            . "semestral_term = :semestral_term AND "
				            . "school_year = :school_year"
				            . " GROUP BY user_code");
				    $get_prof_peers_evaluated->bindparam(':f_id', $my_fac_id);
				    $get_prof_peers_evaluated->bindparam(":semester", $today_sem);
				    $get_prof_peers_evaluated->bindparam(":semestral_term", $today_sem_term);
				    $get_prof_peers_evaluated->bindparam(":school_year", $today_sy);
				    $get_prof_peers_evaluated->execute();
				    
				    /* Get the faculty total students evaluated */
				    $get_prof_stds_evaluated = $db_con->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
				            . "evaluation_type = 'student' AND "
				            . "semester = :semester AND "
				            . "semestral_term = :semestral_term AND "
				            . "school_year = :school_year"
				            . " GROUP BY user_code");
				    $get_prof_stds_evaluated->bindparam(':f_id', $my_fac_id);
				    $get_prof_stds_evaluated->bindparam(":semester", $today_sem);
				    $get_prof_stds_evaluated->bindparam(":semestral_term", $today_sem_term);
				    $get_prof_stds_evaluated->bindparam(":school_year", $today_sy);
				    $get_prof_stds_evaluated->execute();

				    
				    /* Get the faculty total supervisor evaluated */
				    $get_supervisor_stds_evaluated = $db_con->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
				            . "evaluation_type = 'supervisor' AND "
				            . "semester = :semester AND "
				            . "semestral_term = :semestral_term AND "
				            . "school_year = :school_year"
				            . " GROUP BY user_code");
				    $get_supervisor_stds_evaluated->bindparam(':f_id', $my_fac_id);
				    $get_supervisor_stds_evaluated->bindparam(":semester", $today_sem);
				    $get_supervisor_stds_evaluated->bindparam(":semestral_term", $today_sem_term);
				    $get_supervisor_stds_evaluated->bindparam(":school_year", $today_sy);
				    $get_supervisor_stds_evaluated->execute();

				    
				    /* Get the Total Students */
				    $get_courses = $db_con->prepare("SELECT * FROM courses");
				    $get_courses->execute();
				    
				    while ($course = $get_courses->fetch(PDO::FETCH_ASSOC)) {
				        
				        $get_subjs = $db_con->prepare("SELECT * FROM subjects WHERE course = :course AND f_id = :f_id GROUP BY COURSE");
				        $get_subjs->bindparam(":course", $course["course_acronym"]);
				        $get_subjs->bindparam(':f_id', $my_fac_id);
				        $get_subjs->execute();
				                
				        while ($subjects = $get_subjs->fetch(PDO::FETCH_ASSOC)) {
				                    
				            $get_stds = $db_con->prepare("SELECT * FROM students WHERE course = :course");
				            $get_stds->bindparam(":course", $course['course_acronym']);

				            $get_stds->execute();
				            
				            while ($stds = $get_stds->fetch(PDO::FETCH_ASSOC)) {
				                
				                $students[$stds["firstname"].$stds["lastname"]] = '1';
				                
				            }
				            
				        }
				        
				    }
				    
				    $t_stds = isset($students) ? count($students) : '0';

					?>

					<div style="height: 50px"></div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/avatar/no-avatar-male.png" width="90px" alt=""/>
                            </div>
                            <div class="text-center">
                                <h3><b><?php print($my_name) ?></b></h3>
                                <h4><?php print($my_role) ?></h4>
                            </div>
                        </div>
                        <ul class="nav-sidebar-menu">
                            <li>User Code: <span class="pull-right"><b><?php print($my_fac_userCode) ?></b></span></li>
                            <li>Students: <span class="pull-right"><?php echo $get_prof_stds_evaluated->rowCount() ?>/<?php echo $t_stds ?></span></li>
                            <li>Peer: <span class="pull-right"><?php echo $get_prof_peers_evaluated->rowCount() ?>/<?php echo $t_peer_evaluators['total_evaluators'] ?></span></li>
                            <li>Supervisor: <span class="pull-right"><?php echo $get_supervisor_stds_evaluated->rowCount() ?>/<?php echo $t_supervisor_evaluators['total_evaluators'] ?></span></li>
                            <li>
                                <a href="personal-rating.php">Personal Rating <span class="pull-right"><i class="fa fa-star"></i></span></a>
                            </li>
                        </ul>
                        <div class="panel-footer">
                            <h5 class="text-center"><b><i class="fa fa-calendar-o"></i> <?php echo date("F j, Y"); ?> <span id="time"></span></b></h5>
                        </div>
                    </div>