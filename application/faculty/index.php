<?php

    $page_title = 'Faculty';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    } else if ($my_role != 'Administrator' && $my_role == 'Student') {
        header("Location: $base_url/application/evaluate");
    } else if ($my_role == 'Supervisor') {
        header("Location: $base_url/application/supervisor");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';

?>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include 'sidemenu.php' ?>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar-o"></i> List of Evaluation Schedule</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                                <table class="table table-hover table-bordered" style="margin: 0px !important;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Professor</th>
                                            <th class="text-center">Evaluation Type</th>
                                            <th class="text-center">Start Date</th>
                                            <th class="text-center">End Date</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php include 'faculty-schedules.php'; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-calendar-o"></i> By-Campus Evaluation Schedule</h4>
                        </div>
                        <div class="panel-body">
                            <div class="scroll-x">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Campus</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $campus_schedules = $db_con->prepare("SELECT * FROM campus_schedules WHERE status = 'Active'");
                                            $campus_schedules->execute();
                                            
                                            if ($campus_schedules->rowCount() != null) {

                                                while ($campus_SchedData = $campus_schedules->fetch(PDO::FETCH_ASSOC)) {
                                                    
                                                    $branch = "";
                                                    
                                                    switch($campus_SchedData['branch']) {
                                                        case 'MBC':
                                                            $branch = "Bongabong Campus";
                                                            break;
                                                        case 'MMC':
                                                            $branch = "Main Campus";
                                                            break;;
                                                        case 'MCC':
                                                            $branch = "Calapan City Campus";
                                                            break;
                                                        default :
                                                            break;
                                                    }
                                                    
                                                    ?>
                                        <tr>
                                            <td><b><?php print($branch) ?></b></td>
                                            <td><?php print($campus_SchedData['start_date']) ?></td>
                                            <td><?php print($campus_SchedData['end_date']) ?></td>
                                        </tr>
                                                    <?php
                                                    
                                                }
                                                
                                            } else {
                                                
                                                ?>
                                        <tr>
                                            <td class="text-center" colspan="3">no active schedules yet</td>
                                        </tr>
                                                <?php                                                
                                                
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include '../../modals/faculty-evaluation-modals.php'; ?>
        <script src="../../ajax/faculty-evaluation-ajax.js" type="text/javascript"></script>
<?php
    include '../../template/footer.php';
?>