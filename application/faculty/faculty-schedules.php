<?php
    /* Self Schedule */
    $self_schedules = $db_con->prepare("SELECT * FROM self_schedules WHERE f_id = :id");
    $self_schedules->bindparam(':id', $me_faculty_data['f_id']);
    $self_schedules->execute();
    
    while ($self_SchedData = $self_schedules->fetch(PDO::FETCH_ASSOC)) {
        
        ?>
<tr>
    <td><b><?php echo $my_name ?></b></td>
    <td class="text-center">Self</td>
    <td><?php echo $self_SchedData['start_date'] ?></td>
    <td><?php echo $self_SchedData['end_date'] ?></td>
    <td class="text-center">
        <?php
        
        $evaluation_type = 'self';
        
        $checkIfEvaluated = $db_con->prepare("SELECT * FROM evaluation_results WHERE branch = :branch AND "
            . "user_code = :user_code AND "
            . "f_id = :f_id "
            . "AND semester = :semester AND "
            . "semestral_term = :semestral_term AND "
            . "school_year = :school_year AND "
            . "evaluation_type = :evaluation_type");
        $checkIfEvaluated->bindparam(":branch", $my_branch);
        $checkIfEvaluated->bindparam(":user_code", $my_userCode);
        $checkIfEvaluated->bindparam('f_id', $self_SchedData['f_id']);
        $checkIfEvaluated->bindparam(":semester", $today_sem);
        $checkIfEvaluated->bindparam(":semestral_term", $today_sem_term);
        $checkIfEvaluated->bindparam(":school_year", $today_sy);
        $checkIfEvaluated->bindparam(":evaluation_type", $evaluation_type);
        $checkIfEvaluated->execute();
                               
        if ($checkIfEvaluated->rowCount() == null && $fac_self_schedule->rowCount() == 1) {
                                   
        ?>
        <button type="button" onclick="evaluateSelf(<?php print($self_SchedData['f_id']) ?>)" class="btn-evaluate"><i class="fa fa-pencil-square-o"></i> Evaluate</button>
        <?php
                                   
        } else if ($checkIfEvaluated->rowCount() != null && $fac_self_schedule->rowCount() == 1) {
                                    
        ?>
        <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-pencil-square-o"></i> Evaluated</button>
        <?php
                                    
        } else if ($fac_self_schedule->rowCount() == 0) {
            
            ?>
        <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-calendar-times-o"></i> no schedule</button>
            <?php
            
        }
        
        ?>
    </td>
</tr>
        <?php
        
    }

    /* Peer Schedules */
    $peer_schedules = $db_con->prepare("SELECT * FROM peer_schedules WHERE user_code = :user_code AND "
            . "semester = :semester AND "
            . "semestral_term = :semestral_term AND "
            . "school_year = :school_year");
    $peer_schedules->bindparam(":user_code", $my_fac_userCode);
    $peer_schedules->bindparam(":semester", $today_sem);
    $peer_schedules->bindparam(":semestral_term", $today_sem_term);
    $peer_schedules->bindparam(":school_year", $today_sy);
    $peer_schedules->execute();
    
    while ($peer_SchedData = $peer_schedules->fetch(PDO::FETCH_ASSOC)) {
        
        /* Get the Peer Faculty Information */
        $get_peerInfo = $db_con->prepare("SELECT * FROM faculties WHERE f_id = :id");
        $get_peerInfo->bindparam(':id', $peer_SchedData['f_id']);
        $get_peerInfo->execute();
        $peerInfo = $get_peerInfo->fetch(PDO::FETCH_ASSOC);
        
        ?>
<tr>
    <td><b><?php echo $peerInfo['firstname'] . " " . $peerInfo['lastname'] ?></b></td>
    <td class="text-center">Peer</td>
    <td><?php echo $peer_SchedData['start_date'] ?></td>
    <td><?php echo $peer_SchedData['end_date'] ?></td>
    <td class="text-center">
        
        <?php
        
        /* Check if the Peer Schedule is on Date */
        $fac_peer_schedule = $db_con->prepare("SELECT * FROM peer_schedules WHERE "
                . "branch = :branch AND "
                . "f_id = :f_id AND "
                . "user_code = :user_code AND "
                . "start_date <= NOW() AND "
                . "end_date >= NOW() AND "
                . "semester = :semester AND "
                . "semestral_term = :semestral_term AND "
                . "school_year = :school_year");
        $fac_peer_schedule->bindparam(":branch", $my_branch);
        $fac_peer_schedule->bindparam(':f_id', $peer_SchedData['f_id']);
        $fac_peer_schedule->bindparam(":user_code", $my_userCode);
        $fac_peer_schedule->bindparam(":semester", $today_sem);
        $fac_peer_schedule->bindparam(":semestral_term", $today_sem_term);
        $fac_peer_schedule->bindparam(":school_year", $today_sy);
        $fac_peer_schedule->execute();
        
        $checkIfEvaluated = $db_con->prepare("SELECT * FROM evaluation_results WHERE branch = :branch AND "
            . "user_code = :user_code AND "
            . "f_id = :f_id "
            . "AND semester = :semester AND "
            . "semestral_term = :semestral_term AND "
            . "school_year = :school_year AND "
            . "evaluation_type = :evaluation_type");
        $checkIfEvaluated->bindparam(":branch", $my_branch);
        $checkIfEvaluated->bindparam(":user_code", $my_userCode);
        $checkIfEvaluated->bindparam('f_id', $peer_SchedData['f_id']);
        $checkIfEvaluated->bindparam(":semester", $today_sem);
        $checkIfEvaluated->bindparam(":semestral_term", $today_sem_term);
        $checkIfEvaluated->bindparam(":school_year", $today_sy);
        $checkIfEvaluated->bindparam(":evaluation_type", $evaluation_type);
        $checkIfEvaluated->execute();
                               
        if ($checkIfEvaluated->rowCount() == null && $fac_peer_schedule->rowCount() == 1) {
                                   
        ?>
        <button type="button" onclick="evaluateProfessor(<?php print($peer_SchedData['f_id']) ?>)" class="btn-evaluate"><i class="fa fa-pencil-square-o"></i> Evaluate</button>
        <?php
                                   
        } else if ($checkIfEvaluated->rowCount() != null && $fac_peer_schedule->rowCount() == 1) {
                                    
        ?>
        <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-pencil-square-o"></i> Evaluated</button>
        <?php
                                    
        } else if ($fac_peer_schedule->rowCount() == 0) {
            
            ?>
        <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-calendar-times-o"></i> no schedule</button>
            <?php
            
        }
        
        ?>
        
    </td>
</tr>
        <?php
        
    }
?>