<?php
    $page_title = 'Faculty';
    include '../../config/config.php';
    
    if ($not_logged_in) {
        header("Location: $base_url/application/login");
    } else if ($my_role != 'Administrator' && $my_role == 'Student') {
        header("Location: $base_url/application/evaluate");
    } else if ($my_role == 'Supervisor') {
        header("Location: $base_url/application/supervisor");
    }
    
    include '../../template/header.php';
    include '../../template/navigation-top.php';
    
?>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include 'sidemenu.php'; ?>
                </div>
                <div class="col-md-9">
                    <ol class="breadcrumb breadcrumb-arrow">
                        <li><a href="index.php">Home</a></li>
                        <li class="active"><span></span>Personal Rating</span></li>
                    </ol>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-star"></i> Personal Rating</h4>
                        </div>
                        <div class="panel-body"></div>
                    </div>
                </div>
                </div>
            </div>
        </div>
<?php
    include '../../template/footer.php';
?>