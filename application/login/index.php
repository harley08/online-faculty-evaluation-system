<?php
    include '../../config/config.php';
    $page_title = 'Login';
    include '../../template/header.php';
    
    if ($logged_in) {
        header("Location: $base_url/application/dashboard");
    }
?>

        <style>
            .footer {
                display: none;
            }
        </style>
        <div class="container">
            <div class="form-signin">
                <div class="text-center">
                    <img src="<?php echo $base_url ?>/assets/imgs/rateme-banner.png" width="60%" />
                </div>
                <div style="height: 60px;"></div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                            <img class="userphoto" src="<?php echo $base_url ?>/assets/imgs/minscat-logo.png" width="90px" alt=""/>
                            <h4 class="panel-title"><b>Login</b></h4>
                        </div>
                        <hr/>
                        <?php
                        
                        if (isset($_GET['login_error'])) {
                            
                            if ($_GET['login_error'] == 'failed') {
                                
                                ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-warning"></i> Invalid username or password!
                        </div>
                                <?php
                                
                            }
                            
                        }
                        
                        ?>
                        <form method="post" action="login.php">
                            <div class="form-group">
                                <label for="user_code">User Code:</label>
                                <div class="input-group">
                                    <input type="text" name="user_code" class="form-control" placeholder="0000-0000" />
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" name="password" id="password" class="form-control" data-toggle="password" placeholder="***********" />
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Login</button>
                            <a href="<?php echo $base_url ?>/application/registration" class="btn btn-default btn-block">Create Account</a>
                        </form>
                    </div>
                </div>
                <center>
                    <p>2017-<?php echo date('Y') ?>. All Rights Reserved. <b>RateME</b></p>
                    <div style="margin: 15px;"></div>
                    <a class="btn btn-default" href="<?php echo $base_url ?>/download/rateme+online.apk"><i class="fa fa-android"></i> <i class="fa fa-download"></i> Download RateME App Now!</a>
                </center>
            </div>
        </div>
        <script type="text/javascript">
            $("#password").password('toggle');
        </script>
<?php
    include '../../template/footer.php';
?>