<?php
    class data {
        
        private $db;
        
        function __construct($db_con) {
            $this->db = $db_con;
        }
        
        
        var $base_url = "http://localhost/rateme"; 
        
        public function login($user_code, $password){
            
            try {
                
                global $base_url;
                
                $stmt = $this->db->prepare("SELECT * FROM users WHERE user_code = :user_code AND password = :password");
                $stmt->bindparam(":user_code", $user_code);
                $stmt->bindparam(":password", $password);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                if ($stmt->rowCount() == 1 && $row['account_status'] == 'Active' && $row['role'] == 'Administrator') {
                    
                    $_SESSION['user_id'] = $row['user_id'];
                    header("Location: $base_url/application/dashboard");
                    
                } else if ($stmt->rowCount() == 1 && $row['account_status'] != 'Active'){
                    
                    header("Location: $base_url/application/verify-account");
                    
                } else if ($stmt->rowCount() == 1 && $row['account_status'] == 'Active' && $row['role'] == 'Student') {
                    
                    $_SESSION['user_id'] = $row['user_id'];
                    header("Location: $base_url/application/evaluate");
                    
                } else if ($stmt->rowCount() == 1 && $row['account_status'] == 'Active' && $row['role'] == 'Faculty') {
                    
                    $_SESSION['user_id'] = $row['user_id'];
                    header("Location: $base_url/application/faculty");
                    
                } else if ($stmt->rowCount() == 1 && $row['account_status'] == 'Active' && $row['role'] == 'Supervisor') {
                    
                    $_SESSION['user_id'] = $row['user_id'];
                    header("Location: $base_url/application/supervisor");
                    
                } else if ($stmt->rowCount() != 1) {
                    
                    header("Location: $base_url/application/login?login_error=failed");
                    
                }
                                               
            } catch (PDOException $ex) {
                
                $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function loadSubjectsPerBranch($branch)
        {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM courses WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                echo '<option value="">-- select course --</option>'; 
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>                    
                    <option value="<?php print($row['course_acronym']) ?>"><?php print($row['course_name']) ?></option>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <option value="">no courses yet</option>
                    <?php
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addStudent($branch, $firstname, $middlename, $lastname, $school_id, $course, $curriculum_year, $email, $mobile_no) {
            
            try {
                
                function generateAccessCode($length = 8) {
                    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
                }
                
                $access_code = generateAccessCode();
                
                $stmt = $this->db->prepare("INSERT INTO students (school_id, firstname, middlename, lastname, course, curriculum_year, email, mobile, access_code, branch) VALUES "
                        . "(:school_id, :firstname, :middlename, :lastname, :course, :curriculum_year, :email, :mobile, :access_code, :branch)");
                $stmt->bindparam(":school_id", $school_id);
                $stmt->bindparam(":firstname", $firstname);
                $stmt->bindparam(":middlename", $middlename);
                $stmt->bindparam(":lastname", $lastname);
                $stmt->bindparam(":course", $course);
                $stmt->bindparam(':curriculum_year', $curriculum_year);
                $stmt->bindparam(":email", $email);
                $stmt->bindparam(":mobile", $mobile_no);
                $stmt->bindparam(":access_code", $access_code);
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
            } catch (Exception $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addFaculty($branch, $firstname, $middlename, $lastname, $employmentStatus, $email, $mobile, $role) {
            
            try {
                
                function generateFacultyCode($length = 4) {
                    return substr(str_shuffle(str_repeat($x='012345678901234567890123456789012345678901234567890123456789', ceil($length/strlen($x)) )),1,$length);
                }
                
                $f_code = "f" . generateFacultyCode();
                
                function generateAccessCode($length = 8) {
                    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
                }
                
                $access_code = generateAccessCode();
                
                $stmt = $this->db->prepare("INSERT INTO faculties (branch, f_code, firstname, middlename, lastname, employment_status, email, mobile, role, access_code) VALUES "
                        . "(:branch, :f_code, :firstname, :middlename, :lastname, :employment_status, :email, :mobile, :role, :access_code)");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":f_code", $f_code);
                $stmt->bindparam(":firstname", $firstname);
                $stmt->bindparam(":middlename", $middlename);
                $stmt->bindparam(":lastname", $lastname);
                $stmt->bindparam(":employment_status", $employmentStatus);
                $stmt->bindparam(":email", $email);
                $stmt->bindparam(":mobile", $mobile);
                $stmt->bindparam(":role", $role);
                $stmt->bindparam(":access_code", $access_code);
                $stmt->execute();
                
                return true;
                
            } catch (Exception $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showFacultyData($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                <tr>
                    <td class="text-center"><b><?php print($row['f_code']) ?></b></td>
                    <td class="text-center"><?php print($row['firstname'].' '.$row['lastname']) ?></td>
                    <td class="text-center"><?php print($row['employment_status']) ?></td>
                    <td class="text-center"><?php print($row['email']) ?></td>
                    <td class="text-center"><?php print($row['mobile']) ?></td>
                    <td class="text-center"><b><?php print($row['access_code']) ?></b></td>
                    <td class="text-center"><?php print($row['role']) ?></td>
                    <td class="text-center">
                        <button type="button" class="btn btn-default" style="margin: 2px;" onclick="viewFacultyInfo(<?php print($row['f_id']) ?>)">
                            <i class="fa fa-search"></i>
                        </button>
                        <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateFaculty(<?php print($row['f_id']) ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteFaculty(<?php print($row['f_id']) ?>, '<?php print($row['branch']) ?>')">
                            <i class="fa fa-remove"></i>
                        </button>
                    </td>
                </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <tr>
                    <td colspan="8" class="text-center">no data yet</td>
                </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showCategories() {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM categories");
                $stmt->fetch(PDO::FETCH_ASSOC);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                <tr>
                    <td class="text-center"><?php print($row['category']) ?></td>
                    <td class="text-center"><?php print($row['category_name']) ?></td>
                    <td class="text-center"><?php print($row['percentage']) ?></td>
                    <td class="text-center"><?php print($row['status']) ?></td>
                    <td class="text-center">
                        <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateCategory(<?php print($row['category_id']) ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteCategory(<?php print($row['category_id']) ?>)">
                            <i class="fa fa-remove"></i>
                        </button>
                    </td>
                </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <tr>
                    <td class="text-center" colspan="4">no data</td>
                </tr>
                    <?php
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addCategory($category_name, $percentage, $status) {
            
            try {
                
                /* Check Category */
                $categ = $this->db->prepare("SELECT * FROM categories ORDER BY category_id DESC");
                $categ->execute();
                            
                $categData = $categ->fetch(PDO::FETCH_ASSOC);
                
                $category = "";
                
                if ($categ->rowCount() == null) {
                    $category = 'A';
                } else if ($categData['category'] == 'A') {
                    $category = 'B';
                } else if ($categData['category'] == 'B') {
                    $category = 'C';
                } else if ($categData['category'] == 'C') {
                    $category = 'D';
                }
                
                $stmt = $this->db->prepare("INSERT INTO categories (category, category_name, percentage, status) VALUES (:category, :category_name, :percentage, :status)");
                $stmt->bindparam(":category", $category);
                $stmt->bindparam(":category_name", $category_name);
                $stmt->bindparam(":percentage", $percentage);
                $stmt->bindparam(":status", $status);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showQuestionsData() {
            
            try {
                
                $categories = $this->db->prepare("SELECT * FROM categories");
                $categories->execute();
                
                if ($categories->rowCount() != null) {
                    
                    while ($categ = $categories->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                <div class="alert alert-success" style="padding: 5px !important; border-bottom-left-radius: 0px !important; ; border-bottom-right-radius: 0px !important; padding-left: 20px !important; padding-right: 20px !important; margin-bottom: 0px !important; margin-top: 5px !important;">
                    <b><?php print($categ['category']) ?>. <?php print($categ['category_name']) ?></b>
                    <span class="pull-right"><small><?php print($categ['status']) ?></small></span>
                </div>
                <table class="table table-striped" style="border: 1px solid #a9a9a9; border-top: 0px solid #ffffff;">
                    <thead>
                        <tr>
                            <th>Question</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php
                        
                        $questions = $this->db->prepare("SELECT * FROM questions WHERE category = :category");
                        $questions->bindparam(":category", $categ['category']);
                        $questions->execute();
                        
                        if ($questions->rowCount() != null) {
                            
                            for ($i = 1; $i <= $questions->rowCount(); $i++) {
                                
                                $quest = $questions->fetch(PDO::FETCH_ASSOC);
                                
                                ?>
                        <tr>
                            <td>
                                <?php print("<b>" . $i . ".</b> " .$quest['question']) ?>
                            </td>
                            <td class="text-center">
                                <?php print($quest['status']) ?>
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateQuestion(<?php print($quest['id']) ?>)">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteQuestion(<?php print($quest['id']) ?>)">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                                <?php
                                
                            }
                            
                        } else {
                            
                            ?>
                        <tr>
                            <td class="text-center" colspan="3">
                                no questions yet
                            </td>
                        </tr>
                            <?php
                            
                        }
                        
                        ?>
                        
                    </tbody>
                </table>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <div class="alert alert-warning">
                    <h3 class="text-center">no category yet</h3>
                </div>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addQuestion($category, $question, $status) {
            
            try {
                
                $stmt = $this->db->prepare("INSERT INTO questions (category, question, status) VALUES (:category, :question, :status)");
                $stmt->bindparam(":category", $category);
                $stmt->bindparam(":question", $question);
                $stmt->bindparam(":status", $status);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addSubject($branch, $course, $curriculum_year, $subject_code, $subject_description, $f_id) {
            
            try {
                
                $stmt = $this->db->prepare("INSERT INTO subjects (branch, course, curriculum_year, subject_code, subject_description, f_id) VALUES "
                        . "(:branch, :course, :curriculum_year, :subject_code, :subject_description, :f_id)");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":course", $course);
                $stmt->bindparam(':curriculum_year', $curriculum_year);
                $stmt->bindparam(":subject_code", $subject_code);
                $stmt->bindparam(":subject_description", $subject_description);
                $stmt->bindparam(':f_id', $f_id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function loadSubjects($course, $branch) {
            
            try {
                
                /* Load Subject For 1st Year */
                $firstYear = $this->db->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year	= 1");
                $firstYear->bindparam(":branch", $branch);
                $firstYear->bindparam(":course", $course);
                $firstYear->execute();
                
                
                /* Get the Course Data */
                $courseData = $this->db->prepare("SELECT * FROM courses WHERE course_acronym = :course_acronym");
                $courseData->bindparam(":course_acronym", $course);
                $courseData->execute();
                
                $getCourseData = $courseData->fetch(PDO::FETCH_ASSOC);
                
                ?>
                <div class="dotted-line"></div>
                <div class="text-center" style="margin-bottom: 20px;">
                    <b>
                        <?php print($getCourseData['course_name']) ?>
                    </b>
                </div>                
                
                <div class="alert alert-success" style="padding: 5px !important; border-bottom-left-radius: 0px !important; ; border-bottom-right-radius: 0px !important; padding-left: 20px !important; margin-bottom: 0px !important;"><b>1st Year</b></div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Subject Code</th>
                            <th class="text-center">Subject Description</th>
                            <th class="text-center">Professor</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if ($firstYear->rowCount() != null) {
                                
                                while ($firstYear_data = $firstYear->fetch(PDO::FETCH_ASSOC)) {
                                    
                                    $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                                    $get_prof->bindparam(":f_id", $firstYear_data['f_id']);
                                    $get_prof->execute();
                   
                                    $profData = $get_prof->fetch(PDO::FETCH_ASSOC);
                                    
                                    ?>
                        <tr>
                            <td class="text-center"><?php print($firstYear_data['subject_code']) ?></td>
                            <td class="text-center"><?php print($firstYear_data['subject_description']) ?></td>
                            <td class="text-center"><?php print($profData['firstname'] . " " . $profData['lastname']) ?></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateSubject(<?php print($firstYear_data['id']) ?>, '<?php print($firstYear_data['branch']) ?>', '<?php print($firstYear_data['course']) ?>')">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteSubject(<?php print($firstYear_data['id']) ?>, '<?php print($firstYear_data['branch']) ?>', '<?php print($firstYear_data['course']) ?>')">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                                    <?php
                                    
                                }
                                
                            } else {
                                
                                ?>
                        <tr>
                            <td class="text-center" colspan="4">no subjects yet</td>
                        </tr>
                                <?php
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="dotted-line"></div>
                <?php
                
                /* Load Subject For 2nd Year */
                $secondYear = $this->db->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year = 2");
                $secondYear->bindparam(":branch", $branch);
                $secondYear->bindparam(":course", $course);
                $secondYear->execute();
                
                ?>
                <div class="alert alert-success" style="padding: 5px !important; border-bottom-left-radius: 0px !important; ; border-bottom-right-radius: 0px !important; padding-left: 20px !important; margin-bottom: 0px !important;"><b>2nd Year</b></div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Subject Code</th>
                            <th class="text-center">Subject Description</th>
                            <th class="text-center">Professor</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if ($secondYear->rowCount() != null) {
                                
                                while ($secondYear_data = $secondYear->fetch(PDO::FETCH_ASSOC)) {
                                    
                                    $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                                    $get_prof->bindparam(":f_id", $secondYear_data['f_id']);
                                    $get_prof->execute();
                   
                                    $profData = $get_prof->fetch(PDO::FETCH_ASSOC);
                                    
                                    ?>
                        <tr>
                            <td class="text-center"><?php print($secondYear_data['subject_code']) ?></td>
                            <td class="text-center"><?php print($secondYear_data['subject_description']) ?></td>
                            <td class="text-center"><?php print($profData['firstname'] . " " . $profData['lastname']) ?></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateSubject(<?php print($firstYear_data['id']) ?>, '<?php print($firstYear_data['branch']) ?>', '<?php print($firstYear_data['course']) ?>')">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteSubject(<?php print($secondYear_data['id']) ?>, '<?php print($secondYear_data['branch']) ?>', '<?php print($secondYear_data['course']) ?>')">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                                    <?php
                                    
                                }
                                
                            } else {
                                
                                ?>
                        <tr>
                            <td class="text-center" colspan="4">no subjects yet</td>
                        </tr>
                                <?php
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="dotted-line"></div>
                <?php
                
                /* Load Subject For 3rd Year */
                $thirdYear = $this->db->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year = 3");
                $thirdYear->bindparam(":branch", $branch);
                $thirdYear->bindparam(":course", $course);
                $thirdYear->execute();
                
                ?>
                <div class="alert alert-success" style="padding: 5px !important; border-bottom-left-radius: 0px !important; ; border-bottom-right-radius: 0px !important; padding-left: 20px !important; margin-bottom: 0px !important;"><b>Third Year</b></div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Subject Code</th>
                            <th class="text-center">Subject Description</th>
                            <th class="text-center">Professor</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if ($thirdYear->rowCount() != null) {
                                
                                while ($thirdYear_data = $thirdYear->fetch(PDO::FETCH_ASSOC)) {
                                    
                                    $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                                    $get_prof->bindparam(":f_id", $thirdYear_data['f_id']);
                                    $get_prof->execute();
                   
                                    $profData = $get_prof->fetch(PDO::FETCH_ASSOC);
                                    
                                    ?>
                        <tr>
                            <td class="text-center"><?php print($thirdYear_data['subject_code']) ?></td>
                            <td class="text-center"><?php print($thirdYear_data['subject_description']) ?></td>
                            <td class="text-center"><?php print($profData['firstname'] . " " . $profData['lastname']) ?></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateSubject(<?php print($firstYear_data['id']) ?>, '<?php print($firstYear_data['branch']) ?>', '<?php print($firstYear_data['course']) ?>')">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteSubject(<?php print($thirdYear_data['id']) ?>, '<?php print($thirdYear_data['branch']) ?>', '<?php print($thirdYear_data['course']) ?>')">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                                    <?php
                                    
                                }
                                
                            } else {
                                
                                ?>
                        <tr>
                            <td class="text-center" colspan="4">no subjects yet</td>
                        </tr>
                                <?php
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="dotted-line"></div>
                <?php
                
                /* Load Subject For 4th Year */
                $fourthYear = $this->db->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year = 4");
                $fourthYear->bindparam(":branch", $branch);
                $fourthYear->bindparam(":course", $course);
                $fourthYear->execute();
                
                ?>
                <div class="alert alert-success" style="padding: 5px !important; border-bottom-left-radius: 0px !important; ; border-bottom-right-radius: 0px !important; padding-left: 20px !important; margin-bottom: 0px !important;"><b>Fourth Year</b></div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Subject Code</th>
                            <th class="text-center">Subject Description</th>
                            <th class="text-center">Professor</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if ($thirdYear->rowCount() != null) {
                                
                                while ($fourthYear_data = $fourthYear->fetch(PDO::FETCH_ASSOC)) {
                                    
                                    $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                                    $get_prof->bindparam(":f_id", $fourthYear_data['f_id']);
                                    $get_prof->execute();
                   
                                    $profData = $get_prof->fetch(PDO::FETCH_ASSOC);
                                    
                                    ?>
                        <tr>
                            <td class="text-center"><?php print($fourthYear_data['subject_code']) ?></td>
                            <td class="text-center"><?php print($fourthYear_data['subject_description']) ?></td>
                            <td class="text-center"><?php print($profData['firstname'] . " " . $profData['lastname']) ?></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateSubject(<?php print($firstYear_data['id']) ?>, '<?php print($firstYear_data['branch']) ?>', '<?php print($firstYear_data['course']) ?>')">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteSubject(<?php print($fourthYear_data['id']) ?>, '<?php print($fourthYear_data['branch']) ?>', '<?php print($thirdYear_data['course']) ?>')">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                                    <?php
                                    
                                }
                                
                            } else {
                                
                                ?>
                        <tr>
                            <td class="text-center" colspan="4">no subjects yet</td>
                        </tr>
                                <?php
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="dotted-line"></div>
                <?php
                
                /* Load Subject For 5th Year */
                $fifthYear = $this->db->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year = 5");
                $fifthYear->bindparam(":branch", $branch);
                $fifthYear->bindparam(":course", $course);
                $fifthYear->execute();
                
                ?>
                <div class="alert alert-success" style="padding: 5px !important; border-bottom-left-radius: 0px !important; ; border-bottom-right-radius: 0px !important; padding-left: 20px !important; margin-bottom: 0px !important;"><b>Fifth Year</b></div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Subject Code</th>
                            <th class="text-center">Subject Description</th>
                            <th class="text-center">Professor</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if ($fifthYear->rowCount() != null) {
                                
                                while ($fifthYear_data = $fifthYear->fetch(PDO::FETCH_ASSOC)) {
                                    
                                    $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                                    $get_prof->bindparam(":f_id", $fifthYear_data['f_id']);
                                    $get_prof->execute();
                   
                                    $profData = $get_prof->fetch(PDO::FETCH_ASSOC);
                                    
                                    ?>
                        <tr>
                            <td class="text-center"><?php print($fifthYear_data['subject_code']) ?></td>
                            <td class="text-center"><?php print($fifthYear_data['subject_description']) ?></td>
                            <td class="text-center"><?php print($profData['firstname'] . " " . $profData['lastname']) ?></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateSubject(<?php print($firstYear_data['id']) ?>, '<?php print($firstYear_data['branch']) ?>', '<?php print($firstYear_data['course']) ?>')">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteSubject(<?php print($fifthYear_data['id']) ?>, '<?php print($fifthYear_data['branch']) ?>', '<?php print($fifthYear_data['course']) ?>')">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                                    <?php
                                    
                                }
                                
                            } else {
                                
                                ?>
                        <tr>
                            <td class="text-center" colspan="4">no subjects yet</td>
                        </tr>
                                <?php
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="dotted-line"></div>
                <?php
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function checkStudent($branch, $firstname, $middlename, $lastname, $school_id, $course) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM students WHERE branch = :branch AND firstname = :firstname AND middlename = :middlename AND lastname = :lastname AND school_id = :school_id AND course = :course");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":firstname", $firstname);
                $stmt->bindparam(":middlename", $middlename);
                $stmt->bindparam(":lastname", $lastname);
                $stmt->bindparam(":school_id", $school_id);
                $stmt->bindparam(":course", $course);
                $stmt->execute();
                
                if ($stmt->rowCount() == 1) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $username = $row['school_id'];
                        $id = $row['id'];
                        
                        echo json_encode(array("id" => $id, "username" => $username));
                        return true;
                        
                    }
                                        
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                $ex->getMessage();
                return $ex;
                
            }
            
        }
        
        public function checkFaculty($branch, $firstname, $middlename, $lastname) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch AND "
                        . "firstname = :firstname AND "
                        . "middlename = :middlename AND "
                        . "lastname = :lastname");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":firstname", $firstname);
                $stmt->bindparam(":middlename", $middlename);
                $stmt->bindparam(":lastname", $lastname);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $id = $row['f_id'];
                        $username = $row['f_code'];
                        
                        echo json_encode(array("id" => $id, "username" => $username));
                        return true;
                        
                    }
                    
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function createStudentAccount($id, $username, $password) {
            
            try {
                
                $role = 'Student';
                $account_status = 'Inactive';
                
                $stmt = $this->db->prepare("INSERT INTO users (std_id, user_code, password, role, account_status) VALUES (:std_id, :user_code, :password, :role, :account_status)");
                $stmt->bindparam(':std_id', $id);
                $stmt->bindparam(":user_code", $username);
                $stmt->bindparam(":password", md5($password));
                $stmt->bindparam(":role", $role);
                $stmt->bindparam(":account_status", $account_status);
                $stmt->execute();
                
                /* Get the faculty email */
                $get_email = $this->db->prepare("SELECT * FROM students WHERE f_code = :f_code");
                $get_email->bindparam(":school_id", $username);
                $get_email->execute();
                
                $email = $get_email->fetch(PDO::FETCH_ASSOC);
                
                $to = $email['email'];
                $subject = 'RateME Account Access Code';
                $message = 'This is your account access code. Use this in activating your RateME account: '.$email['access_code'];
                $from = "From: Harley Lumagui <info@rateme.x10host.com>";
                mail($to,$subject,$message,$from);
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function createFacultyAccount($id, $username, $password) {
            
            try {
                
                $role = 'Faculty';
                $account_status = 'Inactive';
                
                $stmt = $this->db->prepare("INSERT INTO users (std_id, user_code, password, role, account_status) VALUES "
                        . "(:std_id, :user_code, :password, :role, :account_status)");
                $stmt->bindparam(':std_id', $id);
                $stmt->bindparam(":user_code", $username);
                $stmt->bindparam(":password", md5($password));
                $stmt->bindparam(":role", $role);
                $stmt->bindparam(":account_status", $account_status);
                $stmt->execute();
                
                /* Get the faculty email */
                $get_email = $this->db->prepare("SELECT * FROM faculties WHERE f_code = :f_code");
                $get_email->bindparam(":f_code", $username);
                $get_email->execute();
                
                $email = $get_email->fetch(PDO::FETCH_ASSOC);
                
                $to = $email['email'];
                $subject = 'RateME Account Access Code';
                $message = 'This is your account access code. Use this in activating your RateME account: '.$email['access_code'];
                $from = "From: Harley Lumagui <info@rateme.x10host.com>";
                mail($to,$subject,$message,$from);
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function activateAccount($user_code, $activation_code) {
            
            try {
                
                $check_student = $this->db->prepare("SELECT * FROM students WHERE school_id = :school_id AND access_code = :access_code");
                $check_student->bindparam(":school_id", $user_code);
                $check_student->bindparam(":access_code", $activation_code);
                $check_student->execute();
                
                if ($check_student->rowCount() == 1) {
                    
                    $act_stud = $this->db->prepare("UPDATE users SET account_status = 'Active' WHERE user_code = :user_code");
                    $act_stud->bindparam(":user_code", $user_code);
                    $act_stud->execute();
                    
                    return true;
                    
                } else if ($check_student->rowCount() == null) {
                    
                    $check_faculty = $this->db->prepare("SELECT * FROM faculties WHERE f_code = :f_code AND access_code = :access_code");
                    $check_faculty->bindparam(":f_code", $user_code);
                    $check_faculty->bindparam(":access_code", $activation_code);
                    $check_faculty->execute();
                    
                    if ($check_faculty->rowCount() == 1) {
                        
                        
                        $activ_fac = $this->db->prepare("UPDATE users SET account_status = 'Active' WHERE user_code = :user_code");
                        $activ_fac->bindparam(":user_code", $user_code);
                        $activ_fac->execute();
                        
                        return true;
                        
                    } else {
                        
                        return false;
                        
                    }
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showStudents($branch, $course) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM students WHERE branch = :branch AND course = :course ORDER BY curriculum_year");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":course", $course);
                $stmt->execute();
                
                if ($stmt->rowCount()) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $curr_yr = "";
                        switch($row['curriculum_year']) {
                            case 1:
                                $curr_yr = "1st Yr.";
                                break;
                            case 2:
                                $curr_yr = "2nd Yr.";
                                break;
                            case 3:
                                $curr_yr = "3rd Yr.";
                                break;
                            case 4:
                                $curr_yr = "4th Yr.";
                                break;
                            case 5:
                                $curr_yr = "5th Yr.";
                                break;
                            default :
                                break;
                        }
                        
                        ?>
                <tr>
                    <td class="text-center">
                        <?php print($row['school_id']) ?>
                    </td>
                    <td class="text-center">
                        <b><?php print($row['firstname'] . " " . $row['lastname']) ?></b>
                    </td>
                    <td class="text-center">
                        <?php print($curr_yr) ?>
                    </td>
                    <td class="text-center">
                        <?php print($row['email']) ?>
                    </td>
                    <td class="text-center">
                        <?php print($row['mobile']) ?>
                    </td>
                    <td class="text-center">
                        <b><?php print($row['access_code']) ?></b>
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateStudent(<?php print($row['id']) ?>, '<?php print($row['branch']) ?>')">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteStudent(<?php print($row['id']) ?>, '<?php print($row['branch']) ?>', '<?php print($row['course']) ?>')">
                            <i class="fa fa-remove"></i>
                        </button>
                    </td>
                </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <tr>
                    <td class="text-center" colspan="7">no data</td>
                </tr>
                    <?php
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateStudent($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM students WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $branch = $row['branch'];
                        $firstname = $row['firstname'];
                        $middlename = $row['middlename'];
                        $lastname = $row['lastname'];
                        $school_id = $row['school_id'];
                        $course = $row['course'];
                        $curriculum_year = $row['curriculum_year'];
                        $email = $row['email'];
                        $mobile = $row['mobile'];
                        
                        echo json_encode(array("branch" => $branch, "firstname" => $firstname, "middlename" => $middlename, "lastname" => $lastname, "school_id" => $school_id, "course" => $course, "curriculum_year" => $curriculum_year, "email" => $email, "mobile" => $mobile));
                        
                        return true;
                        
                    }
                    
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function loadCoursesPerBranch($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM courses WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    echo '<option value="" disable="true">-- select course --</option>';
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                <option value="<?php print($row['course_acronym']) ?>"><?php print($row['course_name']) ?></option>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <option value="">-- no courses yet --</option>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deleteStudent($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM students WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deleteCategory($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM categories WHERE category_id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateCategory($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM categories WHERE category_id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $category_name = $row['category_name'];
                        $percentage = $row['percentage'];
                        $status = $row['status'];
                        
                        echo json_encode(array("category_name" => $category_name, "percentage" => $percentage, "status" => $status));
                        
                    }
                    
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateCategory($id, $category_name, $percentage, $status) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE categories SET category_name = :category_name, percentage = :percentage, status = :status WHERE category_id = :id");
                $stmt->bindparam(":category_name", $category_name);
                $stmt->bindparam(':percentage', $percentage);
                $stmt->bindparam(":status", $status);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (Exception $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deleteQuestion($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM questions WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateQuestion($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM questions WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $category = $row['category'];
                        $questions = $row['question'];
                        $status = $row['status'];
                        
                        echo json_encode(array("category" => $category, "question" => $questions, "status" => $status));
                        
                    }
                    
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateQuestion($id, $category, $question, $status) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE questions SET category = :category, question = :question, status = :status WHERE id = :id");
                $stmt->bindparam(":category", $category);
                $stmt->bindparam(":question", $question);
                $stmt->bindparam(":status", $status);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function loadCourse($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM courses WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                <tr>
                    <td class="text-center">
                        <?php print($row['course_name']) ?>
                    </td>
                    <td class="text-center">
                        <?php print($row['course_description']) ?>
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateCourse(<?php print($row['id']) ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteCourse(<?php print($row['id']) ?>)">
                            <i class="fa fa-remove"></i>
                        </button>
                    </td>
                </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                <tr>
                    <td class="text-center" colspan="3">no data</td>
                </tr>
                    <?php
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addCourse($branch, $course_name, $course_acronym, $course_description) {
            
            try {
                
                $stmt = $this->db->prepare("INSERT INTO courses (branch, course_name, course_acronym, course_description) VALUES (:branch, :course_name, :course_acronym, :course_description)");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":course_name", $course_name);
                $stmt->bindparam(":course_acronym", $course_acronym);
                $stmt->bindparam(":course_description", $course_description);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deleteCourse($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM courses WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateCourse($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM courses WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $branch = $row['branch'];
                        $course_name = $row['course_name'];
                        $course_acronym = $row['course_acronym'];
                        $course_description = $row['course_description'];
                        
                        echo json_encode(array("branch" => $branch, "course_name" => $course_name, "course_acronym" => $course_acronym, "course_description" => $course_description));
                        
                    }
                    
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateCourse($id, $branch, $course_name, $course_acronym, $course_description) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE courses SET branch = :branch, course_name = :course_name, course_acronym = :course_acronym, course_description = :course_description WHERE id = :id");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":course_name", $course_name);
                $stmt->bindparam(":course_acronym", $course_acronym);
                $stmt->bindparam(":course_description", $course_description);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deleteSubject($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM subjects WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showSemANDSY() {
            
            try {
                
                $sem_and_sy = $this->db->prepare("SELECT * FROM sem_sy_settings");
                $sem_and_sy->execute();
                                    
                while ($row = $sem_and_sy->fetch(PDO::FETCH_ASSOC)) {
                                       
                    ?>
                    <tr>
                        <td class="text-center"><?php print($row['semester']) ?></td>
                        <td class="text-center"><?php print($row['term']) ?></td>
                        <td class="text-center"><?php print($row['school_year']) ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateSemAndSY(<?php print($row['id']) ?>)">
                                <i class="fa fa-edit"></i>
                            </button>
                        </td>
                        <td class="text-center"><?php print($row['status']) ?></td>
                    </tr>
                    <?php
                                        
                }
                
            } catch (PDOException $ex) {

            }
            
        }
        
        public function showUpdateSemAndSY($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM sem_sy_settings WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $semester = $row['semester'];
                $term = $row['term'];
                $school_year = $row['school_year'];
                $status = $row['status'];
                
                echo json_encode(array("semester" => $semester, "term" => $term, "school_year" => $school_year, "status" => $status));
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateSemANDSY($semester, $term, $school_year, $status) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE sem_sy_settings SET semester = :semester, term = :term, school_year = :school_year, status = :status");
                $stmt->bindparam(":semester", $semester);
                $stmt->bindparam(":term", $term);
                $stmt->bindparam(":school_year", $school_year);
                $stmt->bindparam(":status", $status);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addEvaluatorType($evaluator, $percentage) {
            
            try {
                
                $stmt = $this->db->prepare("INSERT INTO evaluator_types (evaluator, percentage) VALUES (:evaluator, :percentage)");
                $stmt->bindparam(":evaluator", $evaluator);
                $stmt->bindparam(":percentage", $percentage);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showEvaluatorTypes() {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM evaluator_types");
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                    <tr>
                        <td class="text-center"><b><?php print($row['evaluator']) ?></b></td>
                        <td class="text-center"><?php print($row['percentage']) ?></td>
                        <td class="text-center"><?php print($row['total_evaluators']) ?></td>
                        <td class="text-center"><button type="button" class="btn btn-primary" style="margin: 2px;" onclick="editEvaluationType(<?php print($row['id']) ?>)"><i class="fa fa-edit"></i></button></td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td colspan="3" class="text-center">no data</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showupdateEvaluatorType($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM evaluator_types WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $evaluator = $row['evaluator'];
                        $percentage = $row['percentage'];
                        $total_evaluators = $row['total_evaluators'];
                        
                        echo json_encode(array("evaluator" => $evaluator, "percentage" => $percentage, "total_evaluators" => $total_evaluators));
                        
                        return true;
                        
                    }
                    
                } else {
                    
                    return false;
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateEvaluatorType($evaluator, $percentage, $id, $total_evaluators) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE evaluator_types SET evaluator = :evaluator, percentage = :percentage, total_evaluators = :total_evaluators WHERE id = :id");
                $stmt->bindparam(":evaluator", $evaluator);
                $stmt->bindparam(":percentage", $percentage);
                $stmt->bindparam(":total_evaluators", $total_evaluators);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function ShowCampusScheduleData() {
            
            try {
                
                /* Get the Active Semester and School Year */
                $today_Sem_SY = $this->db->prepare("SELECT * FROM sem_sy_settings WHERE status = 'Active'");
                $today_Sem_SY->execute();
        
                $today_Sem_SY_Data = $today_Sem_SY->fetch(PDO::FETCH_ASSOC);
        
                $today_sem = $today_Sem_SY_Data['semester'];
                $today_sem_term = $today_Sem_SY_Data['term'];
                $today_sy = $today_Sem_SY_Data['school_year'];
                
                $stmt = $this->db->prepare("SELECT * FROM campus_schedules");
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $branch = "";
                        
                        switch($row['branch']) {
                            case 'MBC':
                                $branch = 'Bongabong Campus';
                                break;
                            case 'MMC':
                                $branch = 'Main Campus';
                                break;
                            case 'MCC':
                                $branch = 'Calapan City Campus';
                                break;
                            default :
                                break;
                        }
                        
                        ?>
                    <tr>
                        <td><b><?php print($branch) ?></b></td>
                        <td><?php print($row['start_date']) ?></td>
                        <td><?php print($row['end_date']) ?></td>
                        <td><?php print($today_sem . " - " . $today_sem_term . ", " . $today_sy) ?></td>
                        <td><?php print($row['status']) ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="updateCampusSched(<?php print($row['id']) ?>)"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td colspan="6" class="text-center">no campus schedules yet</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateCampusSchedule($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM campus_schedules WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $branch = $row['branch'];
                        $start_date = $row['start_date'];
                        $end_date = $row['end_date'];
                        $status = $row['status'];
                        $id = $row['id'];
                        
                        echo json_encode(array("branch" => $branch, "start_date" => $start_date, "end_date" => $end_date, "status" => $status, "id" => $id));
                        
                        return false;
                        
                    }
                    
                } else {
                    return false;
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function UpdateCampusSched($branch, $start_date, $end_date, $status, $id) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE campus_schedules SET branch = :branch, start_date = :start_date, end_date = :end_date, status = :status WHERE id = :id");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(":start_date", $start_date);
                $stmt->bindparam(":end_date", $end_date);
                $stmt->bindparam(":status", $status);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showProfessorsByBranch_inSelect($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                    <option value="<?php print($row['f_id']) ?>"><?php print($row['firstname'] . " " . $row['lastname']) ?></option>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <option value="">no data</option>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addUpdateSelfSchedule($branch, $f_id, $start_date, $end_date) {
            
            try {
                
                $check_faculty = $this->db->prepare("SELECT * FROM self_schedules WHERE f_id = :f_id");
                $check_faculty->bindparam(':f_id', $f_id);
                $check_faculty->execute();
                
                if ($check_faculty->rowCount() != null) {
                    
                    $updateSelfSchedule = $this->db->prepare("UPDATE self_schedules SET branch = :branch, start_date = :start_date, end_date = :end_date WHERE f_id = :f_id");
                    $updateSelfSchedule->bindparam(":branch", $branch);
                    $updateSelfSchedule->bindparam(":start_date", $start_date);
                    $updateSelfSchedule->bindparam(":end_date", $end_date);
                    $updateSelfSchedule->bindparam(':f_id', $f_id);
                    $updateSelfSchedule->execute();
                    
                    return true;
                    
                } else {
                    
                    $addSelfSchedule = $this->db->prepare("INSERT INTO self_schedules (branch, f_id, start_date, end_date) VALUES (:branch, :f_id, :start_date, :end_date)");
                    $addSelfSchedule->bindparam(":branch", $branch);
                    $addSelfSchedule->bindparam(":f_id", $f_id);
                    $addSelfSchedule->bindparam(":start_date", $start_date);
                    $addSelfSchedule->bindparam(":end_date", $end_date);
                    $addSelfSchedule->execute();
                    
                    return true;
                    
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showSelfEvaluationData($branch) {
            
            try {
                
                /* Get the Active Semester and School Year */
                $today_Sem_SY = $this->db->prepare("SELECT * FROM sem_sy_settings WHERE status = 'Active'");
                $today_Sem_SY->execute();
        
                $today_Sem_SY_Data = $today_Sem_SY->fetch(PDO::FETCH_ASSOC);
        
                $today_sem = $today_Sem_SY_Data['semester'];
                $today_sem_term = $today_Sem_SY_Data['term'];
                $today_sy = $today_Sem_SY_Data['school_year'];
                
                $stmt = $this->db->prepare("SELECT * FROM self_schedules WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        $f_id = $row['f_id'];
                        
                        $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                        $get_prof->bindparam(':f_id', $f_id);
                        $get_prof->execute();
                        
                        $prof = $get_prof->fetch(PDO::FETCH_ASSOC);
                        
                        ?>
                    <tr>
                        <td><?php print($prof['firstname'] . " " . $prof['lastname']) ?></td>
                        <td><?php print($row['start_date']) ?></td>
                        <td><?php print($row['end_date']) ?></td>
                        <td><?php print($today_sem . " - " . $today_sy) ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-primary" onclick="updateSelfSchedule(<?php echo $row['id'] ?>, '<?php echo $row['branch'] ?>')" style="margin: 2px;">
                                <i class="fa fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger" onclick="deleteSelfSchedule(<?php echo $row['id'] ?>, '<?php echo $row['branch'] ?>')" style="margin: 2px;">
                                <i class="fa fa-remove"></i>
                            </button>
                        </td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">no data</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showStudentProfessors($branch, $course, $curriculum_year) {
            
            global $base_url;
            
            try {
                
                $my_id = $_SESSION['user_id'];
        
                /* Getting the user account information */
                $me_account = $this->db->prepare("SELECT * FROM users WHERE user_id = :user_id");
                $me_account->bindparam(':user_id', $my_id);
                $me_account->execute();
        
                $my_account_data = $me_account->fetch(PDO::FETCH_ASSOC);
        
                $my_userCode = $my_account_data['user_code'];
                $my_role = $my_account_data['role'];
        
                /* Getting the user student information */
                $me_student = $this->db->prepare("SELECT * FROM students WHERE school_id = :school_id");
                $me_student->bindparam(":school_id", $my_userCode);
                $me_student->execute();
        
                if ($me_student->rowCount() != null) {
            
                    $my_student_data = $me_student->fetch(PDO::FETCH_ASSOC);        
                    $my_name = $my_student_data['firstname'] . " " . $my_student_data['lastname'];
                    $my_school_id = $my_student_data['school_id'];
                    $my_branch = $my_student_data['branch'];
                    $my_course = $my_student_data['course'];
                    $my_branch = $my_student_data['branch'];
            
                    $my_curr_year = $my_student_data['curriculum_year'];
            
                }
                
                /* Campus Schedules */
        
                /* During Schedule */
                $now_campus_schedule = $this->db->prepare("SELECT * FROM campus_schedules WHERE "
                . "branch = :branch AND "
                . "start_date <= NOW() AND "
                . "end_date >= NOW() AND "
                . "status = 'Active'");
                $now_campus_schedule->bindparam(":branch", $my_branch);
                $now_campus_schedule->execute();
                
                $subjects = $this->db->prepare("SELECT * FROM subjects WHERE branch = :branch AND course = :course AND curriculum_year = :curriculum_year GROUP BY f_id");
                $subjects->bindparam(":branch", $branch);
                $subjects->bindparam(":course", $course);
                $subjects->bindparam(":curriculum_year", $curriculum_year);
                $subjects->execute();
                
                if ($subjects->rowCount() != null) {
                    
                    while ($row = $subjects->fetch(PDO::FETCH_ASSOC)) {
                        
                        $professor = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                        $professor->bindparam(':f_id', $row['f_id']);
                        $professor->execute();
                        
                        $prof = $professor->fetch(PDO::FETCH_ASSOC);
                        
                        ?>
                    <tr>
                        <td>
                            <?php
                            
                            $prof_subjects = $this->db->prepare("SELECT * FROM subjects WHERE f_id = :f_id AND course = :course AND branch = :branch AND curriculum_year = :curriculum_year");
                            $prof_subjects->bindparam(":course", $course);
                            $prof_subjects->bindparam(":branch", $branch);
                            $prof_subjects->bindparam(":curriculum_year", $curriculum_year);
                            $prof_subjects->bindparam(':f_id', $prof['f_id']);
                            $prof_subjects->execute();
                            
                            if ($prof_subjects->rowCount() != null) {
                                
                                while ($profSubjs = $prof_subjects->fetch(PDO::FETCH_ASSOC)) {
                              
                                    print($profSubjs['subject_description']) . ", ";
                                    
                                }
                                
                            }
                            
                            ?>
                        </td>
                        <td><b><?php print($prof['firstname'] . " " . $prof['lastname']) ?></b></td>
                        <td class="text-center">
                            <?php
                            
                                $my_id = $_SESSION['user_id'];
        
                                /* Getting the user account information */
                                $me_account = $this->db->prepare("SELECT * FROM users WHERE user_id = :user_id");
                                $me_account->bindparam(':user_id', $my_id);
                                $me_account->execute();
        
                                $my_account_data = $me_account->fetch(PDO::FETCH_ASSOC);
        
                                $my_userCode = $my_account_data['user_code'];
                                $my_role = $my_account_data['role'];
        
                                /* Getting the user student information */
                                $me_student = $this->db->prepare("SELECT * FROM students WHERE school_id = :school_id");
                                $me_student->bindparam(":school_id", $my_userCode);
                                $me_student->execute();
        
                                if ($me_student->rowCount() != null) {
            
                                $my_student_data = $me_student->fetch(PDO::FETCH_ASSOC);        
                                $my_name = $my_student_data['firstname'] . " " . $my_student_data['lastname'];
                                $my_school_id = $my_student_data['school_id'];
                                $my_branch = $my_student_data['branch'];
                                $my_course = $my_student_data['course'];
                                $my_branch = $my_student_data['branch'];
            
                                $my_curr_year = $my_student_data['curriculum_year'];
            
                                $my_curriculum_year = "";
            
                                switch ($my_curr_year) {
                                    case 1:
                                        $my_curriculum_year = "1st Year";
                                        break;
                                    case 2:
                                        $my_curriculum_year = "2nd Year";
                                    break;
                                    case 3:
                                        $my_curriculum_year = "3rd Year";
                                        break;
                                    case 4:
                                        $my_curriculum_year = "4th Year";
                                        break;
                                    case 5:
                                        $my_curriculum_year = "5th Year";
                                        break;
                                    default:
                                        # codes here
                                        break;
                                }
            
                            } else {
            
                                $me_faculty = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
                                $me_faculty->bindparam(":f_id", $my_account_data['std_id']);
                                $me_faculty->execute();
            
                                $me_faculty_data = $me_faculty->fetch(PDO::FETCH_ASSOC);
            
                                $my_name = $me_faculty_data['firstname'] . " " . $me_faculty_data['lastname'];
                                $my_branch = $me_faculty_data['branch'];
                                $my_fac_id = $me_faculty_data['f_id'];
                                $my_fac_userCode = $me_faculty_data['f_code'];
            
                            }
        
                            /* Get the Active Semester and School Year */
                            $today_Sem_SY = $this->db->prepare("SELECT * FROM sem_sy_settings WHERE status = 'Active'");
                            $today_Sem_SY->execute();
        
                            $today_Sem_SY_Data = $today_Sem_SY->fetch(PDO::FETCH_ASSOC);
        
                            $today_sem = $today_Sem_SY_Data['semester'];
                            $today_sem_term = $today_Sem_SY_Data['term'];
                            $today_sy = $today_Sem_SY_Data['school_year'];
                            
                            $evaluation_type = 'student';
                            
                                $checkIfEvaluated = $this->db->prepare("SELECT * FROM evaluation_results WHERE branch = :branch AND "
                                        . "user_code = :user_code AND "
                                        . "f_id = :f_id "
                                        . "AND semester = :semester AND "
                                        . "semestral_term = :semestral_term AND "
                                        . "school_year = :school_year AND "
                                        . "evaluation_type = :evaluation_type");
                                $checkIfEvaluated->bindparam(":branch", $my_branch);
                                $checkIfEvaluated->bindparam(":user_code", $my_userCode);
                                $checkIfEvaluated->bindparam('f_id', $prof['f_id']);
                                $checkIfEvaluated->bindparam(":semester", $today_sem);
                                $checkIfEvaluated->bindparam(":semestral_term", $today_sem_term);
                                $checkIfEvaluated->bindparam(":school_year", $today_sy);
                                $checkIfEvaluated->bindparam(":evaluation_type", $evaluation_type);
                                $checkIfEvaluated->execute();
                                
                                if ($checkIfEvaluated->rowCount() == null && $now_campus_schedule->rowCount() == 1) {
                                    
                                    ?>
                            <button type="button" onclick="evaluateProfessor(<?php print($prof['f_id']) ?>)" class="btn-evaluate"><i class="fa fa-pencil-square-o"></i> Evaluate</button>
                                    <?php
                                    
                                } else if ($checkIfEvaluated->rowCount() != null && $now_campus_schedule->rowCount() == 1) {
                                    
                                    ?>
                            <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-pencil-square-o"></i> Evaluated</button>
                                    <?php
                                    
                                } else if ($now_campus_schedule->rowCount() != 1) {
                                    
                                    ?>
                            <button type="button" class="btn-evaluate-done" disabled="true"><i class="fa fa-calendar-times-o"></i> no schedule</button>
                                    <?php
                                    
                                }
                            ?>
                        </td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td class="text-center"colspan="4">no subjects yet</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function getTheFacultyName($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $fullname = $row['firstname'] . " " . $row['lastname'];
                $f_id = $row['f_id'];
                
                echo json_encode(array("fullname" => $fullname, "f_id" => $f_id));
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function evaluateFaculty($n_rates, $rating, $cat, $qid, $evaluation_type, $branch, $user_code, $f_id, $semester, $semestral_term, $school_year, $comment) {
            
            try {
                
                for ($i = 0; $i <= ($n_rates - 1); $i++) {
                    
                    $stmt = $this->db->prepare("INSERT INTO evaluation_results "
                    . "(evaluation_type, branch, user_code, f_id, category, question_id, rating, comment, semester, semestral_term, school_year) VALUES "
                    . "(:evaluation_type, :branch, :user_code, :f_id, :category, :question_id, :rating, :comment, :semester, :semestral_term, :school_year)");
                    $stmt->bindparam(":evaluation_type", $evaluation_type);
                    $stmt->bindparam(":branch", $branch);
                    $stmt->bindparam(":user_code", $user_code);
                    $stmt->bindparam(':f_id', $f_id);
                    $stmt->bindparam(":category", $cat[$i]);
                    $stmt->bindparam(":question_id", $qid[$i]);
                    $stmt->bindparam(':rating', $rating[$i]);
                    $stmt->bindparam(":comment", $comment);
                    $stmt->bindparam(":semester", $semester);
                    $stmt->bindparam(":semestral_term", $semestral_term);
                    $stmt->bindparam(":school_year", $school_year);
                    $stmt->execute();
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showFacultiesPerBranch_inSelect($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        ?>
                    <option value="<?php print($row['f_id']) ?>"><?php echo $row['firstname'] . " " . $row['lastname'] ?></option>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <option value="">no prof. yet</option>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function ShowPeerEvaluationData($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM peer_schedules WHERE branch = :branch GROUP BY f_id");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        /* Get the Professor Info */
                        $get_prof = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :id");
                        $get_prof->bindparam(':id', $row['f_id']);
                        $get_prof->execute();
                        
                        $profData = $get_prof->fetch(PDO::FETCH_ASSOC);
                        
                        ?>
                    <tr>
                        <td class="text-center"><b><?php echo $profData['firstname'] . " " . $profData['lastname'] ?></b></td>
                        <td>
                            <?php
                                $get_evaluators = $this->db->prepare("SELECT * FROM peer_schedules WHERE f_id = :id");
                                $get_evaluators->bindparam(':id', $row['f_id']);
                                $get_evaluators->execute();
                                                                
                                for ($i = 1; $i <= $get_evaluators->rowCount(); $i++) {
                                    
                                    $evaluators = $get_evaluators->fetch(PDO::FETCH_ASSOC);
                                        
                                    /* Get the Evaluators Info */
                                    $get_evaluators_info = $this->db->prepare("SELECT * FROM faculties WHERE f_code = :user_code");
                                    $get_evaluators_info->bindparam(":user_code", $evaluators['user_code']);
                                    $get_evaluators_info->execute();
                                    $evaluators_info = $get_evaluators_info->fetch(PDO::FETCH_ASSOC);
                                 
                                    print("<b>" . $i . "</b> " . $evaluators_info['firstname'] . " " . $evaluators_info['lastname'] .  "<br>");
                                }
                            ?>
                        </td>
                        <td><?php echo $row['start_date'] ?></td>
                        <td><?php echo $row['end_date'] ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-primary" style="margin: 2px;" onclick="UpdatePeerSchedule(<?php echo $row['f_id'] ?>, '<?php echo $row['branch'] ?>')"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" style="margin: 2px;" onclick="deleteSchedule(<?php echo $row['f_id'] ?>, '<?php echo $row['branch'] ?>')"><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">no data</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function PickRandomEvaluators($branch) {
            
            try {
                
                $range = $this->db->prepare("SELECT MAX(f_id) AS max_id, MIN(f_id) AS min_id FROM faculties WHERE branch = :branch");
                $range->bindparam(":branch", $branch);
                $range->execute();
                
                $range_row = $range->fetch(PDO::FETCH_ASSOC);
                $random = mt_rand($range_row['min_id'], $range_row['max_id']);
                                    
                $result = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch AND f_id >= $random LIMIT 0, 5");
                $result->bindparam(":branch", $branch);
                $result->execute();
                
                echo '<div style="margin: 15px;"></div>';
                
                for ($i = 1; $i <= $result->rowCount(); $i++) {
                    $row = $result->fetch(PDO::FETCH_ASSOC);
                    ?>
                    <input type="hidden" name="user_code[]" value="<?php echo $row['f_code'] ?>" />
                    <?php
                    
                    echo "<b>" . $i . ".</b> " . $row['firstname'] . " " . $row['lastname'] . ", <b>" . $row['f_code'] . "</b>" . "<br />";
                    
                }
                
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function AddUpdatePeerSchedule($branch, $f_id, $totalNo_user_codes, $user_code, $start_date, $end_date, $semester, $semestral_term, $school_year) {
            
            try {
                
                for ($i=0; $i<=($totalNo_user_codes-1); $i++) {
                
                $stmt = $this->db->prepare("INSERT INTO peer_schedules (branch, f_id, user_code, start_date, end_date, semester, semestral_term, school_year) VALUES "
                    . "(:branch, :f_id, :user_code, :start_date, :end_date, :semester, :semestral_term, :school_year)");
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam('f_id', $f_id);
                $stmt->bindparam(":user_code", $user_code[$i]);
                $stmt->bindparam(":start_date", $start_date);
                $stmt->bindparam(":end_date", $end_date);
                $stmt->bindparam(":semester", $semester);
                $stmt->bindparam(":semestral_term", $semestral_term);
                $stmt->bindparam("school_year", $school_year);
                $stmt->execute();
            
                continue;
                        
            }
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateStudent($branch, $firstname, $middlename, $lastname, $school_id, $curriculum_year, $course, $email, $mobile_no, $id) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE students SET "
                        . "school_id = :school_id, "
                        . "firstname = :firstname, "
                        . "middlename = :middlename, "
                        . "lastname = :lastname, "
                        . "course = :course, "
                        . "curriculum_year = :curriculum_year, "
                        . "email = :email, "
                        . "mobile = :mobile, "
                        . "branch = :branch WHERE "
                        . "id = :id");
                $stmt->bindparam(":school_id", $school_id);
                $stmt->bindparam(":firstname", $firstname);
                $stmt->bindparam(":middlename", $middlename);
                $stmt->bindparam(":lastname", $lastname);
                $stmt->bindparam(":course", $course);
                $stmt->bindparam(":curriculum_year", $curriculum_year);
                $stmt->bindparam(":email", $email);
                $stmt->bindparam(":mobile", $mobile_no);
                $stmt->bindparam(":branch", $branch);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deletePeerSchedules($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM peer_schedules WHERE f_id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdatePeerSchedule($branch, $id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM peer_schedules  WHERE f_id = :id AND branch = :branch");
                $stmt->bindparam(':id', $id);
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $f_id = $row['f_id'];
                $user_code = $row['user_code'];
                $start_date = $row['start_date'];
                $end_date = $row['end_date'];
                $semester = $row['semester'];
                $semestral_term = $row['semestral_term'];
                $school_year = $row['school_year'];
                
                echo json_encode(array("branch" => $branch, "f_id" => $f_id, "user_code" => $user_code, "start_date" => $start_date, "end_date" => $end_date, "semester" => $semester, "semestral_term" => $semestral_term, "school_year" => $school_year));
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateEvaluatorsOfProfessor($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM peer_schedules WHERE f_id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                for ($i = 1; $i <= $stmt->rowCount(); $i++) {
                    
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    
                    /* Get the Professor Information */
                    $get_faculty = $this->db->prepare("SELECT * FROM faculties WHERE f_code = :f_code ");
                    $get_faculty->bindparam("f_code", $row['user_code']);
                    $get_faculty->execute();
                    
                    $faculty = $get_faculty->fetch(PDO::FETCH_ASSOC);
                    
                    ?>
                    <b><?php echo $i ?></b> <?php echo $faculty['firstname'] . " " . $faculty['lastname'] ?>, <?php echo $row['user_code'] ?><br/>
                    <?php
                    
                }
                                    
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function UpdateSelfEvaluationByCampus($branch, $start_date, $end_date) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE self_schedules SET "
                        . "start_date = :start_date, "
                        . "end_date = :end_date WHERE "
                        . "branch = :branch");
                $stmt->bindparam(":start_date", $start_date);
                $stmt->bindparam(":end_date", $end_date);
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function deleteSelfSchedule($id) {
            
            try {
                
                $stmt = $this->db->prepare("DELETE FROM self_schedules WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateSelfSchedule($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM self_schedules WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $branch = $row['branch'];
                $f_id = $row['f_id'];
                $start_date = $row['start_date'];
                $end_date = $row['end_date'];
                
                echo json_encode(array("branch" => $branch, "f_id" => $f_id, "start_date" => $start_date, "end_date" => $end_date));
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function UpdateSelfSchedule($id, $start_date, $end_date) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE self_schedules SET "
                        . "start_date = :start_date, "
                        . "end_date = :end_date WHERE "
                        . "id = :id");
                $stmt->bindparam(":start_date", $start_date);
                $stmt->bindparam(":end_date", $end_date);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function UpdatePeerSchedule($f_id, $start_date, $end_date, $today_sem, $today_sem_term, $today_sy) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE peer_schedules SET "
                        . "start_date = :start_date, "
                        . "end_date = :end_date WHERE "
                        . "f_id = :f_id AND "
                        . "semester = :semester AND "
                        . "semestral_term = :semestral_term AND "
                        . "school_year = :school_year");
                $stmt->bindparam(":start_date", $start_date);
                $stmt->bindparam(":end_date", $end_date);
                $stmt->bindparam(":f_id", $f_id);
                $stmt->bindparam("semester", $today_sem);
                $stmt->bindparam(":semestral_term", $today_sem_term);
                $stmt->bindparam(":school_year", $today_sy);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showEvaluationResults($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM overall_results WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        /* Get the faculty information */
                        $get_faculty = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :id");
                        $get_faculty->bindparam(':id', $row['f_id']);
                        $get_faculty->execute();
                        
                        $faculty = $get_faculty->fetch(PDO::FETCH_ASSOC);
                        
                        ?>
                    <tr>
                        <td class="text-center"><?php echo $faculty['firstname'] . " " . $faculty['lastname'] ?></td>
                        <td class="text-center"><?php echo $row['total_qce'] ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td class="text-center" colspan="3">no data yet</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (Exception $ex) {

            }
            
        }
        
        public function showProfessorsPerBranch($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    
                        ?>
                        <option value="<?php echo $row['f_id']  ?>"><?php echo $row['firstname'] . " " . $row['lastname'] ?></option>
                        <?php
                    
                    }
                    
                } else {
                    ?>
                    <option value="">-- no professors yet --</option>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateSubjects($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM subjects WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $course = $row['course'];
                $subject_code = $row['subject_code'];
                $subject_description = $row['subject_description'];
                $curriculum_year = $row['curriculum_year'];
                
                echo json_encode(array("subject_code" => $subject_code, "subject_description" => $subject_description, "course" => $course, "curriculum_year" => $curriculum_year));
                
                return false;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function updateSubject($subject_code, $subject_description, $f_id, $id) {
            
            try {
                
                $stmt = $this->db->prepare("UPDATE subjects SET "
                        . "subject_code = :subject_code, "
                        . "subject_description = :subject_description, "
                        . "f_id = :f_id WHERE "
                        . "id = :id");
                $stmt->bindparam(":subject_code", $subject_code);
                $stmt->bindparam(":subject_description", $subject_description);
                $stmt->bindparam(':f_id', $f_id);
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showFacultyEvaluations($branch) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE branch = :branch");
                $stmt->bindparam(":branch", $branch);
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    ?>
                    <table id="currentEvaluationTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Faculty</th>
                                <th class="text-center">Action</th>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    
                                    ?>
                            <tr>
                                <td class="text-center"><b><?php echo $row['firstname'].' '.$row['lastname'] ?></b></td>
                                <td class="text-center">
                                    <button type="button" onclick="showFacultyEvaluations(<?php echo $row['f_id'] ?>)" class="btn btn-default"><i class="fa fa-search"></i> View Evaluations</button>
                                </td>
                            </tr>
                                    <?php
                                    
                                }
                            ?>
                            
                        </tbody>
                    </table>
                    <?php
                    
                } else {
                
                    ?>
                    <div class="text-center">
                        <h4>no data</h4>
                    </div>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function getFacultyData($fid, $today_sem, $today_sem_term, $today_sy) {
            
            try {
                
                /* Get the student evaluators */
                $stds_Evaluated = $this->db->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
                        . "semester = :semester AND "
                        . "semestral_term = :semestral_term AND "
                        . "school_year = :school_year AND "
                        . "evaluation_type = 'student' "
                        . "GROUP BY user_code");
                $stds_Evaluated->bindparam(':f_id', $fid);
                $stds_Evaluated->bindparam(":semester", $today_sem);
                $stds_Evaluated->bindparam(":semestral_term", $today_sem_term);
                $stds_Evaluated->bindparam(":school_year", $today_sy);
                $stds_Evaluated->execute();
                
                /* Total Students Evaluated at this Season (Semester) */
                $stds_TotalEvaluated = $stds_Evaluated->rowCount();
                
                /* ############################################################################################### */
                
                
                /* Get the peer evaluators */
                $peers_Evaluated = $this->db->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
                        . "semester = :semester AND "
                        . "semestral_term = :semestral_term AND "
                        . "school_year = :school_year AND "
                        . "evaluation_type = 'peer' "
                        . "GROUP BY user_code");
                $peers_Evaluated->bindparam('f_id', $fid);
                $peers_Evaluated->bindparam(":semester", $today_sem);
                $peers_Evaluated->bindparam(":semestral_term", $today_sem_term);
                $peers_Evaluated->bindparam(":school_year", $today_sy);
                $peers_Evaluated->execute();
                
                /* Total Peers Evaluated at this Season (Semester) */
                $peers_totalEvaluated = $peers_Evaluated->rowCount();
                
                /* ############################################################################################### */
                
                $supervisor_Evaluated = $this->db->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
                        . "semester = :semester AND "
                        . "semestral_term = :semestral_term AND "
                        . "school_year = :school_year AND "
                        . "evaluation_type = 'supervisor' "
                        . "GROUP BY user_code");
                $supervisor_Evaluated->bindparam('f_id', $fid);
                $supervisor_Evaluated->bindparam(":semester", $today_sem);
                $supervisor_Evaluated->bindparam(":semestral_term", $today_sem_term);
                $supervisor_Evaluated->bindparam(":school_year", $today_sy);
                $supervisor_Evaluated->execute();
                
                /* Total Supervisor Evaluated at this Season (Semester) */
                $supervisor_totalEvaluated = $supervisor_Evaluated->rowCount();
                
                /* ############################################################################################### */

                /* Get the Total Students */
                
                $get_courses = $this->db->prepare("SELECT * FROM courses");
                $get_courses->execute();
    
                while ($course = $get_courses->fetch(PDO::FETCH_ASSOC)) {
        
                    $get_subjs = $this->db->prepare("SELECT * FROM subjects WHERE course = :course AND f_id = :f_id GROUP BY COURSE");
                    $get_subjs->bindparam(":course", $course["course_acronym"]);
                    $get_subjs->bindparam(':f_id', $fid);
                    $get_subjs->execute();
                
                    while ($subjects = $get_subjs->fetch(PDO::FETCH_ASSOC)) {
                    
                    $get_stds = $this->db->prepare("SELECT * FROM students WHERE course = :course");
                    $get_stds->bindparam(":course", $course['course_acronym']);

                    $get_stds->execute();
            
                        while ($stds = $get_stds->fetch(PDO::FETCH_ASSOC)) {

                            $students[$stds["firstname"].$stds["lastname"]] = '1';
                
                        }
            
                    }
        
                }
    
                $t_stds = isset($students) ? count($students) : '0';

                /* ############################################################################################### */

                
                $self_Evaluated = $this->db->prepare("SELECT * FROM evaluation_results WHERE f_id = :f_id AND "
                        . "semester = :semester AND "
                        . "semestral_term = :semestral_term AND "
                        . "school_year = :school_year AND "
                        . "evaluation_type = 'self' "
                        . "GROUP BY user_code");
                $self_Evaluated->bindparam('f_id', $fid);
                $self_Evaluated->bindparam(":semester", $today_sem);
                $self_Evaluated->bindparam(":semestral_term", $today_sem_term);
                $self_Evaluated->bindparam(":school_year", $today_sy);
                $self_Evaluated->execute();
                
                /* Total Self Evaluated at this Season (Semester) */
                $self_totalEvaluated = $self_Evaluated->rowCount();
                
                /* ############################################################################################### */
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :id");
                $stmt->bindparam(':id', $fid);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $fname = $row['firstname'].' '.$row['lastname'];
                
                echo json_encode(array("f_id", $fid, "fname" => $fname, "stds_Evaluated" => $stds_TotalEvaluated, "peers_Evaluated" => $peers_totalEvaluated, "supervisor_totalEvaluated" => $supervisor_totalEvaluated, "self_totalEvaluated" => $self_totalEvaluated, "total_stds" => $t_stds));
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function chk_curr_pw($pw_txt, $my_userCode) {
            
            try {
                
                $pw_txt = md5($pw_txt);
                
                $stmt = $this->db->prepare("SELECT * FROM users WHERE user_code = :user_code AND password = :password");
                $stmt->bindparam(":user_code", $my_userCode);
                $stmt->bindparam(":password", $pw_txt);
                $stmt->execute();
                
                if ($stmt->rowCount() == 1) {
                   
                    ?>
                    <i class="fa fa-check color-green"></i>
                    <?php
                    
                } else {
                    
                    ?>
                    <i class="fa fa-remove color-red"></i>
                    <?php
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function chk_new_pw($pw_txt) {
            
            try {
                
                if (strlen($pw_txt) >= 8) {
                    
                    ?>
                    <i class="fa fa-check color-green"></i>
                    <?php
                    
                } else if (strlen($pw_txt) < 8) {
                    
                    ?>
                    <font class="color-red">
                        (<i class="fa fa-remove"></i> Must be atleast 8 characters and above!)
                    </font>
                    <?php
                    
                }
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function viewFacultyInfo($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM faculties WHERE f_id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $fullname = $row['firstname'].' '.$row['lastname'];
                $mobile = $row['mobile'];
                $email = $row['email'];
                
                
                echo json_encode(array("fullname" => $fullname, "mobile" => $mobile, "email" => $email));
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showDesignationData(){
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM designations");
                $stmt->execute();
                
                if ($stmt->rowCount() != null) {
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        
                        /* Get the designation data */
                        $peer = $this->db->prepare("SELECT * FROM designations WHERE id = :id");
                        $peer->bindparam(':id', $row['peer']);
                        $peer->execute();
                        $peerData = $peer->fetch(PDO::FETCH_ASSOC);

                        ?>
                    <tr>
                        <td class="text-center"><?php echo $row['designation'] ?></td>
                        <td class="text-center"><?php echo $peerData['designation'] ?>
                        </td>
                        <td class="text-center"><?php echo $peerData['designation'] ?>
                        </td>
                        <td class="text-center">
                            <button type="button" onclick="updateDesignation(<?php echo $row['id'] ?>)" class="btn btn-default"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                        <?php
                        
                    }
                    
                } else {
                    
                    ?>
                    <tr>
                        <td class="text-center" colspan="4">no data</td>
                    </tr>
                    <?php
                    
                }
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function addDesignation($designation) {
            
            try {
                
                $stmt = $this->db->prepare("INSERT INTO designations (designation) VALUES (:designation)");
                $stmt->bindparam(":designation", $designation);
                $stmt->execute();
                
                return true;
                
            } catch (PDOException $ex) {
                
                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showUpdateDesignation($id) {
            
            try {
                
                $stmt = $this->db->prepare("SELECT * FROM designations WHERE id = :id");
                $stmt->bindparam(':id', $id);
                $stmt->execute();
                
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $designation = $row['designation'];
                $peer = $row['peer'];
                $supervisor = $row['supervisor'];
                
                echo json_encode(array("designation" => $designation, "peer" => $peer, "supervisor" => $supervisor));
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }
        
        public function showEvaluationsData($evaluation_type, $f_id) {
                        
            try {

                global $base_url;
                
                ?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">NO.</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Total Score</th>
                                <th class="text-center">Print Report</th>
                            </tr>
                            
                            <?php
                            
                            $evaluations = $this->db->prepare("SELECT * FROM evaluation_results WHERE evaluation_type = :evaluation_type AND "
                                    . "f_id = :f_id GROUP BY user_code");
                            $evaluations->bindparam(":evaluation_type", $evaluation_type);
                            $evaluations->bindparam(':f_id', $f_id);
                            $evaluations->execute();
                            
                            if ($evaluations->rowCount() != null) {
                                
                                for ($i = 1; $i <= $evaluations->rowCount(); $i++) {
                                    
                                    $row = $evaluations->fetch(PDO::FETCH_ASSOC);
                                    
                                    if ($evaluation_type == 'student') {
                                        
                                        $std_info = $this->db->prepare("SELECT * FROM students WHERE school_id = :school_id");
                                        $std_info->bindparam(":school_id", $row["user_code"]);
                                        $std_info->execute();
                                        
                                        $std_data = $std_info->fetch(PDO::FETCH_ASSOC);
                                        
                                        $name = $std_data["firstname"] . " " . $std_data["lastname"];
                                        
                                    } else if ($evaluation_type == "peer" || $evaluation_type == 'self' || $evaluation_type == 'supervisor') {
                                        
                                        $fac_info = $this->db->prepare("SELECT * FROM faculties WHERE f_code = :f_code");
                                        $fac_info->bindparam(":f_code", $row["user_code"]);
                                        $fac_info->execute();
                                        
                                        $fac_data = $fac_info->fetch(PDO::FETCH_ASSOC);
                                        
                                        $name = $fac_data["firstname"] . " " . $fac_data["lastname"];
                                        
                                    }
                                    
                                    /* Get the user total rating */
                                    
                                    /* Get the Total Ranking Per Category  */
                                    $categories = $this->db->prepare("SELECT * FROM evaluation_results WHERE user_code = :user_code AND "
                                            . "evaluation_type = :evaluation_type AND "
                                            . "f_id = :f_id GROUP BY category");
                                    $categories->bindparam("user_code", $row["user_code"]);
                                    $categories->bindparam(":evaluation_type", $evaluation_type);
                                    $categories->bindparam(':f_id', $f_id);
                                    $categories->execute();
                                    
                                    $total_rank = 0;
                                    
                                    while ($categ = $categories->fetch(PDO::FETCH_ASSOC)) {
                                        
                                        /* Get the category percentage */
                                        $get_categ = $this->db->prepare("SELECT * FROM categories WHERE category = :category");
                                        $get_categ->bindparam(":category", $categ["category"]);
                                        $get_categ->execute();
                                        
                                        $categData = $get_categ->fetch(PDO::FETCH_ASSOC);
                                        
                                        $user_code = $row["user_code"];
                                        
                                        /* Get all the questions per category */
                                        $questions = $this->db->prepare("SELECT * FROM evaluation_results WHERE user_code = :user_code AND "
                                                . "evaluation_type = :evaluation_type AND "
                                                . "category = :category AND f_id = :f_id");
                                        $questions->bindparam(":user_code", $user_code);
                                        $questions->bindparam(":evaluation_type", $evaluation_type);
                                        $questions->bindparam(":category", $categ["category"]);
                                        $questions->bindparam(':f_id', $f_id);
                                        $questions->execute();
                                        
                                        $rate_per_rank = 0;
                                        
                                        while ($quest = $questions->fetch(PDO::FETCH_ASSOC)) {
                                                 
                                            $rate_per_rank += $quest["rating"];
                                            
                                            /*echo $quest["rating"] . " ";*/
                                            
                                        }
                                        
                                        /*echo $categ["category"]." ";*/
                                        
                                        /*echo $rate_per_rank . " ";*/
                                        
                                        $rate_per_rank = $rate_per_rank * $categData["percentage"];
                                        
                                        /*echo $rate_per_rank . " <br/>";*/
                                        
                                        $total_rank += $rate_per_rank;
                                                                               
                                    } /*echo "<br/>" . $total_rank."<br/><br/>";*/
                                                                       
                                    ?>
                            <tr>
                                <td class="text-center"><b><?php echo $i ?></b></td>
                                <td class="text-center"><?php echo $name ?></td>
                                <td class="text-center"><b><?php echo $total_rank ?></b></td>
                                <td class="text-center">
                                    <a href="<?php echo $base_url ?>/application/dashboard/evaluation.php?" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                                </td>
                            </tr>
                                    <?php
                                    
                                }
                                
                            } else {
                                
                                ?>
                            <tr><td class="text-center" colspan="4"><b>no data</b></td></tr>
                                <?php
                                
                            }
                            
                            ?>
                            
                        </thead>
                    </table>
                <?php
                
                return true;
                
            } catch (PDOException $ex) {

                echo $ex->getMessage();
                return false;
                
            }
            
        }

        public function updateDesignation($designation, $peer, $supervisor, $id) {

            try {

                $stmt = $this->db->prepare("UPDATE designations SET designation = :designation, peer = :peer, supervisor = :supervisor WHERE id = :id");
                $stmt->bindparam(":designation", $designation);
                $stmt->bindparam(':peer', $peer);
                $stmt->bindparam(':supervisor', $supervisor);
                $stmt->bindparam(':id', $id);
                $stmt->execute();

                return true;

            } catch (PDOException $ex) {
                echo $ex;
                return false;
            }

        }
        
    }
?>