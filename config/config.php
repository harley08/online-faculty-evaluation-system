<?php
    session_start();
    
    date_default_timezone_set('Asia/Manila');
    
    $base_url = "http://localhost/rateme";
    $_SESSION['base_url'] = $base_url;
    
    $logged_in = isset($_SESSION['user_id']);
    $not_logged_in = !isset($_SESSION['user_id']);
    $admin_access = isset($_SESSION['Administrator']);
          
    include 'database.php';
    include '../../class/data.php';
    $data = new data($db_con);
    
    if ($logged_in) {        
        $my_id = $_SESSION['user_id'];
        
        /* Getting the user account information */
        $me_account = $db_con->prepare("SELECT * FROM users WHERE user_id = :user_id");
        $me_account->bindparam(':user_id', $my_id);
        $me_account->execute();
        
        $my_account_data = $me_account->fetch(PDO::FETCH_ASSOC);
        
        $my_userCode = $my_account_data['user_code'];
        $my_role = $my_account_data['role'];
        
        /* Getting the user student information */
        $me_student = $db_con->prepare("SELECT * FROM students WHERE school_id = :school_id");
        $me_student->bindparam(":school_id", $my_userCode);
        $me_student->execute();
        
        if ($me_student->rowCount() != null) {
            
            $my_student_data = $me_student->fetch(PDO::FETCH_ASSOC);        
            $my_name = $my_student_data['firstname'] . " " . $my_student_data['lastname'];
            $my_school_id = $my_student_data['school_id'];
            $my_branch = $my_student_data['branch'];
            $my_course = $my_student_data['course'];
            $my_branch = $my_student_data['branch'];
            
            $my_curr_year = $my_student_data['curriculum_year'];
            
            $my_curriculum_year = "";
            
            switch ($my_curr_year) {
                case 1:
                    $my_curriculum_year = "1st Year";
                    break;
                case 2:
                    $my_curriculum_year = "2nd Year";
                    break;
                case 3:
                    $my_curriculum_year = "3rd Year";
                    break;
                case 4:
                    $my_curriculum_year = "4th Year";
                    break;
                case 5:
                    $my_curriculum_year = "5th Year";
                    break;
                default:
                    # codes here
                    break;
            }
            
        } else {
            
            $me_faculty = $db_con->prepare("SELECT * FROM faculties WHERE f_id = :f_id");
            $me_faculty->bindparam(":f_id", $my_account_data['std_id']);
            $me_faculty->execute();
            
            $me_faculty_data = $me_faculty->fetch(PDO::FETCH_ASSOC);
            
            $my_name = $me_faculty_data['firstname'] . " " . $me_faculty_data['lastname'];
            $my_branch = $me_faculty_data['branch'];
            $my_fac_id = $me_faculty_data['f_id'];
            $my_fac_userCode = $me_faculty_data['f_code'];
            
        }
        
        /* Get the Active Semester and School Year */
        $today_Sem_SY = $db_con->prepare("SELECT * FROM sem_sy_settings WHERE status = 'Active'");
        $today_Sem_SY->execute();
        
        $today_Sem_SY_Data = $today_Sem_SY->fetch(PDO::FETCH_ASSOC);
        
        $today_sem = $today_Sem_SY_Data['semester'];
        $today_sem_term = $today_Sem_SY_Data['term'];
        $today_sy = $today_Sem_SY_Data['school_year'];
     
        
        /* Campus Schedules */
        
        /* During Schedule */
        $now_campus_schedule = $db_con->prepare("SELECT * FROM campus_schedules WHERE "
                . "branch = :branch AND "
                . "start_date <= NOW() AND "
                . "end_date >= NOW() AND "
                . "status = 'Active'");
        $now_campus_schedule->bindparam(":branch", $my_branch);
        $now_campus_schedule->execute();
        
        /* Self Schedule */
        $fac_self_schedule = $db_con->prepare("SELECT * FROM self_schedules WHERE "
                . "branch = :branch AND "
                . "f_id = :f_id AND "
                . "start_date <= NOW() AND "
                . "end_date >= NOW()");
        $fac_self_schedule->bindparam(":branch", $my_branch);
        $fac_self_schedule->bindparam('f_id', $my_fac_id);
        $fac_self_schedule->execute();
    }
?>