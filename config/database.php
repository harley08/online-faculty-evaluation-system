<?php
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pass = '';
    $db_name = 'rateme_db';
    
    try {
        $db_con = new PDO("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_pass);
        $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $PDOex) {
        echo $PDOex->getMessage();
    }
?>