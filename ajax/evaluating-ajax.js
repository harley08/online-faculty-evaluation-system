$(document).ready(function(){
    
    $('#instructionModal').modal('show');
    
});

$('#evaluateForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function()
        {
            $('#successEvaluateModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to evaluate professor!');
            $('#loading').hide();
        }
        
    });
    
});