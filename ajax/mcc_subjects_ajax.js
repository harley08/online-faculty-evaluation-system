$(document).ready(function(){
    
    /* Load subjects in MMC by Course */
    $('#mcc_courses').change(function(){
        
        var mcc_courses = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {course: mcc_courses, branch: 'MCC', action: 'loadSubjects'},
            datatype: 'html',
            success: function(result)
            {
                $('#mccSubjectsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load subjects!');
                $('#loading').hide();
            }
            
        });
        
    });
    
});