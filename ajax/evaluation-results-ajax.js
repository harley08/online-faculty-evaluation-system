$(document).ready(function(){
    
    $('#branch').change(function(){
        
        var branch = $(this).val();

        $('#loading').show();

        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showEvaluationResults', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#evaluatioResultsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show evaluation results');
                $('#loading').hide();
            }
            
        });
        
    });
    
});