$(document).ready(function(){
    
    $('#select_branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showSelfEvaluationData', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#selfEvaluationData').html(result);
                $('#selfEvaluationTable').DataTable();
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show self evaluation data!');
                $('#loading').hide();
            }
            
        });
        
    });
    
    $('#branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showProfessorsByBranch_inSelect', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#professors').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load professors in [select]!');
                $('#loading').hide();
            }
            
        });
        
    });
    
});

$('#addSelfScheduleForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $('#selfEvaluationData').html(result);
            $('#AddSelfScheduleModal').modal('hide');
            $('#text_content').text('Self Evaluation Added Successfully!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to Add Self Schedule!');
            $('#loading').hide();
        }
        
    });
    
});

$('#setSelfEvaluationByCampusForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $('#selfEvaluationData').html(result);
            $('#SetSelfEvaluationByCampusModal').modal('hide');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to set by campus self schedule!');
            $('#loading').hide();
        }
        
    });
    
});

function deleteSelfSchedule(id, branch) {
    
    $('#deleteSelfSchedModal').modal('show');
    
    $('.yes').click(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'deleteSelfSchedule', id: id, branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#selfEvaluationData').html(result);
                $('#deleteSelfSchedModal').modal('hide');
                $('#text_content').text('Self Schedule Successfully Deleted!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to delete self schedule!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}

function updateSelfSchedule(id, branch) {
    
    $('#updateSelfScheduleModal').modal('show');
    
    $.ajax({
            
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showProfessorsByBranch_inSelect', branch: branch},
        dataType: 'html',
        success: function(result)
        {
            $('#u_professors').html(result);
        },
        error: function()
        {
            alert('Failed to load professors in [select]!');
        }
        
    });
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateSelfSchedule', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#u_branch').val(result.branch);
            $('#a_u_branch').val(result.branch);
            $('#u_start_date').val(result.start_date);
            $('#u_end_date').val(result.end_date);
            $('#u_id').val(id);
        },
        error: function()
        {
            alert('Failed to load self schedule data!');
        }
        
    });
    
    $('#updateSelfScheduleForm').submit(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $('#selfEvaluationData').html(result);
                $('#updateSelfScheduleModal').modal('hide');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to update self schedule!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}