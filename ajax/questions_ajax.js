function showQuestionData()
    {
        $('#loading').show();
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showQuestionsData'},
            dataType: 'html',
            success: function(result)
            {
                $('#questionsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load questions!');
                $('#loading').hide();
            }
        });
    }
    
$(document).ready(function(){
    showQuestionData();
});

/* Add Question */
$('#addQuestionForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $('#questionsData').html(result);
            $('#addQuestionModal').modal('hide');
            $('#text_content').text('Question successfully added!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to add question!');
            $('#loading').hide();
        }
    });
    
});

/* Delete Question */
function deleteQuestion(id)
{
    $('#deleteQuestionModal').modal('show');
    
    $('.yes').click(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {action: 'deleteQuestion', id: id},
            dataType: 'html',
            success: function(result)
            {
                $('#questionsData').html(result);
                $('#deleteQuestionModal').modal('hide');
                $('#text_content').text('Question successfully deleted!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to delete question!');
                $('#loading').hide();
            }
        });
        
    });
}

/* Update Question */
function updateQuestion(id)
{
    
    $('#updateQuestionModal').modal('show');
    
    $('#loading').show();
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateQuestion', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#u_category').val(result.category);
            $('#u_question').val(result.question);
            $('#u_status').val(result.status);
            $('#u_id').val(id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load question data!');
            $('#loading').hide();
        }
    });
    
    $('#updateQuestionForm').submit(function(e) {
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $('#questionsData').html(result);
                $('#updateQuestionModal').modal('hide');
                $('#text_content').text('Question successfully updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                
                alert('Failed to update question');
                $('#loading').hide();
                
            }
            
        });
        
    });
    
}