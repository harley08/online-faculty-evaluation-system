$(document).ready(function(){
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showStudentProfessors'},
        dataType: 'html',
        success: function(result)
        {
            $('#showStudentProfessors').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load student subjects!');
            $('#loading').hide();
        }
        
    });
    
});

function evaluateProfessor(id) {
    
    $('#continueModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'getTheFacultyName', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#ProfessorName').text(result.fullname);
            $('#f_id').val(result.f_id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to get the faculty information!');
            $('#loading').hide();
        }
        
    });
    
}

function printDiv(divName) {
    
    var newWindow = window.open();
    var doc  = newWindow.document;
    doc.write("<html><head>" +
    "<title>Print Certificate</title>" + 
    "<link rel=\'stylesheet\' type=\'text/css\' href=\'../../assets/css/bootstrap.min.css\' >" + "<style type=\'text/css\'>" + 
    "@media print {table td:last-child {display:none}table th:last-child {display:none}	html, body { display: block; }	}"+
    "</style>"+
    "</head><body>" +document.getElementById(divName).innerHTML  + "</body></html>");
    setTimeout(function(){newWindow.print(); newWindow.close(); }, 100);

    
}