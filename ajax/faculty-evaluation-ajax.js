function evaluateProfessor(id) {
    
    $('#continueModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'getTheFacultyName', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#ProfessorName').text(result.fullname);
            $('#f_id').val(result.f_id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to get the faculty information!');
            $('#loading').hide();
        }
        
    });
    
}

function evaluateSelf(id) {
    
    $('#continueSelfEvaluateModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'getTheFacultyName', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#self_ProfessorName').text(result.fullname);
            $('#self_f_id').val(id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to get the faculty information!');
            $('#loading').hide();
        }
        
    });
    
}