$(document).ready(function(){
    
    $('#branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showFacultyEvaluations', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#currentEvaluationData').html(result);
                $('#currentEvaluationTable').DataTable();
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show faculty evaluations!');
                $('#loading').hide();
            }
            
        });
        
    });
    
});

function showFacultyEvaluations(id) {
    
    $('#facultyEvaluationsModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'getFacultyData', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#ProfessorName').text(result.fname);
            $('#totalStds').text(result.total_stds);
            $('#stdsEvaluated').text(result.stds_Evaluated);
            $('#peersEvaluated').text(result.peers_Evaluated);
            $('#supervisorEvaluated').text(result.supervisor_totalEvaluated);
            $('#selfEvaluated').text(result.self_totalEvaluated);
            $('#f_id').val(id);
            $('#peer_f_id').val(id);
            $('#supervisor_f_id').val(id);
            $('#self_f_id').val(id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to get faculty data!');
            $('#loading').hide();
        }
        
    });
    
}