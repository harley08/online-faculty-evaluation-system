/* Load the Categories */
function showCategories() {
    
    $('#loading').show();
    
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showCategories'},
        dataType: 'html',
        success: function(result)
        {
            $('#categoryData').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load categories!');
            $('#loading').hide();
        }
    });
}       

$(document).ready(function(){
    showCategories();
    
});

$('#addCategoryForm').submit(function(e){
                
    e.preventDefault();
    
        $('#loading').show();
    
        $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            
            $('#categoryData').html(result);
            $('#addCategoryModal').modal('hide');
            $('#text_content').text('Category Successfully Added!');
            $('#successModal').modal('show');
            $('#loading').hide();
            
        showCategories();
        
        },
        error: function()
       {
           
            alert('Failed to add new category!');
            $('#loading').hide();
            
        }
    });
});

function deleteCategory(id) {

    $('#deleteCategoryModal').modal('show');
                
    $('.yes').click(function(e){
        
        $('#loading').show();
        
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {action: 'deleteCategory', id: id},
            dataType: 'html',
            success: function(result)
            {
         
                $('#categoryData').html(result);
                $('#deleteCategoryModal').modal('hide');
                $('#text_content').text('Category successfully deleted!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
            
                alert('Unable to remove category!');
                $('#loading').hide();
            }
        });
    });             
}

function updateCategory(id)
{

    $('#updateCategoryModal').modal('show');
                
    $('#loading').show();
    
    $.ajax({
    
        url: 'actions.php',
        type: 'POST',
        data: {id: id, action: 'showUpdateCategory'},
        dataType: 'json',
        success: function(result)
        {
        
            $('#u_category_name').val(result.category_name);
            $('#u_status').val(result.status);
            $('#u_percentage').val(result.percentage);
            $('#u_id').val(id);
            $('#loading').hide();
            
        },
        error: function(xhr, ajaxOptions, thrownError)
        {
        
            alert('Unable to load category data on form! ' + thrownError);
            $('#loading').hide();
        
        }
    });
    
    $('#updateCategoryForm').submit(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $('#categoryData').html(result);
                $('#updateCategoryModal').modal('hide');
                $('#text_content').text('Category successfully updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to update category!');
                $('#loading').hide();
            }
            
        });
        
    });
}