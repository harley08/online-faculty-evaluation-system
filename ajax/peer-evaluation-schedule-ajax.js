$(document).ready(function(){
    
    $('#branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showFacultiesPerBranch_inSelect', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#f_id').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show faculties per branch!');
                $('#loading').hide();
            }
            
        });
        
    });
    
    $('#select_branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'ShowPeerEvaluationData', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#PeerEvaluationData').html(result);
                $('#PeerEvaluationTable').DataTable();
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show Peer Evaluation Schedules!');
                $('#loading').hide();
            }
                        
        });
        
    });
    
});

function pickEvaluators() {
    
    var branch = $('#branch').val();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'PickRandomEvaluators', branch: branch},
        dataType: 'html',
        success: function(result)
        {
            $('#evaluators').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to pick five(5) random peer evaluators!');
            $('#loading').hide();
        }
        
    });
    
}

$('#AddUpdatePeerScheduleForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $('#addUpdatePeerScheduleModal').modal('hide');
            $('#PeerEvaluationData').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to Add/Update Peer Schedules!');
            $('#loading').hide();
        }
        
    });
    
});

function deleteSchedule(id, branch) {
    
    $('#deleteScheduleModal').modal('show');
    
    $('.yes').click(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'deletePeerSchedules', branch: branch, id: id},
            dataType: 'html',
            success: function(result)
            {
                $('#PeerEvaluationData').html(result);
                $('#deleteScheduleModal').modal('hide');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to delete peer evaluation schedule!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}

function UpdatePeerSchedule(id, branch) {
    
    $('#UpdatePeerScheduleModal').modal('show');
    
    $.ajax({
            
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showFacultiesPerBranch_inSelect', branch: branch},
        dataType: 'html',
        success: function(result)
        {
            $('#u_f_id').html(result);
        },
        error: function()
        {
            alert('Failed to show faculties per branch!');
        }
            
    });
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateEvaluatorsOfProfessor', id: id},
        dataType: 'html',
        success: function(result)
        {
            $('#u_evaluators').html(result);
        },
        error: function()
        {
            alert('Failed to show peer evaluators of this professor!');
        }
        
    });
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdatePeerSchedule', branch: branch, id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#a_u_f_id').val(result.f_id);
            $('#u_branch').val(branch);
            $('#a_branch').val(branch);
            $('#u_start_date').val(result.start_date);
            $('#u_end_date').val(result.end_date);
            $('#u_semester').val(result.semester);
            $('#u_semestral_term').val(result.semestral_term);
            $('#u_school_year').val(result.school_year);
            
        },
        error: function()
        {
            alert('Failed to show peer schedule data!');
        }
        
    });
    
    $('#UpdatePeerScheduleForm').submit(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                
                $('#PeerEvaluationData').html(result);
                $('#UpdatePeerScheduleModal').modal('hide');
                $('#text_content').text('Peer Schedule Successfully updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function() {
                alert('Failed to update peer schedule for this professor');
                $('#loading').hide();
            }
            
        });
        
    });
    
}