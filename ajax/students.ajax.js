function printDiv(divName) {
    var newWindow = window.open();
    var doc  = newWindow.document;
    doc.write("<html><head>" +
    "<title>Report</title>" + 
    "<link rel=\'stylesheet\' type=\'text/css\' href=\../../assets/css/bootstrap.min.css\' >" + "<style type=\'text/css\'>" + 
    "@media print {table td:last-child {display:none}table th:last-child {display:none}	html, body { display: block; }	}"+
    "</style>"+
    "</head><body>" +
    "<h1>Faculties</h1><br>"  +document.getElementById(divName).innerHTML  + "</body></html>");
    setTimeout(function(){newWindow.print(); newWindow.close(); }, 100);			
}

$(document).ready(function(){
    
    /* Load Course of the Selected Branch */
    $('#selectedBranch_loadCourse').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'loadCoursesPerBranch', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#loadCourseByBranch').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show courses!');
                $('#loading').hide();
            }
            
        });
        
    });
    
    $('#u_branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'loadCoursesPerBranch', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#u_course').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show courses!');
                $('#loading').hide();
            }
            
        });
        
    });
    
    /* Load MBC Students By Course */
    $('#mbcCourses').change(function(){
        
        var course = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showStudents', course: course, branch: 'MBC'},
            dataType: 'html',
            success: function(result)
            {
                $('#mbcstudentsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show student data!');
                $('#loading').hide();
            }
            
        });
        
    });
    
    /* Load MMC Students By Course */
    $('#mmcCourses').change(function(){
        
        var course = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showStudents', course: course, branch: 'MMC'},
            dataType: 'html',
            success: function(result)
            {
                $('#mmcstudentsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show student data!');
                $('#loading').hide();
            }
            
        });
        
    });
    
    /* Load MCC Students By Course */
    $('#mccCourses').change(function(){
        
        var course = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showStudents', course: course, branch: 'MCC'},
            dataType: 'html',
            success: function(result)
            {
                $('#mccstudentsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to show student data!');
                $('#loading').hide();
            }
            
        });
        
    });
                
});
            
$('#addStudentForm').submit(function(e){
                
    e.preventDefault();
    
    var branch = $('#selectedBranch_loadCourse').val();
    
    var branch2 = "";
    
    switch(branch) {
        case 'MBC':
            branch2 = "#mbcstudentsData";
            break;
        case 'MMC':
            branch2 = "#mmcstudentsData";
            break;
        case 'MCC':
            branch2 = "#mccstudentsData";
            break;
        default :
            break;
    }
    
    $('#loading').show();
               
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',                 
        success: function(result)
        {
            $(branch2).html(result);
            $('#addStudentModal').modal('hide');
            $('#text_content').text('Student successfully added!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to add student!');
            $('#loading').hide();
        }
    });
});
            
function updateStudent(id, branch) {
                
    $('#updateStudentModal').modal('show');
             
    $('#loading').show();
    
    $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'loadCoursesPerBranch', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#u_course').html(result);
            },
            error: function()
            {
                alert('Failed to show courses!');
            }
            
        });
    
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: {id: id, action: 'showUpdateStudent'},
        dataType: 'json',
        success: function(result)
        {
            $('#u_branch').val(result.branch);
            $('#u_firstname').val(result.firstname);
            $('#u_middlename').val(result.middlename);
            $('#u_lastname').val(result.lastname);
            $('#u_school_id').val(result.school_id);
            $('#u_course').val(result.course);
            $('#u_email').val(result.email);
            $('#u_curriculum_year').val(result.curriculum_year);
            $('#u_mobile_no').val(result.mobile);
            $('#u_id').val(id);
            $('#loading').hide();
        },
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert("Error: " + thrownError);
            $('#loading').hide();
        }
    });
                
    $('#updateStudentForm').submit(function(e){
        e.preventDefault();
        $('#loading').show();
        
        var branch2 = "";
        switch(branch) {
        case 'MBC':
            branch2 = "#mbcstudentsData";
            break;
        case 'MMC':
            branch2 = "#mmcstudentsData";
            break;
        case 'MCC':
            branch2 = "#mccstudentsData";
            break;
        default :
            break;
        }
        
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $(branch2).html(result);
                $('#updateStudentModal').modal('hide');
                $('#text_content').text('Student successfully updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Error: Failed to update student information!');
                $('#loading').hide();
            }
        });
    });
}
            
function deleteStudent(id, branch, course)
{
    $('#deleteStudentModal').modal("show");
    $('.yes').click(function(e){
        
        var branch2 = "";
        
        switch(branch) {
            case 'MBC':
                branch2 = "#mbcstudentsData";
                break;
            case 'MMC':
                branch2 = "#mmcstudentsData";
                break;
            case 'MCC':
                branch2 = "#mccstudentsData";
                break;
            default:
                break;
        }
        
    $('#loading').show();
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: {action: 'deleteStudent', id: id, branch: branch, course: course},
        dataType: 'html',
        success: function(result)
        {
            $(branch2).html(result);
            $('#deleteStudentModal').modal('hide');
            $('#text_content').text('Student successfully deleted!');
            $('#successModal').modal('show');
            $('#loading').hide();
        }, error: function()
        {
            alert('Failed to delete course!');
            $('#loading').hide();
        }
    });
    });
}

$(function(){
    
    $('#select_branch').change(function(){
        
        $('.branch_option').hide();
        $('#' + $(this).val()).show();
        
    });
    
});