$(document).ready(function(){
    
    /* Load subjects in MBC by Course */
    $('#mbc_courses').change(function(){
        
        var mbc_courses = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {course: mbc_courses, branch: 'MBC', action: 'loadSubjects'},
            datatype: 'html',
            success: function(result)
            {
                $('#mbcSubjectsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load subjects!');
                $('#loading').hide();
            }
            
        });
        
    });
    
});