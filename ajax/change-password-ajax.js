$(document).ready(function(){
        
        $('#curr_pw').keyup(function(){
            
            var pw_txt = $(this).val();
            
            $.ajax({
                
                url: 'actions.php',
                type: 'POST',
                data: {action: 'chk_curr_pw', pw_txt: pw_txt},
                dataType: 'html',
                success: function(result)
                {
                    $('#chk_curr_pw').html(result);
                },
                error: function()
                {
                    alert('Error: Filtering your current password!');
                }
                
            });
            
        });
        
        $('#new_pw').keyup(function(){
            
            var pw_txt = $(this).val();
            
            $.ajax({
                
                url: 'actions.php',
                data: {action: 'chk_new_pw', pw_txt: pw_txt},
                dataType: 'html',
                success: function(result)
                {
                    $('#chk_new_pw').html(result);
                },
                error: function()
                {
                    
                    alert('Error: Filtering your new password!');
                    
                }
                
            });
            
        });
    
});