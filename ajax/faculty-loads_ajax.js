$('#addSubjectForm').submit(function(e){
    
    e.preventDefault();
    
    var selected_branch = $('#branch').val();
    
    var load_branch = '';
    
    switch(selected_branch) {
        case 'MBC':
            load_branch = '#mbcSubjectsData';
            break;
        case 'MMC':
            load_branch = '#mmcSubjectsData';
            break;
        case 'MCC':
            load_branch = '#mccSubjectsData';
            break;
        default:
            break;
    }
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $(load_branch).html(result);
            $('#addSubjectModal').modal('hide');
            $('#text_content').text('Subject sucessfully added!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to add new Subject!');
            $('#loading').hide();
        }
        
    });
    
});

$(document).ready(function(){
    
    $('#branch').change(function(){
        
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'loadSubjectsPerBranch', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#coursesPerBranch').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load courses!');
                $('#loading').hide();
            }
            
        });
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showProfessorsPerBranch', branch: branch},
            dataType: 'html',
            success: function(result) {
                $('#professors').html(result);
            },
            error: function() {
                alert('Failed to show professors!');
            }
            
        });
        
    });
    
});

function updateSubject(id, branch, course) {
    
    $('#updateSubjectModal').modal('show');
    
    $.ajax({
            
        url: 'actions.php',
        type: 'POST',
        data: {action: 'loadSubjectsPerBranch', branch: branch},
        dataType: 'html',
        success: function(result)
        {
            $('#u_courses').html(result);
        },
        error: function()
        {
            alert('Failed to load courses!');            
        }
            
    });
    
    $.ajax({
            
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showProfessorsPerBranch', branch: branch},
        dataType: 'html',
        success: function(result) {
            $('#u_professors').html(result);
        },
        error: function() {
            alert('Failed to show professors!');
        }
            
    });    
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateSubjects', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#u_branch').val(branch);
            $('#u_courses').val(result.course);
            $('#u_curriculum_year').val(result.curriculum_year);
            $('#u_subject_code').val(result.subject_code);
            $('#u_subject_description').val(result.subject_description);
        },
        error: function()
        {}
        
    });
    
    $('#updateSubjectForm').submit(function(e){
        
        var selected_branch = branch;
    
        var load_branch = '';
    
        switch(selected_branch) {
            case 'MBC':
                load_branch = '#mbcSubjectsData';
                break;
            case 'MMC':
                load_branch = '#mmcSubjectsData';
                break;
            case 'MCC':
                load_branch = '#mccSubjectsData';
                break;
            default:
                break;
        }
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $(load_branch).html(result);
                $('#updateSubjectModal').modal('hide');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to update subject!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}

function deleteSubject(id, branch, course) {
    
    $('#deleteSubjectModal').modal('show');
    
    $('.yes').click(function(){
        
    $('#loading').show();
    
    var load_branch = "";
    
    switch(branch) {
        case 'MBC':
            load_branch = '#mbcSubjectsData';
            break;
        case 'MMC':
            load_branch = '#mmcSubjectsData';
            break;
        case 'MCC':
            load_branch = '#mccSubjectsData';
            break;
        default:
            break;
    }
        
    $.ajax({
          
        url: 'actions.php',
        type: 'POST',
        data: {action: 'deleteSubject', id: id, branch: branch, course: course},
        dataType: 'html',
        success: function(result)
        {
            
            $(load_branch).html(result);
            $('#deleteSubjectModal').modal('hide');
            $('#text_content').text('Subject successfully deleted');
            $('#successModal').modal('show');
            $('#loading').hide();
            
        },
        error: function()
        {
            
            alert('Unable to delete subject!');
            $('#loading').hide();
            
        }
        
    });
    
    });
    
}