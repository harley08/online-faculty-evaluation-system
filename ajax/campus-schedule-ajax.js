$(document).ready(function(){
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'ShowCampusScheduleData'},
        dataType: 'html',
        success: function(result)
        {
            $('#campusSchedulesData').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load Campus Schedules Data!');
            $('#loading').hide();
        }
        
    });
    
});

function updateCampusSched(id) {
    
    $('#updateCampusSchedModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateCampusSchedule', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#u_branch').val(result.branch);
            $('#u_start_date').val(result.start_date);
            $('#u_end_date').val(result.end_date);
            $('#u_status').val(result.status);
            $('#u_id').val(result.id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load campus schedule in inputs!');
            $('#loading').hide();
        }
        
    });
    
    $('#updateCampusSchedForm').submit(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            success: function(result)
            {
                $('#campusSchedulesData').html(result);
                $('#updateCampusSchedModal').modal('hide');
                $('#text_content').text('Campus Schedule Successfully Updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to update campus schedule!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}