$(document).ready(function(){
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showEvaluatorTypes'},
        dataType: 'html',
        success: function(result)
        {
            $('#evaluatorsTypeData').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load evaluator types!');
            $('#loading').hide();
        }
        
    });
    
});

$('#addEvaluatorForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $('#evaluatorsTypeData').html(result);
            $('#addEvaluatorModal').modal('hide');
            $('#text_content').text('New Evaluator Type Successfully Added!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            
            alert('Failed to add evaluator type!');
            $('#loading').hide();
            
        }
        
    });
    
});

function editEvaluationType(id) {
    
    $('#updateEvaluatorModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateEvaluatorType', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#u_evaluator').val(result.evaluator);
            $('#u_percentage').val(result.percentage);
            $('#u_total_evaluators').val(result.u_total_evaluators);
            $('#u_id').val(id);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to load Evaluator Type Data to inputs!');
            $('#loading').hide();
        }
        
    });
    
    $('#updateEvaluatorForm').submit(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $('#evaluatorsTypeData').html(result);
                $('#updateEvaluatorModal').modal('hide');
                $('#text_content').text('Evaluator Type Successfully Updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to update evaluation type!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}