$(document).ready(function(){
    
    $('#select_branch').change(function(){
        
        var branch_acronym = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {branch: branch_acronym, action: 'loadCourse'},
            success: function(result)
            {
                
                $('#coursesData').html(result);
                $('#coursesTable').DataTable();
                $('#loading').hide();
                
            },
            error: function()
            {
                
                alert('Unable to load courses from the selected branch!');
                $('#loading').hide();
                
            }
            
        });
        
    });
    
});

$('#addCourseForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        success: function(result)
        {
            $('#coursesData').html(result);
            $('#addCourseModal').modal('hide');
            $('#text_content').text('New course successfully added!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to add new course!');
            $('#loading').hide();
        }
        
    });
    
});

function updateCourse(id) {
    
    $('#updateCourseModal').modal('show');
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {id: id, action: 'showUpdateCourse'},
        dataType: 'json',
        success: function(data)
        {
            
            $('#u_branch').val(data.branch);
            $('#u_course_name').val(data.course_name);
            $('#u_course_acronym').val(data.course_acronym);
            $('#u_course_description').val(data.course_description);
            $('#u_id').val(id);
            $('#loading').hide();
            
        },
        error: function(xhr, ajaxOptions, thrownError)
        {
            
            alert('Failed to load course data! ' + thrownError);
            $('#loading').hide();
            
        }
        
    });
    
    $('#updateCourseForm').submit(function(e){
        
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {       
                $('#coursesData').html(result);
                $('#updateCourseModal').modal('hide');                
                $('#text_content').text('Course successfully updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
                
            },
            error: function()
            {
                
                alert('Failed to update course!');
                $('#loading').hide();
                
            }
            
        });
        
    });
    
}

function deleteCourse(id) {
    
    $('#deleteCourseModal').modal('show');
    
    $('.yes').click(function(e){
        
        e.preventDefault();
        
        var select_branch = $('#select_branch').val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {action: 'deleteCourse', id: id, branch: select_branch},
            dataType: 'html',
            success: function(result)
            {
                $('#coursesData').html(result);
                $('#deleteCourseModal').modal('hide');
                $('#text_content').text('Course successfully deleted!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to delete course!');
                $('#loading').hide();
            }
            
        });
        
    });
    
}