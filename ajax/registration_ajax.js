$("#password").password('toggle');

$('#verifyForm').submit(function(e){
    
    e.preventDefault();
    
    var register_as = $('#user').val();
    var user = "";
    
    switch (register_as) {
        case 'faculty':
            user = '#verifiedFacultyModal';
            break;
        case 'student_selected':
            user = '#verifiedStudentModal';
            break;
        default:
            break;
    }
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'reg-functions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(result)
        {
            $('#id').val(result.id);
            $('#fac_id').val(result.id);
            $('#username').val(result.username);
            $('#fac_username').val(result.username);
            $(user).modal('show');
            $('#loading').hide();
        },
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert('I dont know you!');
            $('#loading').hide();
        }
        
    });
    
});

$(function(){
    $('#user').change(function(){
        
        $('.register_option').hide();
        $('#' + $(this).val()).show();
        
    });
});

$('#createFacultyAccountForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'reg-functions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function()
        {
            $('#verifiedFacultyModal').modal('hide');
            $('#accountSuccessModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert("Failed to create faculty account!");
            $('#loading').hide();
        }
        
    });
    
});

$('#createStudentAccountForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'reg-functions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function()
        {
            $('#verifiedStudentModal').modal('hide');
            $('#accountSuccessModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to create student account!');
            $('#loading').hide();
        }
        
    });
    
});

$(document).ready(function(){
    
    $('#selectBranch').change(function(){
        
        var branch = $(this).val();
        
        $.ajax({
            
            url: 'reg-functions.php',
            type: 'POST',
            data: {action: 'loadCoursesPerBranch', branch: branch},
            dataType: 'html',
            success: function(result)
            {
                $('#coursesData').html(result);
            },
            error: function()
            {
                alert('Failed to show courses!');
            }
            
        });
    });
    
});