$(document).ready(function(){
    
    /* Load subjects in MMC by Course */
    $('#mmc_courses').change(function(){
        
        var mmc_courses = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            
            url: 'actions.php',
            type: 'POST',
            data: {course: mmc_courses, branch: 'MMC', action: 'loadSubjects'},
            datatype: 'html',
            success: function(result)
            {
                $('#mmcSubjectsData').html(result);
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load subjects!');
                $('#loading').hide();
            }
            
        });
        
    });
    
});