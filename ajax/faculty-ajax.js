$(document).ready(function(){
    
    /* Upload Faculty Avatar */
    $('#select_avatar').change(function(){
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                
                var i = 1;

                $('#loadImgSelected').show();

                var timer = setInterval(function(){

                    if (i === 0) {
                        $('#loadImgSelected').hide();
                        $('#selected_avatar').attr('src', e.target.result);
                        clearInterval(timer);
                    }
                    i--;

                }, 500);
            };
            
            reader.readAsDataURL(this.files[0]);
                      
        }
        
    });
                
    /* Load the Faculty Data */
    $('#selectBranch').change(function(){
                    
        var branch = $(this).val();
        
        $('#loading').show();
        
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {action: 'showFacultyData', branch: branch},
            dataType: 'html',
            success: function(result)
            {                
                $('#facultyData').html(result);
                $('#facultyTable').DataTable();
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to load the faculty data!');
                $('#loading').hide();
            }
        });
                    
    });
});
            
/* Add Faculty Form Handler */
$('#addFacultyForm').submit(function(e) {
               
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',                    
        success: function(result)
        {
            $('#facultyData').html(result);
            $('#addFacultyModal').modal('hide');
            $('#text_content').text('Faculty successfully added!');
            $('#successModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed!');
            $('#loading').hide();
        }
    });
});
            
function updateFaculty(id)
{
    $('#editFacultyModal').modal('show');
    $('#loading').show();
               
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: {id: id, action: 'showUpdateFaculty'},
        dataType: 'json',
        success: function(result)
        {
            $('#u_branch').val(result.branch);
            $('#u_firstname').val(result.firstname);
            $('#u_middlename').val(result.middlename);
            $('#u_lastname').val(result.lastname);
            $('#u_employment_status').val(result.employment_status);
            $('#u_email').val(result.email);
            $('#u_mobile').val(result.mobile);
            $('#u_role').val(result.role);
            $('#u_id').val(id);
            $('#loading').hide();
        },
        error: function(error) {
            alert("Error: " + error);
            $('#loading').hide();
        }
    });
               
    $('#updateFacultyForm').submit(function(e){
                 
        e.preventDefault();
        
        $('#loading').show();
        
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $('#facultyData').html(result);
                $('#editFacultyModal').modal('hide');
                $('#text_content').text('Faculty successfully updated!');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Error: Failed to update faculty information');
                $('#loading').hide();
            }
    });
                  
});
}
            
function deleteFaculty(id, branch)
{
    $('#deleteFacultyModal').modal("show");
    
    $('.yes').click(function(e){
        
    $('#loading').show();            
    $.ajax({
        url: 'actions.php',
        type: 'POST',
        data: {action: 'deleteFaculty', id: id, branch: branch},
        dataType: 'html',
        success: function(result)
        {
            
            $('#facultyData').html(result);
            $('#deleteFacultyModal').modal('hide');
            $('#text_content').text('Faculty successfully removed!');
            $('#successModal').modal('show');
            $('#loading').hide();
            
        },
        error: function()
        {
            alert('Failed to remove faculty!');
            $('#loading').hide();
        }
        
    });
    
    });
    
}

function viewFacultyInfo(id) {
    
    $('#showFacInfoModal').modal('show');
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'viewFacultyInfo', id},
        dataType: 'json',
        success: function(result)
        {
            $('#show_fullname').text(result.fullname);
            $('#show_mobile').text(result.mobile);
            $('#show_email').text(result.email);
        },
        error: function()
        {
            alert('Failed to show faculty info!');
        }
        
    });
    
}