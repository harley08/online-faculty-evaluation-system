$(document).ready(function(){
    var i = 4;
    var time = $('#time');
    
    time.html('5');

    var timer = setInterval(function(){
        
        time.html(i);
        
        if (i === 0) {
            $('#show').show();
            $('#fetch').hide();
            clearInterval(timer);
        }
        i--;

    }, 1000);
});

$('#showEvaluationsForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        success: function(result)
        {
            $('#show').hide();
            $('#evaluationsData').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to show evaluations!');
            $('#loading').hide();
        }
        
    });
    
});