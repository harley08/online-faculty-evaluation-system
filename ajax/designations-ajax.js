$(document).ready(function(){
    
    $('#loading').show();
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showDesignationData'},
        dataType: 'html',
        success: function(result)
        {
            $('#designationsData').html(result);
            $('#loading').hide();
        },
        error: function()
        {
            alert('Failed to show designation data!');
            $('#loading').hide();
        }
        
    });
    
});

$('#addDesignationForm').submit(function(e) {
    
    e.preventDefault();
    
    $('#loading').show();
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function(result)
        {
            $('#loading').hide();
            $('#designationsData').html(result);
            $('#addDesignationModal').modal('hide');
            $('#text_content').tex('Designation successfully added!');
            $('#successModal').modal('show');
            
        },
        error: function()
        {
            alert('Failed to add new designation!');
            $('#loading').hide();
        }
        
        
    });
    
});

function updateDesignation(id) {
    
    $('#updateDesignationModal').modal('show');
    
    $.ajax({
        
        url: 'actions.php',
        type: 'POST',
        data: {action: 'showUpdateDesignation', id: id},
        dataType: 'json',
        success: function(result)
        {
            $('#u_designation').val(result.designation);
            $('#u_peer').val(result.peer);
            $('#u_supervisor').val(result.supervisor);
            $('#u_id').val(id);
        },
        error: function()
        {
            alert('Failed to show designation data!');
        }
        
    });

    $('#updateDesignationForm').submit(function(e){

        e.preventDefault();

        $('#loading').show();

        $.ajax({

            url: 'actions.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result)
            {
                $('#designationsData').html(result);
                $('#updateDesignationModal').modal('hide');
                $('#text_content').text('Designation successfully updated!');
                $('#successModal').modal('show');
                $('#loading').hide();
            },
            error: function()
            {
                alert('Failed to update designation');
                $('#loading').hide();
            }

        });

    });
    
}