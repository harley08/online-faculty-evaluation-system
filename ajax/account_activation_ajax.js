$('#activateAccountForm').submit(function(e){
    
    e.preventDefault();
    
    $('#loading').show();
    $.ajax({
        
        url: 'verify-functions.php',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'html',
        success: function()
        {
            $('#successActivateModal').modal('show');
            $('#loading').hide();
        },
        error: function()
        {
            alert('Invalid School ID/Faculty/User Code and Activation Code!');
            $('#loading').hide();
        }
        
    });
    
});