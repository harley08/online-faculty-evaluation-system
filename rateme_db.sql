-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2018 at 03:31 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rateme_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `campus_schedules`
--

CREATE TABLE `campus_schedules` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campus_schedules`
--

INSERT INTO `campus_schedules` (`id`, `branch`, `start_date`, `end_date`, `status`) VALUES
(1, 'MBC', '2018-02-01', '2018-02-28', 'Active'),
(3, 'MMC', '2018-01-09', '2018-04-19', 'Active'),
(4, 'MCC', '2018-01-09', '2018-04-19', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category` varchar(1) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `percentage` decimal(10,2) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category`, `category_name`, `percentage`, `status`) VALUES
(28, 'A', 'Commitment', '0.25', 'Active'),
(29, 'B', 'Knowledge of the Subject', '0.25', 'Active'),
(30, 'C', 'Teaching for Independent Learning', '0.25', 'Active'),
(32, 'D', 'Management of Learning', '0.25', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `semester` varchar(20) NOT NULL,
  `semestral_term` varchar(20) NOT NULL,
  `school_year` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `branch`, `user_code`, `date`, `semester`, `semestral_term`, `school_year`) VALUES
(2, 'MBC', '2014-0555', '2018-01-31 18:53:19', 'First Semester', 'Finals', '2017-2018'),
(3, 'MBC', '2014-0547', '2018-02-01 17:35:23', 'First Semester', 'Finals', '2017-2018'),
(4, 'MBC', 'f5763', '2018-02-01 22:59:59', 'First Semester', 'Finals', '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `course_name` text NOT NULL,
  `course_acronym` varchar(10) NOT NULL,
  `course_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `branch`, `course_name`, `course_acronym`, `course_description`) VALUES
(11, 'MBC', 'Bachelor of Science in Computer Engineering', 'BSCPE', 'BSCPE'),
(12, 'MBC', 'Bachelor of Science in Information Technology', 'BSIT', 'BSIT'),
(15, 'MBC', 'Bachelor of Science in Crimonology', 'BSCrim', 'BSCrim'),
(16, 'MBC', 'Bachelor of Science in Hotel and Restaurant Management', 'BSHRM', 'BSHRM'),
(17, 'MBC', 'Bachelor of Science in Hotel and Tourism Management', 'BSHTM', 'BSHTM'),
(18, 'MBC', 'Bachelor of Science in Fisheries', 'BSFi', 'BSFi'),
(19, 'MBC', 'Bachelor of Secondary Education - Mathematics', 'BSEd - Mat', 'BSEd - Mathematics'),
(20, 'MBC', 'Bachelor of Secondary Education - English', 'BSEd - Eng', 'BSEd - English'),
(21, 'MBC', 'Bachelor of Secondary Education - Biological Science', 'BSEd - Bio', 'BSEd - Bio Sci'),
(22, 'MBC', 'Bachelor of Secondary Education - TLE', 'BSEd - TLE', 'BSEd - TLE'),
(23, 'MBC', 'Bachelor of Elementary Education', 'BEEd', 'BEEd'),
(24, 'MBC', 'Bachelor of Art and Science - Political Science', 'BS - Pol S', 'BS - Pol Sci'),
(25, 'MBC', 'Bachelor of Science in Entrepreneurship', 'BS -Entrep', 'BS - Entrep');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `peer` int(11) NOT NULL,
  `supervisor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation`, `peer`, `supervisor`) VALUES
(1, 'Instructor', 1, 5),
(2, 'Assistant Professor', 0, 0),
(3, 'Principal, Laboratory High School', 0, 0),
(4, 'Director for Research', 0, 0),
(5, 'Director for Instruction', 0, 0),
(6, 'Director for Extension', 0, 0),
(7, 'Director for Production & Business Operation', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_results`
--

CREATE TABLE `evaluation_results` (
  `id` int(11) NOT NULL,
  `evaluation_type` varchar(10) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `f_id` int(11) NOT NULL,
  `category` varchar(1) NOT NULL,
  `question_id` int(11) NOT NULL,
  `rating` decimal(10,2) NOT NULL,
  `comment` text NOT NULL,
  `semester` varchar(25) NOT NULL,
  `semestral_term` varchar(50) NOT NULL,
  `school_year` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluation_results`
--

INSERT INTO `evaluation_results` (`id`, `evaluation_type`, `branch`, `user_code`, `f_id`, `category`, `question_id`, `rating`, `comment`, `semester`, `semestral_term`, `school_year`) VALUES
(81, 'self', 'MBC', 'f5500', 16, 'A', 3, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(82, 'self', 'MBC', 'f5500', 16, 'A', 4, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(83, 'self', 'MBC', 'f5500', 16, 'A', 5, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(84, 'self', 'MBC', 'f5500', 16, 'A', 10, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(85, 'self', 'MBC', 'f5500', 16, 'A', 11, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(86, 'self', 'MBC', 'f5500', 16, 'B', 7, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(87, 'self', 'MBC', 'f5500', 16, 'B', 8, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(88, 'self', 'MBC', 'f5500', 16, 'B', 9, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(89, 'self', 'MBC', 'f5500', 16, 'B', 12, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(90, 'self', 'MBC', 'f5500', 16, 'B', 13, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(91, 'self', 'MBC', 'f5500', 16, 'C', 14, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(92, 'self', 'MBC', 'f5500', 16, 'C', 15, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(93, 'self', 'MBC', 'f5500', 16, 'C', 16, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(94, 'self', 'MBC', 'f5500', 16, 'C', 17, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(95, 'self', 'MBC', 'f5500', 16, 'C', 18, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(96, 'self', 'MBC', 'f5500', 16, 'D', 19, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(97, 'self', 'MBC', 'f5500', 16, 'D', 20, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(98, 'self', 'MBC', 'f5500', 16, 'D', 21, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(99, 'self', 'MBC', 'f5500', 16, 'D', 22, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(100, 'self', 'MBC', 'f5500', 16, 'D', 23, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(101, 'student', 'MBC', '2014-0555', 16, 'A', 3, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(102, 'student', 'MBC', '2014-0555', 16, 'A', 4, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(103, 'student', 'MBC', '2014-0555', 16, 'A', 5, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(104, 'student', 'MBC', '2014-0555', 16, 'A', 10, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(105, 'student', 'MBC', '2014-0555', 16, 'A', 11, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(106, 'student', 'MBC', '2014-0555', 16, 'B', 7, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(107, 'student', 'MBC', '2014-0555', 16, 'B', 8, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(108, 'student', 'MBC', '2014-0555', 16, 'B', 9, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(109, 'student', 'MBC', '2014-0555', 16, 'B', 12, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(110, 'student', 'MBC', '2014-0555', 16, 'B', 13, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(111, 'student', 'MBC', '2014-0555', 16, 'C', 14, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(112, 'student', 'MBC', '2014-0555', 16, 'C', 15, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(113, 'student', 'MBC', '2014-0555', 16, 'C', 16, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(114, 'student', 'MBC', '2014-0555', 16, 'C', 17, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(115, 'student', 'MBC', '2014-0555', 16, 'C', 18, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(116, 'student', 'MBC', '2014-0555', 16, 'D', 19, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(117, 'student', 'MBC', '2014-0555', 16, 'D', 20, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(118, 'student', 'MBC', '2014-0555', 16, 'D', 21, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(119, 'student', 'MBC', '2014-0555', 16, 'D', 22, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(120, 'student', 'MBC', '2014-0555', 16, 'D', 23, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(121, 'self', 'MBC', 'f5453', 24, 'A', 3, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(122, 'self', 'MBC', 'f5453', 24, 'A', 4, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(123, 'self', 'MBC', 'f5453', 24, 'A', 5, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(124, 'self', 'MBC', 'f5453', 24, 'A', 10, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(125, 'self', 'MBC', 'f5453', 24, 'A', 11, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(126, 'self', 'MBC', 'f5453', 24, 'B', 7, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(127, 'self', 'MBC', 'f5453', 24, 'B', 8, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(128, 'self', 'MBC', 'f5453', 24, 'B', 9, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(129, 'self', 'MBC', 'f5453', 24, 'B', 12, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(130, 'self', 'MBC', 'f5453', 24, 'B', 13, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(131, 'self', 'MBC', 'f5453', 24, 'C', 14, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(132, 'self', 'MBC', 'f5453', 24, 'C', 15, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(133, 'self', 'MBC', 'f5453', 24, 'C', 16, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(134, 'self', 'MBC', 'f5453', 24, 'C', 17, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(135, 'self', 'MBC', 'f5453', 24, 'C', 18, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(136, 'self', 'MBC', 'f5453', 24, 'D', 19, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(137, 'self', 'MBC', 'f5453', 24, 'D', 20, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(138, 'self', 'MBC', 'f5453', 24, 'D', 21, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(139, 'self', 'MBC', 'f5453', 24, 'D', 22, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(140, 'self', 'MBC', 'f5453', 24, 'D', 23, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(141, 'student', 'MBC', '2014-0555', 15, 'A', 3, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(142, 'student', 'MBC', '2014-0555', 15, 'A', 4, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(143, 'student', 'MBC', '2014-0555', 15, 'A', 5, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(144, 'student', 'MBC', '2014-0555', 15, 'A', 10, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(145, 'student', 'MBC', '2014-0555', 15, 'A', 11, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(146, 'student', 'MBC', '2014-0555', 15, 'B', 7, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(147, 'student', 'MBC', '2014-0555', 15, 'B', 8, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(148, 'student', 'MBC', '2014-0555', 15, 'B', 9, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(149, 'student', 'MBC', '2014-0555', 15, 'B', 12, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(150, 'student', 'MBC', '2014-0555', 15, 'B', 13, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(151, 'student', 'MBC', '2014-0555', 15, 'C', 14, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(152, 'student', 'MBC', '2014-0555', 15, 'C', 15, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(153, 'student', 'MBC', '2014-0555', 15, 'C', 16, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(154, 'student', 'MBC', '2014-0555', 15, 'C', 17, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(155, 'student', 'MBC', '2014-0555', 15, 'C', 18, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(156, 'student', 'MBC', '2014-0555', 15, 'D', 19, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(157, 'student', 'MBC', '2014-0555', 15, 'D', 20, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(158, 'student', 'MBC', '2014-0555', 15, 'D', 21, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(159, 'student', 'MBC', '2014-0555', 15, 'D', 22, '5.00', '', 'First Semester', 'Finals', '2017-2018'),
(160, 'student', 'MBC', '2014-0555', 15, 'D', 23, '5.00', '', 'First Semester', 'Finals', '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `evaluator_types`
--

CREATE TABLE `evaluator_types` (
  `id` int(11) NOT NULL,
  `evaluator` varchar(25) NOT NULL,
  `percentage` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluator_types`
--

INSERT INTO `evaluator_types` (`id`, `evaluator`, `percentage`) VALUES
(1, 'student', '0.20'),
(2, 'peer', '0.25'),
(3, 'supervisor', '0.30'),
(4, 'self', '0.20');

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `f_id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `f_code` varchar(9) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `employment_status` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `role` varchar(15) NOT NULL,
  `access_code` varchar(8) NOT NULL,
  `avatar_link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`f_id`, `branch`, `f_code`, `firstname`, `middlename`, `lastname`, `employment_status`, `email`, `mobile`, `role`, `access_code`, `avatar_link`) VALUES
(14, 'MBC', 'f5763', 'Gerolyn', 'Bacudo', 'Javier', 'Part-Time', 'gerolynjavier@gmail.com', '09551655746', 'Professor', 'LWOE3noD', ''),
(15, 'MBC', 'f7697', 'Glenda', 'Pacaul', 'Binay', 'Full Time', 'glendapbinay@gmail.com', '09551655747', 'Professor', 'wNUiOIQy', ''),
(16, 'MBC', 'f5500', 'Sheryl Mae', 'Dino', 'Lainez', 'Full Time', 'leon.she24@gmail.com', '09551655747', 'Professor', 'WsnxzDr0', ''),
(22, 'MBC', 'f2840', 'Evelyn', 'Anthony', 'Rodriguez', 'Permanent', 'Evelyn@gmail.com', '09098080231', 'Professor', 'ecqvXoF8', ''),
(23, 'MBC', 'f9255', 'Nicko', 'A', 'Magnaye', 'Full Time', 'magnaye@gmail.com', '09368392146', 'Professor', 'ZuSH0nPM', ''),
(24, 'MBC', 'f5453', 'Ariel', 'J', 'Jimenez', 'Permanent', 'arieljimenez@gmail.com', '09472222999', 'Professor', 'XmD0qE7H', ''),
(25, 'MBC', 'f7329', 'Enrique', 'A', 'Magalay', 'Full Time', 'enriquemagalay@gmail.com', '09395278906', 'Professor', '7t2QinPb', ''),
(26, 'MBC', 'f1468', 'Jay', 'B', 'Fallan', 'Permanent', 'jay@gmail.com', '09305823882', 'Professor', 'hWE8OXq5', ''),
(27, 'MBC', 'f3679', 'Paula', '', 'Mercene', 'Permanent', 'paulamercene@gmail.com', '09305381047', 'Professor', 'CNnqIX2i', ''),
(28, 'MBC', 'f9169', 'Jennilyn', '', 'Pena', 'Permanent', 'jennilynpena@gmail.com', '09063882492', 'Advisor', 'PCocmsOT', ''),
(29, 'MBC', 'f2469', 'Elsie', '', 'Guibone', 'Full Time', 'guivone@gmail.com', '09482847382', 'Professor', 'XoH8VBwa', ''),
(30, 'MBC', 'f1450', 'Elaine', '', 'Dela Cruz', 'Full Time', 'delacruzelaine@gmail.com', '09482847373', 'Professor', 'IklGSUQP', ''),
(31, 'MBC', 'f3702', 'Marco', '', 'Rico', 'Permanent', 'marcorico@gmail.com', '09163926218', 'Professor', 'YWz7ZELn', ''),
(32, 'MBC', 'f9370', 'Myla', '', 'Izon', 'Permanent', 'mylaizon@gmail.com', '09279294832', 'Professor', 'xhq7DMHd', ''),
(33, 'MBC', 'f3604', 'Alona', '', 'Labaguis', 'Permanent', 'alonalabaguis@gmail.com', '09279294691', 'Professor', 'g0ThLHCG', ''),
(34, 'MBC', 'f7431', 'Benjamin', '', 'Mataya', 'Permanent', 'mataya@gmail.com', '09361294392', 'Professor', 'jBvG4qcA', ''),
(35, 'MBC', 'f2021', 'Lorie Jane', '', 'Rey', 'Permanent', 'reylorie@gmail.com', '09355829918', 'Professor', 'sO5owVCG', ''),
(36, 'MBC', 'f5044', 'Eva', 'Villanueva', 'Brinosa', 'Permanent', 'evabrinosa@gmail.com', '09355829964', 'Supervisor', 'l3FJgkv6', ''),
(37, 'MBC', 'f5195', 'Gladys Wajima', '', 'De Guzman', 'Permanent', 'gladysdeguzman@gmail.com', '09355820431', 'Professor', 'koyMSJxO', ''),
(38, 'MBC', 'f0114', 'Raymond', '', 'Taladtad', 'Full Time', 'taladtad@gmail.com', '09355820479', 'Professor', '7idfGpgW', ''),
(39, 'MBC', 'f1928', 'William', '', 'Ramirez', 'Full Time', 'ramirez@gmail.com', '09169437831', 'Professor', 'TZeNfUhC', ''),
(40, 'MBC', 'f9372', 'Mark', '', 'De Guzman', 'Full Time', 'deguzman@gmail.com', '09358939218', 'Professor', 'j6314I5u', ''),
(41, 'MBC', 'f7618', 'Ciedel', '', 'Salazar', 'Full Time', 'salazarciedel@gmail.com', '09129343854', 'Professor', 'VuNYTcoW', ''),
(42, 'MBC', 'f6876', 'Ernesto Jr.', '', 'Rodriguez', 'Full Time', 'rodriguezern@gmail.com', '09486660994', 'Professor', 'dKnwu8Am', ''),
(43, 'MBC', 'f5811', 'Janeth ', '', 'Linga', 'Permanent', 'lingajaneth@gmail.com', '09329499220', 'Professor', 'ripZ50H1', ''),
(44, 'MBC', 'f9230', 'Madonna', 'Palapus', 'Melchor', 'Permanent', 'madonna@gmail.com', '09369392014', 'Professor', 'bgwQ3Ccd', ''),
(45, 'MBC', 'f0546', 'Zernan', '', 'Maling', 'Full Time', 'zernanmaling@gmail.com', '09169376421', 'Professor', 'PdwCiXJH', ''),
(46, 'MBC', 'f8148', 'Edelyn', '', 'Maling', 'Full Time', 'edzmaling2@gmail.com', '09483639541', 'Professor', '2EM19SZK', ''),
(47, 'MBC', 'f9494', 'Leonardo', '', 'Gresos', 'Permanent', 'gresosleonardo@gmail.com', '09568392653', 'Professor', 'cZU7SsBY', ''),
(48, 'MBC', 'f0786', 'Mailen Mae', '', 'Yadao', 'Full Time', 'yadao@gmail.com', '09469375392', 'Professor', 'NCshr5jZ', ''),
(49, 'MBC', 'f0576', 'Constancia ', 'M', 'Dagsaan', 'Permanent', 'constanciadagsaan@gmail.com', '09096216086', 'Professor', 'NGobDgrp', ''),
(50, 'MBC', 'f2014', 'Regal', 'R', 'Izon', 'Permanent', 'regalizon@gmail.com', '09097654345', 'Professor', 'OJCKdUA3', ''),
(51, 'MBC', 'f1587', 'Enrico', 'A', 'Castillo', 'Permanent', 'enricocastillo@gmail.com', '09127634857', 'Professor', 'YLoeTfnc', ''),
(52, 'MBC', 'f4828', 'Mylene', 'C', 'Brinosa', 'Permanent', 'mylene@gmail.com', '09096216086', 'Professor', 'H9G1CDms', ''),
(53, 'MBC', 'f0313', 'Zussette ', 'C', 'Aplaon', 'Permanent', 'zussette@gmail.com', '09365427864', 'Professor', '2ipD1QyW', ''),
(54, 'MBC', 'f9871', 'Hazelle Paulene ', 'A', 'Reamosio', 'Permanent', 'hazellep@gmail.com', '09354217685', 'Professor', 'PihkRG7m', ''),
(55, 'MBC', 'f9979', 'Brian', 'M', 'Elaydo', 'Permanent', 'brian@gmail.com', '09357654326', 'Professor', '03uFoJiS', ''),
(56, 'MBC', 'f6448', 'Rosalinda', 'R', 'Maling', 'Permanent', 'rosalinda@gmail.com', '09167654234', 'Professor', '1qaf4XIh', ''),
(57, 'MBC', 'f9921', 'Marian', 'T', 'Urate', 'Part-Time', 'marian@gmail.com', '09365427186', 'Professor', 't1yElixb', ''),
(58, 'MBC', 'f4172', 'Roland', 'D', 'Urate', 'Part-Time', 'roland@gmail.com', '09557543239', 'Professor', 'End2yNMZ', ''),
(59, 'MBC', 'f6489', 'Miriam', 'DC', 'Enriquez', 'Full Time', 'miriam@gmail.com', '09157623109', 'Professor', '2IL8meqi', ''),
(63, 'MBC', 'f7153', 'Joel', 'Moreno', 'Lumagui', 'Full Time', 'lumagui.harl0723@gmail.com', '09551655757', 'Professor', 'eruzI1ZF', '');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_students`
--

CREATE TABLE `faculty_students` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `f_id` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `semestral_term` varchar(50) NOT NULL,
  `school_year` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `overall_results`
--

CREATE TABLE `overall_results` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `f_id` int(11) NOT NULL,
  `student_total` decimal(10,2) NOT NULL,
  `peer_total` decimal(10,2) NOT NULL,
  `supervisor_total` decimal(10,2) NOT NULL,
  `self_total` decimal(10,2) NOT NULL,
  `semester` varchar(20) NOT NULL,
  `semestral_term` varchar(50) NOT NULL,
  `school_year` varchar(20) NOT NULL,
  `total_qce` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peer_schedules`
--

CREATE TABLE `peer_schedules` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `f_id` int(11) NOT NULL,
  `user_code` varchar(20) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `semester` varchar(20) NOT NULL,
  `semestral_term` varchar(20) NOT NULL,
  `school_year` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peer_schedules`
--

INSERT INTO `peer_schedules` (`id`, `branch`, `f_id`, `user_code`, `start_date`, `end_date`, `semester`, `semestral_term`, `school_year`) VALUES
(21, 'MBC', 14, 'f7329', '2018-02-18', '2018-02-26', 'First Semester', 'Finals', '2017-2018'),
(22, 'MBC', 14, 'f1468', '2018-02-18', '2018-02-26', 'First Semester', 'Finals', '2017-2018'),
(23, 'MBC', 14, 'f3679', '2018-02-18', '2018-02-26', 'First Semester', 'Finals', '2017-2018'),
(24, 'MBC', 14, 'f9169', '2018-02-18', '2018-02-26', 'First Semester', 'Finals', '2017-2018'),
(25, 'MBC', 14, 'f2469', '2018-02-18', '2018-02-26', 'First Semester', 'Finals', '2017-2018'),
(31, 'MBC', 24, 'f5763', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(32, 'MBC', 24, 'f7697', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(33, 'MBC', 24, 'f5500', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(34, 'MBC', 24, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(35, 'MBC', 24, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(41, 'MBC', 25, 'f1928', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(42, 'MBC', 25, 'f9372', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(43, 'MBC', 25, 'f7618', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(44, 'MBC', 25, 'f6876', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(45, 'MBC', 25, 'f5811', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(46, 'MBC', 26, 'f7697', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(47, 'MBC', 26, 'f5500', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(48, 'MBC', 26, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(49, 'MBC', 26, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(50, 'MBC', 26, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(51, 'MBC', 27, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(52, 'MBC', 27, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(53, 'MBC', 27, 'f5195', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(54, 'MBC', 27, 'f0114', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(55, 'MBC', 27, 'f1928', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(56, 'MBC', 28, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(57, 'MBC', 28, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(58, 'MBC', 28, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(59, 'MBC', 28, 'f0313', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(60, 'MBC', 28, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(61, 'MBC', 29, 'f9979', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(62, 'MBC', 29, 'f6448', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(63, 'MBC', 29, 'f9921', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(64, 'MBC', 29, 'f4172', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(65, 'MBC', 29, 'f6489', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(66, 'MBC', 30, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(67, 'MBC', 30, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(68, 'MBC', 30, 'f0313', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(69, 'MBC', 30, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(70, 'MBC', 30, 'f9979', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(71, 'MBC', 31, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(72, 'MBC', 31, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(73, 'MBC', 31, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(74, 'MBC', 31, 'f7329', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(75, 'MBC', 31, 'f1468', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(76, 'MBC', 32, 'f5500', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(77, 'MBC', 32, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(78, 'MBC', 32, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(79, 'MBC', 32, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(80, 'MBC', 32, 'f7329', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(81, 'MBC', 33, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(82, 'MBC', 33, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(83, 'MBC', 33, 'f7329', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(84, 'MBC', 33, 'f1468', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(85, 'MBC', 33, 'f3679', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(86, 'MBC', 34, 'f0786', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(87, 'MBC', 34, 'f0576', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(88, 'MBC', 34, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(89, 'MBC', 34, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(90, 'MBC', 34, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(91, 'MBC', 35, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(92, 'MBC', 35, 'f5195', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(93, 'MBC', 35, 'f0114', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(94, 'MBC', 35, 'f1928', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(95, 'MBC', 35, 'f9372', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(96, 'MBC', 36, 'f5763', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(97, 'MBC', 36, 'f7697', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(98, 'MBC', 36, 'f5500', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(99, 'MBC', 36, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(100, 'MBC', 36, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(101, 'MBC', 37, 'f9230', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(102, 'MBC', 37, 'f0546', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(103, 'MBC', 37, 'f8148', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(104, 'MBC', 37, 'f9494', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(105, 'MBC', 37, 'f0786', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(106, 'MBC', 38, 'f9494', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(107, 'MBC', 38, 'f0786', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(108, 'MBC', 38, 'f0576', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(109, 'MBC', 38, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(110, 'MBC', 38, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(111, 'MBC', 39, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(112, 'MBC', 39, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(113, 'MBC', 39, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(114, 'MBC', 39, 'f0313', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(115, 'MBC', 39, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(116, 'MBC', 40, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(117, 'MBC', 40, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(118, 'MBC', 40, 'f5195', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(119, 'MBC', 40, 'f0114', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(120, 'MBC', 40, 'f1928', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(121, 'MBC', 41, 'f0576', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(122, 'MBC', 41, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(123, 'MBC', 41, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(124, 'MBC', 41, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(125, 'MBC', 41, 'f0313', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(126, 'MBC', 42, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(127, 'MBC', 42, 'f7329', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(128, 'MBC', 42, 'f1468', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(129, 'MBC', 42, 'f3679', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(130, 'MBC', 42, 'f9169', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(131, 'MBC', 43, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(132, 'MBC', 43, 'f9979', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(133, 'MBC', 43, 'f6448', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(134, 'MBC', 43, 'f9921', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(135, 'MBC', 43, 'f4172', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(136, 'MBC', 44, 'f0576', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(137, 'MBC', 44, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(138, 'MBC', 44, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(139, 'MBC', 44, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(140, 'MBC', 44, 'f0313', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(141, 'MBC', 45, 'f7431', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(142, 'MBC', 45, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(143, 'MBC', 45, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(144, 'MBC', 45, 'f5195', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(145, 'MBC', 45, 'f0114', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(146, 'MBC', 46, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(147, 'MBC', 46, 'f9979', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(148, 'MBC', 46, 'f6448', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(149, 'MBC', 46, 'f9921', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(150, 'MBC', 46, 'f4172', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(151, 'MBC', 47, 'f7697', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(152, 'MBC', 47, 'f5500', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(153, 'MBC', 47, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(154, 'MBC', 47, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(155, 'MBC', 47, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(156, 'MBC', 48, 'f5500', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(157, 'MBC', 48, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(158, 'MBC', 48, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(159, 'MBC', 48, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(160, 'MBC', 48, 'f7329', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(161, 'MBC', 49, 'f3702', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(162, 'MBC', 49, 'f9370', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(163, 'MBC', 49, 'f3604', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(164, 'MBC', 49, 'f7431', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(165, 'MBC', 49, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(166, 'MBC', 50, 'f5195', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(167, 'MBC', 50, 'f0114', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(168, 'MBC', 50, 'f1928', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(169, 'MBC', 50, 'f9372', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(170, 'MBC', 50, 'f7618', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(171, 'MBC', 51, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(172, 'MBC', 51, 'f9979', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(173, 'MBC', 51, 'f6448', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(174, 'MBC', 51, 'f9921', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(175, 'MBC', 51, 'f4172', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(176, 'MBC', 52, 'f9370', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(177, 'MBC', 52, 'f3604', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(178, 'MBC', 52, 'f7431', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(179, 'MBC', 52, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(180, 'MBC', 52, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(181, 'MBC', 53, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(182, 'MBC', 53, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(183, 'MBC', 53, 'f5195', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(184, 'MBC', 53, 'f0114', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(185, 'MBC', 53, 'f1928', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(186, 'MBC', 54, 'f9494', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(187, 'MBC', 54, 'f0786', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(188, 'MBC', 54, 'f0576', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(189, 'MBC', 54, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(190, 'MBC', 54, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(191, 'MBC', 55, 'f2840', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(192, 'MBC', 55, 'f9255', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(193, 'MBC', 55, 'f5453', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(194, 'MBC', 55, 'f7329', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(195, 'MBC', 55, 'f1468', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(196, 'MBC', 56, 'f9370', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(197, 'MBC', 56, 'f3604', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(198, 'MBC', 56, 'f7431', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(199, 'MBC', 56, 'f2021', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(200, 'MBC', 56, 'f5044', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(201, 'MBC', 57, 'f0576', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(202, 'MBC', 57, 'f2014', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(203, 'MBC', 57, 'f1587', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(204, 'MBC', 57, 'f4828', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(205, 'MBC', 57, 'f0313', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(211, 'MBC', 59, 'f7618', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(212, 'MBC', 59, 'f6876', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(213, 'MBC', 59, 'f5811', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(214, 'MBC', 59, 'f9230', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(215, 'MBC', 59, 'f0546', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(216, 'MBC', 16, 'f9871', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(217, 'MBC', 16, 'f9979', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(218, 'MBC', 16, 'f6448', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(219, 'MBC', 16, 'f9921', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018'),
(220, 'MBC', 16, 'f4172', '2018-02-01', '2018-02-28', 'First Semester', 'Finals', '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `category` varchar(1) NOT NULL,
  `question` text NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `category`, `question`, `status`) VALUES
(3, 'A', 'Demonstrates sensitivity to students ability to attend and absorb content information.', 'Active'),
(4, 'A', 'Integrates sensitivity his/her learning objectives with those of the students in a collaborative process.', 'Active'),
(5, 'A', 'Makes self-available to students beyond official time.', 'Active'),
(7, 'B', 'Demonstrates mastery of the subject matter (explain the subject matter without relaying solely on the prescribed textbook).', 'Active'),
(8, 'B', 'Draws and share information on the state on the art of theory and practice in his/her discipline.', 'Active'),
(9, 'B', 'Integrates subjects to practical circumstances and learning intents/purposes of students.', 'Active'),
(10, 'A', 'Regularly comes to class on time, well-groomed and well-prepared to complete assigned responsibilities', 'Active'),
(11, 'A', 'Keeps accurate records of students performance and prompt submission of the same ', 'Active'),
(12, 'B', 'Explains the revelance of present present topics to the  previous lessons, and relates the subject matter to relevant current issues and/or daily live activities', 'Active'),
(13, 'B', 'Demostrates up-to-date knowledge and/or awareness on current trends and issues of the subject', 'Active'),
(14, 'C', 'Creates teaching strategies that allow students to practice using concepts they need to understand (interactive discussion)', 'Active'),
(15, 'C', 'Enhances student self-esteem and/or gives due recognition to students performance/potentials', 'Active'),
(16, 'C', 'Allows students to create their own course with objectives and realistically defined student professor rules and make them accountable for their performance ', 'Active'),
(17, 'C', 'Allows students to think independently and make their own decisions and holding them accountable for their performance based largely on their success in executing decisions', 'Active'),
(18, 'C', 'Encourages students to learn beyond what is required and help/guide the students how to the concepts learned', 'Active'),
(19, 'D', 'Creates oppurtunities for intensive and/or extensive contribution of students in the class activities (e.g. breaks class into days, triads or buss/task groups)', 'Active'),
(20, 'D', 'Assumes roles as facilitator, resource person, coach, inquisitor, integrator, referee in drawing students to contribute to knowledge and understanding of the concepts in hands', 'Active'),
(21, 'D', 'Design and implements learning conditions and experiences that promotes healthy exchange and/or', 'Active'),
(22, 'D', 'Structures/re-structures learning and teaching-learning context to enhance attainment of collective learning objectives ', 'Active'),
(23, 'D', 'Uses Instructional Materials (audio/video materials, fieldtrips, film showing, computer aided instruction and etc.) to reinforces learning processes ', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `self_schedules`
--

CREATE TABLE `self_schedules` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `f_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `self_schedules`
--

INSERT INTO `self_schedules` (`id`, `branch`, `f_id`, `start_date`, `end_date`) VALUES
(4, 'MBC', 23, '2018-02-28', '2018-03-31'),
(5, 'MBC', 16, '2018-02-01', '2018-02-28'),
(6, 'MBC', 14, '2018-01-08', '2018-03-08'),
(7, 'MBC', 24, '2018-02-01', '2018-02-28');

-- --------------------------------------------------------

--
-- Table structure for table `sem_sy_settings`
--

CREATE TABLE `sem_sy_settings` (
  `id` int(11) NOT NULL,
  `semester` varchar(20) NOT NULL,
  `term` varchar(50) NOT NULL,
  `school_year` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sem_sy_settings`
--

INSERT INTO `sem_sy_settings` (`id`, `semester`, `term`, `school_year`, `status`) VALUES
(1, 'First Semester', 'Finals', '2017-2018', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `school_id` varchar(9) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `middlename` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `course` text NOT NULL,
  `curriculum_year` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` text NOT NULL,
  `access_code` varchar(8) NOT NULL,
  `branch` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `school_id`, `firstname`, `middlename`, `lastname`, `course`, `curriculum_year`, `email`, `mobile`, `access_code`, `branch`) VALUES
(1, '2014-0222', 'Harley', 'Ferrer', 'Lumagui', 'BSIT', 4, 'lumagui.harl0723@gmail.com', '09551655757', 'vlGTOw0d', 'MBC'),
(2, '2014-0555', 'Diosher', 'Gupo', 'Centino', 'BSIT', 4, 'cdiosher@gmail.com', '09551655757', 'EqsijLNy', 'MBC'),
(3, '2014-0303', 'Francis Jay', 'Navarro', 'Almarez', 'BSEd - Mat', 3, 'lumagui.harl0723@gmail.com', '09551655757', 'krHzI4aK', 'MBC');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `branch` varchar(3) NOT NULL,
  `course` varchar(15) NOT NULL,
  `curriculum_year` int(11) NOT NULL,
  `subject_code` varchar(20) NOT NULL,
  `subject_description` text NOT NULL,
  `f_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `branch`, `course`, `curriculum_year`, `subject_code`, `subject_description`, `f_id`) VALUES
(65, 'MBC', 'BSIT', 2, 'ITC 212', 'Discrete Structures', 16),
(66, 'MBC', 'BSIT', 3, 'ITE 312', 'IT Elective 3(E-Business)', 16),
(68, 'MBC', 'BSIT', 4, 'IT 410', 'Capstone Project', 16),
(70, 'MBC', 'BSIT', 4, 'ITFE 412', 'Free Elective 2 (Presentation Skill and Research)', 16),
(71, 'MBC', 'BSIT', 4, 'TVET 415', 'Contact Center', 15),
(74, 'MBC', 'BSIT', 2, 'ITP 211', 'Object-Oriented Programming', 22),
(75, 'MBC', 'BSIT', 3, 'ITP 310', 'Web Development', 22),
(76, 'MBC', 'BSIT', 3, 'ITP 311', 'Database Management System', 22),
(77, 'MBC', 'BSIT', 4, 'ITFE 414', 'Seminar/Inspection Trip', 22),
(78, 'MBC', 'BSIT', 2, 'ITC 210', 'Computer Organization', 23),
(79, 'MBC', 'BSIT', 3, 'ITFE 313', 'Free Elective 1 (Intro to Game Programming)', 23),
(80, 'MBC', 'BSIT', 3, 'Soc Sc 310', 'Economics w/ TAR Integrated', 24),
(81, 'MBC', 'BSIT', 2, 'Eng 210', 'Speech and Oral Communication', 25),
(82, 'MBC', 'BSIT', 2, 'Fil 201', 'Pagbasa at Pagsulat Tungo sa Pananaliksik', 26),
(83, 'MBC', 'BSIT', 2, 'P.E.3', 'Fund. of Games and Sports', 27),
(84, 'MBC', 'BSIT', 2, 'Soc Sc 210', 'Society & Culture w/ Family Planning', 28),
(85, 'MBC', 'BSIT', 4, 'ITC 411', 'Professional Ethics', 29),
(86, 'MBC', 'BSIT', 2, 'ITP 213', 'Accounting Principles', 30),
(88, 'MBC', 'BSIT', 1, 'P.E.1', 'Self-Testing Activities', 27),
(89, 'MBC', 'BSCPE', 3, 'Engineering Economy', 'Engineering Economy', 17),
(90, 'MBC', 'BSCPE', 3, 'Soft. Eng.', 'Software Engineering', 22),
(91, 'MBC', 'BSCPE', 3, 'Electronics Device &', 'Electronics Device & Circuits', 31),
(92, 'MBC', 'BSCPE', 3, 'Computer Engineering', 'Computer Engineering Drafting & Design', 31),
(93, 'MBC', 'BSCPE', 3, 'Differential Equatio', 'Differential Equations', 32),
(94, 'MBC', 'BSCPE', 3, 'CSO w/ Assembly Lang', 'Computer System Organization w/ Assembly Language', 33),
(95, 'MBC', 'BSCPE', 3, 'Safety Management', 'Safety Management', 33),
(96, 'MBC', 'BSCPE', 3, 'Static of Rigid Bodi', 'Static of Rigid Bodies', 34),
(97, 'MBC', 'BSCPE', 4, 'Hum 2', 'Art Appreciation', 24),
(98, 'MBC', 'BSCPE', 4, 'Circuit 2', 'Circuit 2', 33),
(99, 'MBC', 'BSCPE', 4, 'Digital Signal Proce', 'Digital Signal Processing', 31),
(100, 'MBC', 'BSCPE', 4, 'Dynamics of R-B', 'Dynamics of Rigid Bodies', 34),
(101, 'MBC', 'BSCPE', 4, 'OS', 'Operating System', 17),
(102, 'MBC', 'BSCPE', 4, 'Eng\'g Ethics & Compu', 'Eng\'g Ethics & Computer Laws', 33),
(103, 'MBC', 'BSCPE', 4, 'Electronics Device &', 'Electronics Device & Circuits', 31),
(104, 'MBC', 'BSCPE', 5, 'Design Project 1', 'Methods of Research', 33),
(105, 'MBC', 'BSCPE', 5, 'MS', 'Microprocessor System', 31),
(106, 'MBC', 'BSCPE', 5, 'Elective 3', 'Emerging Technology', 31),
(107, 'MBC', 'BSCPE', 5, 'Comp. System', 'Computer System Architecture', 33),
(108, 'MBC', 'BSCPE', 5, 'Data Comm.', 'Data Communication', 17),
(109, 'MBC', 'BSCPE', 1, 'P.E. 2', 'Rhythmic Activities', 27),
(110, 'MBC', 'BSCrim', 2, 'Eng 201', 'Technical Report Writing 1', 25),
(111, 'MBC', 'BSCrim', 2, 'Nat Sc 101', 'General Chemistry', 29),
(112, 'MBC', 'BSCrim', 2, 'LEA 3', 'Police Patrol Oprtn w/ Pol. Comm. Sys.', 39),
(113, 'MBC', 'BSCrim', 2, 'CLJ 1', 'Criminal Law (Book 1)', 41),
(114, 'MBC', 'BSCrim', 2, 'LEA 4', 'Police Intelligence', 38),
(115, 'MBC', 'BSCrim', 2, 'Philo 1', 'Logic', 24),
(116, 'MBC', 'BSCrim', 2, 'Criminalistics 1', 'Personnel Identification', 39),
(117, 'MBC', 'BSCrim', 2, 'P.E. 3', 'First Aid & Water Survival', 39),
(118, 'MBC', 'BSCrim', 3, 'Lit.1', 'Philippine Literature', 40),
(119, 'MBC', 'BSCrim', 3, 'Soc Sc 4', 'Basic Economics w/ TLR', 37),
(120, 'MBC', 'BSCrim', 3, 'CDI 2', 'Traffic Management & Accident Investigation', 39),
(121, 'MBC', 'BSCrim', 3, 'Criminalistics 3', 'Forensic Ballistics', 38),
(122, 'MBC', 'BSCrim', 3, 'Criminalistics 4', 'Questioned Documents Examination', 38),
(123, 'MBC', 'BSCrim', 3, 'CA 1', 'Institutional Corrections', 39),
(128, 'MBC', 'BSCrim', 4, 'LEA 6', 'Comparative Police System', 38),
(129, 'MBC', 'BSCrim', 4, 'CDI 5', 'Drug Addiction & Vice Control', 38),
(130, 'MBC', 'BSCrim', 4, 'CDI 6', 'Fire Technology & Arson Investigation', 39),
(131, 'MBC', 'BSCrim', 4, 'CS 6', 'Criminological Research & Statistics', 38),
(133, 'MBC', 'BSHRM', 3, 'Soc Sc 2', 'Cultural Anthropology		\r\n', 24),
(134, 'MBC', 'BSHRM', 3, 'HTC3', 'Tourism Planning & Development', 44),
(136, 'MBC', 'BSHRM', 3, 'Bus Ed 5', 'Basic Finance', 30),
(137, 'MBC', 'BSHRM', 3, 'BHRM', 'Bar Management', 44),
(139, 'MBC', 'BSHRM', 3, 'CHRM', 'Asian & Western Cuisine', 45),
(140, 'MBC', 'BSHRM', 3, 'Rizal', 'Life and Works of Rizal', 24),
(141, 'MBC', 'BSHRM', 3, 'Bus Ed 4', 'Principles of Marketing', 45),
(142, 'MBC', 'BSHRM', 4, 'HTC 6', 'Total Quality Management', 45),
(143, 'MBC', 'BSHRM', 4, 'BUS ED 7', 'Management Information System', 23),
(144, 'MBC', 'BSHRM', 4, 'HRM 8', 'Food & Beverage Control System', 45),
(145, 'MBC', 'BSHRM', 4, 'HTC 7', 'Events Management', 44),
(146, 'MBC', 'BSHRM', 4, 'HTC 5', 'Entrepreneurship & Business Planning', 46),
(147, 'MBC', 'BSHRM', 4, 'Foreign Lang 2', 'Korean / Spanish', 44),
(148, 'MBC', 'BSHTM', 3, 'Soc Sci 2', 'Cultural Anthropology', 24),
(149, 'MBC', 'BSHTM', 3, 'TC 4', 'Philippine Tourism', 45),
(150, 'MBC', 'BSHTM', 3, 'TC 5', 'World Tourism', 45),
(151, 'MBC', 'BSHTM', 3, 'TC 6', 'Total Quality Management', 45),
(152, 'MBC', 'BSHTM', 3, 'TC 7', 'Information Technology in Tourism', 44),
(153, 'MBC', 'BSHTM', 3, 'TC 8', 'Tourism Marketing', 45),
(154, 'MBC', 'BSHTM', 3, 'BE 5', 'Basic Finance', 30),
(155, 'MBC', 'BSHTM', 4, 'Lit 1', 'Philippine Literature', 40),
(156, 'MBC', 'BSHTM', 4, 'Hum 1', 'Arts and Culture', 40),
(157, 'MBC', 'BSHTM', 4, 'Soc Sci 3', 'Basic Economics', 37),
(158, 'MBC', 'BSHTM', 4, 'Soc Sci 4', 'Politics and Governance w/ Consti.', 40),
(159, 'MBC', 'BSHTM', 4, 'SC 4', 'Culinary Arts and Sciences', 44),
(160, 'MBC', 'BSHTM', 4, 'BE 7', 'Management Information System', 23),
(161, 'MBC', 'BSFi', 3, 'Soc Sci 2', 'Society & Culture w/ Family Planning', 28),
(162, 'MBC', 'BSFi', 3, 'Soc Sci 3', 'Phil. Gov\'t Politics & Constitution', 40),
(163, 'MBC', 'BSFi', 3, 'PC 9', 'Project Dev\'t. & Management', 29),
(164, 'MBC', 'BSFi', 3, 'PC 7', 'Meteorology', 47),
(165, 'MBC', 'BSFi', 4, 'Hum III', 'The Philippine Literature', 40),
(167, 'MBC', 'BSFi', 4, 'Elec 4 ', 'Fish Health Management', 50),
(168, 'MBC', 'BSFi', 4, 'Elec 5', 'Navigation & Seamanship', 47),
(169, 'MBC', 'BSFi', 4, 'PC 15', 'Fisheries Management', 51),
(170, 'MBC', 'BSFi', 4, 'Seminar', 'Seminar', 50),
(171, 'MBC', 'BSFi', 3, 'FC 2', 'Analytical Chemistry', 48),
(172, 'MBC', 'BSFi', 3, 'PC 8', 'Aquatic Resources', 50),
(173, 'MBC', 'BSFi', 3, 'FC 4', 'Botany', 50),
(174, 'MBC', 'BS - Entre', 3, 'Soc Sc 132', 'Macroeconomics', 40),
(175, 'MBC', 'BS - Entre', 3, 'Hum 132', 'Philosophy & Ethics', 46),
(176, 'MBC', 'BS - Entre', 3, 'Entrep 131', 'Business Law', 46),
(177, 'MBC', 'BS - Entre', 3, 'Bus. Law 124', 'Human Behavior in Organization (HBO)', 46),
(178, 'MBC', 'BS - Entre', 3, 'Entrep 132', 'Cost Accounting', 30),
(179, 'MBC', 'BS - Entre', 3, 'Entrep 133', 'Business Plan I', 46),
(180, 'MBC', 'BS - Entre', 3, 'Farm Bus 132', 'Aquaculture Operation', 48),
(181, 'MBC', 'BS - Entre', 3, 'Farm Bus 133', 'Aquaculture Facilities', 48),
(182, 'MBC', 'BSEd - Eng', 3, 'Stat', 'Elementary Statistics', 52),
(183, 'MBC', 'BSEd - Eng', 3, 'Educ 4b', 'Principles of Teaching II', 26),
(184, 'MBC', 'BSEd - Eng', 3, 'FS 2', 'Exp. The Teaching & Learning Process', 26),
(185, 'MBC', 'BSEd - Eng', 3, 'Educ 5a', 'Educational Technology I', 26),
(186, 'MBC', 'BSEd - Eng', 3, 'Educ 6a', 'Assessment of Student Learning 1', 32),
(187, 'MBC', 'BSEd - Eng', 3, 'FS 4', 'Exploring the Curriculum', 27),
(188, 'MBC', 'BSEd - Eng', 3, 'STC 1', 'Topics for Distance Learning', 16),
(189, 'MBC', 'BSEd - Eng', 3, 'Engl 8', 'English for Specific Purpose (ESP)', 25),
(190, 'MBC', 'BSEd - Eng', 3, 'Engl 9', 'Mythology & Folklore', 25),
(191, 'MBC', 'BSEd - Eng', 3, 'Engl 10', 'The Teaching of Literature', 35),
(192, 'MBC', 'BSEd - Eng', 4, 'Soc Sc 202', 'Politics & Governance W/ Phil. Constitution', 40),
(193, 'MBC', 'BSEd - Eng', 4, 'Educ 8', 'The Teaching Profession', 36),
(194, 'MBC', 'BSEd - Eng', 4, 'FS 6', 'On Becoming a Teacher', 36),
(195, 'MBC', 'BSEd - Eng', 4, 'Educ 9', 'Developmental Reading', 35),
(196, 'MBC', 'BSEd - Eng', 4, 'STC 2', 'Addressing Learning Gaps', 15),
(197, 'MBC', 'BSEd - Eng', 4, 'STC 3', 'Environmental Education', 49),
(198, 'MBC', 'BSEd - Eng', 4, 'Engl 16', 'Remedial Instruction in English', 35),
(199, 'MBC', 'BSEd - Eng', 4, 'Engl 17', 'Language & Literature Assessment', 35),
(200, 'MBC', 'BSEd - Eng', 4, 'Engl 18', 'Introduction to Stylistics', 35),
(201, 'MBC', 'BSEd - Eng', 4, 'Engl 19', 'Translation & Editing of Text', 25),
(202, 'MBC', 'BSEd - Eng', 4, 'Engl 20', 'Language Research', 25),
(203, 'MBC', 'BSEd - Mat', 3, 'Educ 4b', 'Principles of Teaching II', 14),
(204, 'MBC', 'BSEd - Mat', 3, 'FS 2', 'Exp. The Teaching & Learning Process', 14),
(205, 'MBC', 'BSEd - Mat', 3, 'Educ 5a', 'Educational Technology I', 15),
(206, 'MBC', 'BSEd - Mat', 3, 'Educ 6a', 'Assessment of Student Learning 1', 32),
(207, 'MBC', 'BSEd - Mat', 3, 'Educ 7', 'Curriculum Development', 27),
(208, 'MBC', 'BSEd - Mat', 3, 'FS 4', 'Exploring the Curriculum', 27),
(209, 'MBC', 'BSEd - Mat', 3, 'STC 1', 'Topics for Distance Learning', 16),
(210, 'MBC', 'BSEd - Mat', 3, 'Math 8', 'Elementary Statistics', 32),
(211, 'MBC', 'BSEd - Mat', 3, 'Math 9', 'Analytic Geometry', 34),
(212, 'MBC', 'BSEd - Mat', 3, 'Math 10', 'Action Research in Mathematics', 53),
(213, 'MBC', 'BSEd - Mat', 3, 'Math 11', 'Mathematical Invest. & Modeling', 54),
(214, 'MBC', 'BSEd - Mat', 4, 'Soc Sc 202', 'Politics & Governance w/ Phil. Consti', 41),
(215, 'MBC', 'BSEd - Mat', 4, 'Educ 8', 'The Teaching Profession', 36),
(216, 'MBC', 'BSEd - Mat', 4, 'FS 6', 'On Becomig A Teacher', 36),
(217, 'MBC', 'BSEd - Mat', 4, 'Educ 9', 'Developmental Reading', 35),
(218, 'MBC', 'BSEd - Mat', 4, 'STC 2', 'Addressing Learning Gaps', 15),
(219, 'MBC', 'BSEd - Mat', 4, 'STC 3', 'Environmental Education', 49),
(220, 'MBC', 'BSEd - Mat', 4, 'Math 17', 'Abstract Algebra', 53),
(222, 'MBC', 'BSEd - Mat', 4, 'Math 18', 'Calculus II', 32),
(223, 'MBC', 'BSEd - Mat', 4, 'Math 19', 'Advanced Statistics', 53),
(224, 'MBC', 'BSEd - Mat', 4, 'Math 20', 'Modern Goemetry', 32),
(226, 'MBC', 'BSEd - Bio', 3, 'Educ 1', 'Child and Adolescent Development', 49),
(227, 'MBC', 'BSEd - Bio', 3, 'Educ 2', 'Facilitating Learning', 49),
(228, 'MBC', 'BSEd - Bio', 3, 'Educ 6a', 'Assessment of Student Learning 1', 53),
(229, 'MBC', 'BSEd - Bio', 3, 'Educ 7', 'Curriculum Development', 27),
(230, 'MBC', 'BSEd - Bio', 3, 'FS 4', 'Exploring the Curriculum', 27),
(231, 'MBC', 'BSEd - Bio', 3, 'STC 1', 'Topics for Distance Learning', 16),
(232, 'MBC', 'BSEd - Bio', 3, 'Bio Sc 7', 'Action Research in Science', 55),
(233, 'MBC', 'BSEd - Bio', 3, 'Bio Sc 8', 'Sci. Technology Society', 43),
(234, 'MBC', 'BSEd - Bio', 3, 'Bio Sc 5', 'Biotechniques', 55),
(235, 'MBC', 'BSEd - Bio', 4, 'Soc Sc 202', 'Politics & Governance w/ Phil. Consti', 40),
(236, 'MBC', 'BSEd - Bio', 4, 'Educ 8', 'The Teaching Profession', 36),
(237, 'MBC', 'BSEd - Bio', 4, 'FS 6', 'On Becomig A Teacher', 36),
(238, 'MBC', 'BSEd - Bio', 4, 'Educ 9', 'Developmental Reading', 35),
(239, 'MBC', 'BSEd - Bio', 4, 'STC 2', 'Addressing Learning Gaps', 15),
(240, 'MBC', 'BSEd - Bio', 4, 'STC 3', 'Environmental Education', 49),
(241, 'MBC', 'BSEd - Bio', 4, 'Bio Sci 12', 'Microbiology', 29),
(242, 'MBC', 'BSEd - Bio', 4, 'Bio Sci 13', 'Human Anatomy & Physiology', 55),
(243, 'MBC', 'BSEd - Bio', 4, 'Bio Sci 14', 'Physics for Health Sciences', 43),
(244, 'MBC', 'BSEd - Bio', 4, 'Bio Sci 15 ', 'Cell Biology', 47),
(245, 'MBC', 'BSEd - TLE', 2, 'Eng 201', 'Speech & Oral Communication', 25),
(246, 'MBC', 'BSEd - TLE', 2, 'Fil 201', 'Masining ng Pagpapahayag', 26),
(247, 'MBC', 'BSEd - TLE', 2, 'Educ 1', 'Child and Adolescent Development', 49),
(248, 'MBC', 'BSEd - TLE', 2, 'Educ 2', 'Facilitating Learning', 49),
(249, 'MBC', 'BSEd - TLE', 2, 'Lit 2', 'Literature of the World', 35),
(250, 'MBC', 'BSEd - TLE', 2, 'TLE 1', 'Basic Drafting', 42),
(251, 'MBC', 'BSEd - TLE', 2, 'TLE 2', 'Business Mathematics', 32),
(252, 'MBC', 'BSEd - TLE', 2, 'TLE 3', 'Basic Electronics', 42),
(253, 'MBC', 'BSEd - TLE', 2, 'P.E. 201', 'Individual/Dual Sports', 27),
(254, 'MBC', 'BSEd - TLE', 3, 'Stat', 'Elementary Statistics', 52),
(255, 'MBC', 'BSEd - TLE', 3, 'Educ 4b', 'Principles of Teaching II', 26),
(256, 'MBC', 'BSEd - TLE', 3, 'FS 2', 'Experiencing the Teaching and Learning Proc.', 26),
(257, 'MBC', 'BSEd - TLE', 3, 'FS 3', 'Tech. in the Learning Environ.', 15),
(258, 'MBC', 'BSEd - TLE', 3, 'Educ 5a', 'Educational Technology I', 15),
(259, 'MBC', 'BSEd - TLE', 3, 'Educ 6a', 'Assessment of Student Learning 1', 53),
(260, 'MBC', 'BSEd - TLE', 3, 'Educ 7', 'Curriculum Development', 27),
(261, 'MBC', 'BSEd - TLE', 3, 'FS 4', 'Exploring the Curriculum', 27),
(262, 'MBC', 'BSEd - TLE', 3, 'STC 1', 'Topics for Distance Learning', 16),
(263, 'MBC', 'BSEd - TLE', 3, 'TLE 7', 'Food Selection, Prep. & Processes', 56),
(264, 'MBC', 'BSEd - TLE', 3, 'TLE 8', 'Basic Electronics', 42),
(265, 'MBC', 'BSEd - TLE', 3, 'TLE 9', 'Entrepreneurship', 57),
(266, 'MBC', 'BSEd - TLE', 4, 'Soc Sc 202', 'Politics & Governance W/ Phil. Constitution', 41),
(267, 'MBC', 'BSEd - TLE', 4, 'Educ 8', 'The Teaching Profession', 49),
(268, 'MBC', 'BSEd - TLE', 4, 'Educ 9', 'Developmental Reading', 35),
(269, 'MBC', 'BSEd - TLE', 4, 'FS 6', 'On Becoming a Teacher', 49),
(270, 'MBC', 'BSEd - TLE', 4, 'STC 2', 'Addressing Learning Gaps', 15),
(271, 'MBC', 'BSEd - TLE', 4, 'STC 3', 'Environmental Education', 49),
(272, 'MBC', 'BSEd - TLE', 4, 'TLE 14', 'Advanced Drafting', 42),
(273, 'MBC', 'BSEd - TLE', 4, 'TLE 15', 'Arts & Design', 58),
(274, 'MBC', 'BSEd - TLE', 4, 'TLE 17', 'Advanced Electronics', 42),
(275, 'MBC', 'BSEd - TLE', 4, 'AR', 'Action Research in TLE', 25),
(276, 'MBC', 'BEEd', 3, 'Educ 4b', 'Principles of Teaching 2', 14),
(277, 'MBC', 'BEEd', 3, 'Educ 5a', 'Educational Technology 1', 15),
(278, 'MBC', 'BEEd', 3, 'Educ 6a', 'Assessment of Student Learning 1', 53),
(279, 'MBC', 'BEEd', 3, 'Educ 7', 'Curriculum Development', 14),
(280, 'MBC', 'BEEd', 3, 'FS 2', 'Experiencing the Tchng-Lrng Process', 14),
(281, 'MBC', 'BEEd', 3, 'FS 4', 'Exploring the Curriculum', 53),
(282, 'MBC', 'BEEd', 3, 'Math 03', 'Statistics', 32),
(283, 'MBC', 'BEEd', 3, 'Engl 02', 'Methods of Research', 35),
(284, 'MBC', 'BEEd', 3, 'TS', 'Tchg Strat for Fil, Eng & Soc Studies', 26),
(285, 'MBC', 'BEEd', 3, 'STC 1', 'Topics for Distance Learning', 16),
(286, 'MBC', 'BEEd', 4, 'Music 101', 'Fundamentals of Music', 14),
(287, 'MBC', 'BEEd', 4, 'Educ 112', 'The Teaching Profession', 49),
(288, 'MBC', 'BEEd', 4, 'FS 6', 'On Becoming a Teacher', 49),
(289, 'MBC', 'BEEd', 4, 'SS 1', 'Geography', 24),
(290, 'MBC', 'BEEd', 4, 'Val. Ed.', 'Values Education', 14),
(291, 'MBC', 'BEEd', 4, 'Rizal', 'Rizal\'s Life & Works', 24),
(292, 'MBC', 'BEEd', 4, 'Lit 2', 'World Literature', 35),
(293, 'MBC', 'BEEd', 4, 'STC 3', 'Use of Popular Media in Teaching', 16),
(294, 'MBC', 'BEEd', 4, 'Soc Dim', 'Social Dimension of Education', 49),
(295, 'MBC', 'BS - Pol S', 3, 'Pol Sci 6', 'Ancient & Medieval Political Theories', 40),
(296, 'MBC', 'BS - Pol S', 3, 'Pol Sci 7', 'Modern Political Theories', 37),
(297, 'MBC', 'BS - Pol S', 3, 'Pol Sci 9', 'Political System & Doctrines', 37),
(298, 'MBC', 'BS - Pol S', 3, 'Pol Sci 10', 'Philippine Political Thoughts', 37),
(299, 'MBC', 'BS - Pol S', 3, 'Elective 2', 'International Law', 37),
(300, 'MBC', 'BS - Pol S', 3, 'Business Law', 'Business Law', 46),
(301, 'MBC', 'BS - Pol S', 4, 'Pol Sc 14', 'Int\'l & Regional Organizations', 24),
(302, 'MBC', 'BS - Pol S', 4, 'Pol Sc 15', 'Intro to Political Analysis', 53),
(303, 'MBC', 'BS - Pol S', 4, 'Pol Sc 16', 'Quantitative Analysis of Political Data', 53),
(304, 'MBC', 'BS - Pol S', 4, 'Philo 2', 'Logic', 24),
(305, 'MBC', 'BS - Pol S', 4, 'Research 1', 'Methods of Research', 41),
(306, 'MBC', 'BS - Pol S', 4, 'Ethics 401', 'Prof. Ethics & Values Education', 59),
(307, 'MBC', 'BS -Entrep', 3, 'Soc Sc 132', 'Macroeconomics', 40),
(308, 'MBC', 'BS -Entrep', 3, 'Hum 132', 'Philosophy & Ethics', 46),
(309, 'MBC', 'BS -Entrep', 3, 'Entrep 131', 'Business Law', 46),
(310, 'MBC', 'BS -Entrep', 3, 'Bus. Law 124', 'Human Behavior in Organization (HBO)', 46),
(311, 'MBC', 'BS -Entrep', 3, 'Entrep 132', 'Cost Accounting', 30),
(312, 'MBC', 'BS -Entrep', 3, 'Entrep 133', 'Business Plan I', 46),
(313, 'MBC', 'BS -Entrep', 3, 'Farm Bus 132', 'Aquaculture Operation', 48),
(314, 'MBC', 'BS -Entrep', 3, 'Farm Bus 133', 'Aquaculture Facilities', 48);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `std_id` int(11) NOT NULL,
  `user_code` varchar(9) NOT NULL,
  `password` text NOT NULL,
  `date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` varchar(25) NOT NULL,
  `account_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `std_id`, `user_code`, `password`, `date_registered`, `role`, `account_status`) VALUES
(1, 14, '2014-0222', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-21 05:13:31', 'Administrator', 'Active'),
(3, 14, 'f5763', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-01-22 06:33:20', 'Faculty', 'Active'),
(5, 27, '2014-0555', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-01-22 06:46:06', 'Student', 'Active'),
(6, 37, '2014-0547', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-01-23 13:27:40', 'Student', 'Active'),
(7, 40, '2014-0547', 'f86c4171fa47d258798b467075647092', '2018-01-25 16:53:39', 'Student', 'Active'),
(8, 41, '2012-0094', '704198d95e07c8eafe1a661a1f6264cc', '2018-01-26 07:42:03', 'Student', 'Active'),
(9, 43, '2015-0357', 'e1cdb808801c25354364ae76e0971fb6', '2018-01-26 08:49:38', 'Student', 'Active'),
(10, 44, '2015-0335', '827ccb0eea8a706c4c34a16891f84e7b', '2018-01-26 08:49:36', 'Student', 'Active'),
(11, 45, '2015-0678', '96e79218965eb72c92a549dd5a330112', '2018-01-26 09:00:16', 'Student', 'Active'),
(12, 47, '2014-0120', '4ca4b52193d6fc7c73ae38c3be5c2ee9', '2018-01-26 09:05:23', 'Student', 'Inactive'),
(13, 31, '2014-0635', 'baa3657bb14adad0e6cf38905089ba33', '2018-01-29 14:32:45', 'Administrator', 'Active'),
(14, 24, 'f5453', '4900d0a19b6894a4a514e9ff3afcc2c0', '2018-01-29 13:24:26', 'Faculty', 'Active'),
(15, 31, '2014-0635', 'baa3657bb14adad0e6cf38905089ba33', '2018-01-30 09:49:48', 'Student', 'Active'),
(16, 31, '2014-0635', 'baa3657bb14adad0e6cf38905089ba33', '2018-01-30 09:49:48', 'Student', 'Active'),
(17, 36, 'f5044', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-02 13:24:42', 'Supervisor', 'Active'),
(18, 44, 'f9230', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-05 01:51:14', 'Faculty', 'Inactive'),
(19, 16, 'f5500', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-06 01:16:30', 'Faculty', 'Active'),
(20, 66, '2014-0666', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-06 04:06:17', 'Student', 'Active'),
(21, 3, '2014-0303', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-21 07:50:58', 'Student', 'Active'),
(22, 24, 'f5453', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-02-22 21:47:22', 'Faculty', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campus_schedules`
--
ALTER TABLE `campus_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation_results`
--
ALTER TABLE `evaluation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluator_types`
--
ALTER TABLE `evaluator_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `faculty_students`
--
ALTER TABLE `faculty_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overall_results`
--
ALTER TABLE `overall_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peer_schedules`
--
ALTER TABLE `peer_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `self_schedules`
--
ALTER TABLE `self_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sem_sy_settings`
--
ALTER TABLE `sem_sy_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campus_schedules`
--
ALTER TABLE `campus_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `evaluation_results`
--
ALTER TABLE `evaluation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `evaluator_types`
--
ALTER TABLE `evaluator_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `faculty_students`
--
ALTER TABLE `faculty_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `overall_results`
--
ALTER TABLE `overall_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `peer_schedules`
--
ALTER TABLE `peer_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `self_schedules`
--
ALTER TABLE `self_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sem_sy_settings`
--
ALTER TABLE `sem_sy_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
